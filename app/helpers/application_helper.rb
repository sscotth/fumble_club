module ApplicationHelper

	def resource_name
   		:user
  	end
 
	def resource
    	@resource ||= User.new
  	end
 
 	def devise_mapping
    	@devise_mapping ||= Devise.mappings[:user]
  	end


	def current_week
		y = 1.days.ago.strftime('%Y').to_i
		i = 1.days.ago.strftime('%U').to_i
		if i < 12
			w = 17
			y -= 1
		elsif i.between?(12,34)
			w = 1
		else
			w = [i - 34,17].min
		end
		Week.where(season_year: y, season_week: w)[0]
	end

	def last_week
		if current_week.season_week >= 1
			Week.where(season_year: current_week.season_year, season_week: current_week.season_week - 1)[0]
		else
			nil
		end
	end
	
	def next_week
		Week.where(season_year: current_week.season_year, season_week: current_week.season_week + 1)[0]
	end

	def define_week
	  Time.zone = 'Eastern Time (US & Canada)'
	  
	  if Time.now < current_week.pick_deadline
	    @pick_week = current_week
	    @summary_week = last_week
	  else
	  	@pick_week = next_week
	  	@summary_week = current_week
	  end

	  @deadline = @pick_week.pick_deadline
	end


	def week_games_picked(wk)
		Game.where{(((home_team_id >> my{picked_team_ids} | away_team_id >> my{picked_team_ids}) & (force_no_pick == false)) | (force_pick == true)) & (week_id == my{wk})}.order{points_req}.order{start_time}
	end

	def week_games_not_picked(wk)
		Game.find_all_by_week_id(wk) - week_games_picked(wk)
	end

	def picked_team_ids
		#current season
		ids = [31,32,33,34,35,36,37,38,39,40,41,42,92,93,94,95,96,97,98,99,100,
         101,102,103,220,221,222,223,224,225,226,227,228,109,57,66,135,134,17,21,88,4]
	end

	def points_games
	end
	
	def picked_teams
    picked_teams = Team.where(id: picked_team_ids)
	end

	def active_users
    User.joins{division}.where{division.id >> [1,2,3,4]}
  end
end
