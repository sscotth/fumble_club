ActiveAdmin.register User do
  actions :all, :except => :destroy
   index do
    column :name
    column :email
    column :current_sign_in_at
    column :last_sign_in_at
    column :sign_in_count
    default_actions
  end
  
  form do |f|
    f.inputs "User Details" do
      f.input :email
      f.input :name
      f.input :roles, :as => :check_boxes, :collection => Role.all
    end
    f.buttons
  end 

  action_item :only => :new do
    form do |f|
      f.inputs "User Details" do
        f.input :email
        f.input :name
      	f.input :roles, :as => :check_boxes, :collection => Role.all
     	f.input :password
     	f.input :password_confirmation
      end
      f.buttons
    end 
  end

end
