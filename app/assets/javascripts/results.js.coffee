# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $("#results").dataTable
    aaSorting: [ [ 2, "desc" ], [ 4 , "asc" ] ]
    bPaginate: false  # Remove pagination
    bFilter: false    # Remove filter
    bInfo: false      # Remove useless info
    asSorting: [ "asc" ], "aTargets": [ 0 ]

    "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap";
