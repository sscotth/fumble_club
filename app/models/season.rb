class Season < ActiveRecord::Base
  attr_accessible :year

  has_many :weeks
  has_many :games, :through => :weeks
end
