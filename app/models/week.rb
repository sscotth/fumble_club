class Week < ActiveRecord::Base
  attr_accessible :season_year, :season_week, :pick_deadline
  belongs_to :season
  has_many :games
  has_many :picks, :through => :games
end
