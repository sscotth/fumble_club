class Team < ActiveRecord::Base
  attr_accessible :name, :espn_team_id

  belongs_to :conference
	has_many :users
	has_many :occurances_as_away_team, :class_name => "Game", :foreign_key => "away_team_id"
	has_many :occurances_as_home_team, :class_name => "Game", :foreign_key => "home_team_id"

	validates :name, 		:uniqueness => true, 
											:presence => true
	
end
