class Pick < ActiveRecord::Base
  attr_accessible :home_win_pick, :winner_id, :away_score_pick, :home_score_pick, :game_id, :user_id
  attr_accessor :winner_id

  belongs_to :game
  belongs_to :user
  belongs_to :week

  validates :game_id, :presence => true
  validates :user_id, :presence => true

  validates_numericality_of :away_score_pick, :allow_nil => true,
                                              :only_integer => true,
                                              :less_than => 100,
                                              :greater_than_or_equal_to => 0,
                                              :message => 'Only integer scores 0-99 allowed.'
  validates_numericality_of :home_score_pick, :allow_nil => true,
                                              :only_integer => true,
                                              :less_than => 100,
                                              :greater_than_or_equal_to => 0,
                                              :message => 'Only integer scores 0-99 allowed.'

  validates_inclusion_of :home_win_pick,  :in => [true,false],
                                          :message => 'Missing the home_win_pick'                                            
  
  before_save :change_winner_id

  def change_winner_id
  	if !self.winner_id.nil?
  		logger.info self.winner_id == self.game.away_team_id
  		if self.winner_id == self.game.away_team_id
  			self.home_win_pick = false
  			return true
  		elsif self.winner_id == self.game.home_team_id
  			self.home_win_pick = true
        return true
  		else
  			return false
  		end
  	end
  end

  def winner
  	if self.home_win_pick == true
  		self.game.home_team
  	elsif self.home_win_pick == false
  		self.game.away_team
  	end
  end

  def points_display
    if self.game.points_req
      "#{self.away_score_pick} - #{self.home_score_pick}"
    else
      nil
    end
  end

  def points_earned
    if self.game.points_req

        ap = self.away_score_pick
        hp = self.home_score_pick

      if !self.game.score.nil?
        as = self.game.score.away_score
        hs = self.game.score.home_score
      else
        as = 0
        hs = 0
      end

      p1 = (ap - as).abs
      p2 = (hp - hs).abs
      p3 = ((ap + hp) - (as + hs)).abs
      p4 = 2*((ap - hp) - (as - hs)).abs
  
      pts = p1 + p2 + p3 + p4

      return pts
    end 
  end

  def winning?
    if self.home_win_pick
      self.game.score.away_score < self.game.score.home_score
    else
      self.game.score.away_score > self.game.score.home_score
    end
  end

  def correct?
    if !self.game.score.nil?
      if self.game.score.final
        self.winning?
      else
        return nil
      end
    else
      return nil
    end
  end
end
