class Conference < ActiveRecord::Base
  attr_accessible :name, :league
	has_many :teams

	validates :name, :presence => true, :uniqueness => true
	validates :league, :presence => true
end
