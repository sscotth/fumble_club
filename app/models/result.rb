class Result < ActiveRecord::Base
  attr_accessible :user_id, :week_id, :wins, :losses, :points, :earnings

  validates :user_id, :presence => true
  validates :week_id, :presence => true
  validates :wins, :presence => true
  validates :losses, :presence => true
  validates :points, :presence => true

  belongs_to :user
  belongs_to :week

end
