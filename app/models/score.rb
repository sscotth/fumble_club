class Score < ActiveRecord::Base
  attr_accessible :away_score, :home_score, :status, :final

	validates :game_id, 	:presence => true, 
							:uniqueness => true
	validates :away_score, 	:numericality => { :greater_than_or_equal_to => 0 }
	validates :home_score, 	:numericality => { :greater_than_or_equal_to => 0 }

	def display
		"#{self.away_score} - #{self.home_score}"
	end
end
