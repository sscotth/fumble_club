class Penalty < ActiveRecord::Base
  attr_accessible :week_id, :user_id, :full
  validates :week_id, :presence => true
  validates :user_id, :presence => true

end
