class User < ActiveRecord::Base
	rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable, 
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :role_ids, :team_id, :division_id
  
  has_many :picks
  belongs_to :team
  belongs_to :division

  def short_name
    return "#{self.name.split(' ').first} #{self.name.split(' ').last[0]}."
  end

  def week_picks(wk)
    @picks = self.picks.includes(:game => [:home_team, :away_team]).where(:games => {:week_id => wk}).order("picks.user_id ASC,games.points_req ASC, games.start_time ASC, picks.game_id ASC")
  end

  def live_overall_record
    u_id = self.id

    wins = Pick.joins{game.score}.where{
      (game.score.final == true) &
      (user_id == u_id) &
      (
        (game.score.away_score < home_score) & (home_win_pick == true) |
        (game.score.away_score > home_score) & (home_win_pick == false)
      )}.count

    losses = Pick.joins{game.score}.where{
      (game.score.final == true) &
      (user_id == u_id) &
      (
        (game.score.away_score < home_score) & (home_win_pick == false) |
        (game.score.away_score > home_score) & (home_win_pick == true)
      )}.count

    one_game_penalties = Penalty.find_all_by_user_id_and_full(u_id, false).count
    full_penalties = Penalty.find_all_by_user_id_and_full(u_id, true).count
    
    return "#{wins - one_game_penalties} - #{losses + one_game_penalties} | #{self.live_overall_points} | #{one_game_penalties} - #{full_penalties}"
  end

  def live_week_record(week)
    u_id = self.id
    wk_id = week.id

    wins = Pick.joins{game.score}.where{
      (game.week_id == wk_id) &
      (game.score.final == true) &
      (user_id == u_id) &
      (
        (game.score.away_score < home_score) & (home_win_pick == true) |
        (game.score.away_score > home_score) & (home_win_pick == false)
      )}.count

    losses = Pick.joins{game.score}.where{
      (game.week_id == wk_id) &
      (game.score.final == true) &
      (user_id == u_id) &
      (
        (game.score.away_score < home_score) & (home_win_pick == false) |
        (game.score.away_score > home_score) & (home_win_pick == true)
      )}.count

    one_game_penalties = Penalty.find_all_by_user_id_and_full_and_week_id(u_id, false, wk_id).count
    full_penalties = Penalty.find_all_by_user_id_and_full_and_week_id(u_id, true, wk_id).count
    
    if wins + losses + one_game_penalties + full_penalties > 0 
      return "#{wins - one_game_penalties} - #{losses + one_game_penalties} | #{self.live_week_points} | #{one_game_penalties} - #{full_penalties}"
    else
      return nil
    end
  end

  def live_overall_points
    u_id = self.id

    picks = Pick.joins{game.score}.where{
      (game.score.final == true) &
      (user_id == u_id) &
      (game.points_req == true)}

    sum = 0

    picks.each do |p|
      sum += p.points_earned
    end

    return sum
  end

  def live_week_points(week)
    u_id = self.id
    wk_id = week.id

    picks = Pick.joins{game.score}.where{
      (game.week_id == wk_id) &
      (game.score.final == true) &
      (user_id == u_id) &
      (game.points_req == true)}

    sum = 0

    picks.each do |p|
      sum += p.points_earned
    end

    return sum

  end
  
  def official_overall_record
    wins = Result.where(user_id: self.id).sum(:wins)
    losses = Result.where(user_id: self.id).sum(:losses)
    ogp = self.one_game_penalties
    mgp = self.full_penalties

    return "#{wins - ogp} - #{losses + ogp} | #{self.official_overall_points} | #{ogp} - #{mgp}"
  end

  def official_week_record(week)
    wins = Result.find_by_user_id_and_week_id(self.id, week.id).wins
    losses = Result.find_by_user_id_and_week_id(self.id, week.id).losses
    ogp = Penalty.find_all_by_user_id_and_full_and_week_id(self.id, false, week.id).count
    mgp = Penalty.find_all_by_user_id_and_full_and_week_id(self.id, true, week.id).count

    return "#{wins - ogp} - #{losses + ogp} | #{self.official_week_points(week)} | #{ogp} - #{mgp}"
  end

  def official_overall_points
    Result.where(user_id: self.id).sum(:points)
  end

  def official_week_points(week)
    wins = Result.find_by_user_id_and_week_id(self.id, week.id).points
  end

  def official_overall_earnings
    Result.where(user_id: self.id).sum(:earnings) / 100.0
  end

  def one_game_penalties
    Penalty.find_all_by_user_id_and_full(self.id, false).count
  end

  def full_penalties
    Penalty.find_all_by_user_id_and_full(self.id, true).count
  end

end
