class Game < ActiveRecord::Base
  attr_accessible :week_id, :away_team_id, :home_team_id, :start_time, :espn_game_id, :points_req, :force_no_pick, :force_pick

  belongs_to :week
  belongs_to :season
  has_many :picks
  has_one :score
  belongs_to :away_team, :class_name => "Team", :foreign_key => "away_team_id"
  belongs_to :home_team, :class_name => "Team", :foreign_key => "home_team_id"
  validates :week_id, :presence => true
  validates :away_team_id, :presence => true
  validates :home_team_id, :presence => true

  def teams
    [self.away_team,self.home_team]
  end

  def matchup
    "#{self.away_team.name} vs. #{self.home_team.name}"
  end

  def winner
    if self.score.final
      if self.score.away_score < self.score.home_score
        self.home_team
      else
        self.away_team
      end
    else
      return nil
    end
  end

  def final
    if !self.score.nil?
      self.score.final
    else
      false
    end
  end

  def winning
    if !self.score.nil?
      if self.score.away_score < self.score.home_score
        return self.home_team
      elsif self.score.away_score > self.score.home_score
        return self.away_team
      else
        return nil
      end
    else
      return nil
    end
  end

  #points
  #[37, 134, 255, 378, 508, 637, 1358, 764, 873, 1031, 1107, 1233, 1307, 1325]
  #[322452426, 322520026, 322590024, 322660052, 322730245, 322802579, 322870238, 322940231, 323012506, 323080099, 323150193, 323220231, 323292509, 323362032] 
 
end
