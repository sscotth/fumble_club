class PickSubmission < ActionMailer::Base
  default from: "mail@fumbleclub.com"

   
  def pick_submit_confirmation(user,pick_array)
  	@greeting = 'Hi'
  	@picks = pick_array
  	mail to: user.email, subject: 'Picks submitted'
  end
end
