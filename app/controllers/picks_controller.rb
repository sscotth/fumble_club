class PicksController < ApplicationController
  
  include ApplicationHelper
  before_filter :authenticate_user!

  def index
    define_week
    @week = @pick_week
    @picks = current_user.picks.includes(:game => [:home_team, :away_team]).where(:games => {:week_id => @week}).order("picks.user_id ASC,games.points_req ASC, games.start_time ASC, picks.game_id ASC")
  end

  def new
  	define_week
    @week = @pick_week

    if Time.now > @deadline
      flash[:error] = 'Nice Try! Pick deadline passed. Contact the Administrator'
      redirect_to :picks
    end

    @all_picks = Pick.new
    @week_games = week_games_picked(@week) 
    @prior_picks = current_user.picks.includes(:game => [:home_team, :away_team]).where(:games => {:week_id => @week}).order("picks.user_id ASC,games.points_req ASC, games.start_time ASC, picks.game_id ASC")


  end

  def create
    define_week
    @week = @pick_week
    @user = current_user

    if Time.now > @deadline
      flash[:error] = 'Nice Try! Pick deadline passed. Contact the Administrator'
      redirect_to :picks
    else
      @all_picks = Pick.new
      @week_games = week_games_picked(@week)
      
      picks = params[:all_picks]
    # "all_picks"=>{"6"=>{"winner_id"=>"103"},
    # "16"=>{"winner_id"=>"227"},...},
      points_pick = params[:points_pick]
    # "points_pick"=>{"37"=>{"away_score_pick"=>"2",
    # "home_score_pick"=>"23"}},

      if picks.keys.count != @week_games.count
        flash[:error] = 'Missing a pick'
        render 'new'
      end
      
      picks.keys.each do |game_id|
        pick = current_user.picks.find_or_initialize_by_game_id(game_id.to_i) 
        pick.winner_id = picks[game_id].values[0].to_i
        if !points_pick.nil?
          if points_pick[game_id]
            pick.away_score_pick = points_pick[game_id.to_s]["away_score_pick"].to_i
            pick.home_score_pick = points_pick[game_id.to_s]["home_score_pick"].to_i
          end
        end
        pick.change_winner_id
        pick.save!

      end
      PickSubmission.pick_submit_confirmation(@user,@user.week_picks(@week)).deliver
      flash[:success] = "Picks updated. Check your email for confirmation."
      redirect_to (:picks)
    end
  end

end

private

def find_pick_week
  Time.zone = 'Eastern Time (US & Canada)'
  
  if Time.now < current_week.pick_deadline
    @week = current_week
  else
    @week = next_week
  end

  @deadline = @week.pick_deadline
end
