class StandingsliveController < ApplicationController
include ApplicationHelper

def index
	@result = total_results
	@totals = @result[1]
	@result = @result[0]
	@active_users = []
	Division.all.each do |d|
		div_ids = []
		d.users.each do |u|
			div_ids << u.id
		end
		@active_users << div_ids
	end
end



private

  def total_results
  	result = {}
  	@wt = {}
  	@lt = {}
  	User.all.each do |u|
		@wt[u.id] = 0
		@lt[u.id] = 0
	end
	Week.all.each do |wk|
		puts wk.season_week
		weekly = {}
		@w = {}
		@l = {}
		User.all.each do |u|
			@w[u.id] = 0
			@l[u.id] = 0
		end

	  wk.picks.includes(:game => :score).each do |p|
	    if p.correct?
	      @w[p.user_id] += 1
	      @wt[p.user_id] += 1
	    elsif p.correct? == false
	      @l[p.user_id] += 1
	      @lt[p.user_id] += 1
	    end  
	  end  
	  		puts "w: #{@w}"
	  if 0 < (@w[1] + @l[1])
	  	@w.keys.each do |u|
	 	  	weekly[u] = "#{@w[u]} - #{@l[u]}"
	  	end
	  else
	  	break
	  end
	  result[wk.id] = weekly
	end
	total = {}
	@wt.keys.each do |u|
		total[u] = "#{@wt[u]} - #{@lt[u]}"
	end	
	rtn = [result,total]
	puts rtn
	return rtn
  end

end
