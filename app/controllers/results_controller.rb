class ResultsController < InheritedResources::Base

  def index
    @totals = []
    Result.joins(:user => :division).group(:users => :name, :divisions => :name).
      select('divisions.name as div, users.name, SUM(results.wins) as swins, SUM(results.losses) as slosses,
               SUM(results.points) as spoints, SUM(results.earnings) as searnings').
      order('swins DESC, spoints').each do |v|
      
      @totals << [v.div, v.name, v.swins.to_i, v.slosses.to_i, v.spoints.to_i, v.searnings.to_i]
    end
    
    @good_through = Result.joins(:week).order(:week_id).last.week.season_week
  end
end
