class WeekController < ApplicationController
	
  include ApplicationHelper
  before_filter :authenticate_user!

  def index
    define_week
    @weeks = Week.order(:season_week)
    @week = @summary_week
    @week_games_picked = week_games_picked(@week) 
    @all_picks = User.includes(:picks => {:game => [:score, :home_team, :away_team]}).where(:games => {:week_id => @week.id}).order("picks.user_id ASC,games.points_req ASC, games.start_time ASC, picks.game_id ASC")
    @divs = Division.includes(:users)
    picks_to_hash
  
  end

  def show
    week_wanted = Week.find(params[:id])
    define_week
    if week_wanted.season_week > @summary_week.season_week && !current_user.has_role?(:admin)
      flash[:error] = 'Nice Try!'
      redirect_to '/week'
    else
      @weeks = Week.all
      @week = week_wanted
      @week_games_picked = week_games_picked(@week) 
      @divs = Division.includes(:users)
      @all_picks = User.includes(:picks => {:game => [:score, :home_team, :away_team]}).where(:games => {:week_id => @week.id}).order("picks.user_id ASC,games.points_req ASC, games.start_time ASC, picks.game_id ASC") 
      picks_to_hash
      render :index
    end
  end

  private

  def picks_to_hash
    @disp_picks = {}
    @disp_points = {}
    for u in @all_picks
      up = {}
      for p in u.picks
        if p.home_win_pick
          up[p.game_id] = p.game.home_team.name
        else
          up[p.game_id] = p.game.away_team.name
        end
        if p.game.points_req
          @disp_points[u.id] = [p.points_display,p.points_earned]
        end
      end
      @disp_picks[u.id] = up
    end
    #@disp_picks = User_id => game_id => team_name
    #@disp_points = User_id => "AS - HS"
  end

  def important_games
    @important_games = {}
    @divs.each do |d|
      d.users.each do |u|

    #@important_games = division_id => game_id
      end
    end
  end
end
