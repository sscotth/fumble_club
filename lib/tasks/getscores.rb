#desc "This task is called by the Heroku cron add-on"
puts "Initializing..."
#t :game_load => :environment do
  #if Time.now.hour % 1 == 0 # run every hour
  #if Time.now.hour == 0 # run at midnight
    require 'rubygems'  
		require 'nokogiri'  
		require 'open-uri'
		puts "Running..."
		Time.zone = 'Eastern Time (US & Canada)'
		#games = Game.where(:start_time => ((Time.now - 365.days)..(Time.now + 1.hour)))
		games = Game.where('start_time <= :now', :now => Time.now) + Game.where(start_time: nil)
		games.each do |game|
			puts "\n#{game.id} #{game.espn_game_id} #{game.matchup}"
			s = Score.find_or_initialize_by_game_id(game.id)
			if !s.final
				url = "http://scores.espn.go.com/ncf/boxscore?gameId=" + game.espn_game_id.to_s
				doc = Nokogiri::HTML(open(url))  
				puts doc.at_css("title").text
				status = doc.at_css(".game-state").text
				away_score = doc.xpath('//div[@class="team away"]/div[@class="team-info"]/h3/span').text.gsub(/^(\D.{1,2}\D)/,'')
				home_score = doc.xpath('//div[@class="team home"]/div[@class="team-info"]/h3/span').text.gsub(/^(\D.{1,2}\D)/,'')
				game_time = doc.xpath('//div[@class="game-time-location"]/p').first.text
				if away_score.empty?
					away_score = 0
					home_score = 0
				end
				puts "Status: #{status}"
				puts "Game Time: #{game_time}"
				puts "Score: #{away_score} - #{home_score}"

				if status[0..4] == "Final"
					puts "Game is Final" + "\n"
					s.update_attributes(:away_score => away_score, :home_score => home_score, :status => status, :final => true)
				else
					puts "Game is not Final" + "\n"
					s.update_attributes(:away_score => away_score, :home_score => home_score, :status => status)
				end
			#else
			#puts "Score is already final in game #{game.id} with ESPN id #{game.espn_game_id}"
			s.save!
			game.update_attributes(:start_time => Time.zone.parse(game_time))
			puts "Game Time now: #{game.start_time.in_time_zone('Eastern Time (US & Canada)')}"
			game_time = ''
			end
		end
    puts "done at #{Time.now.in_time_zone('Central Time (US & Canada)')}."
  #end
#end