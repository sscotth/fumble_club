
#!/bin/env ruby
# encoding: utf-8

require 'rubygems'
require 'nokogiri'
require 'open-uri'
include ApplicationHelper

espn_to_fumble = {103 => 1, 228 => 2, 150 => 3, 52 => 4, 59 => 5, 120 => 6, 2390 => 7, 153 => 8, 152 => 9, 258 => 10, 259 => 11, 154 => 12, 239 => 13, 66 => 14, 2305 => 15, 2306 => 16, 142 => 17, 201 => 18, 197 => 19, 251 => 20, 245 => 21, 2641 => 22, 2132 => 23, 41 => 24, 97 => 25, 221 => 26, 164 => 27, 58 => 28, 183 => 29, 277 => 30, 356 => 31, 84 => 32, 2294 => 33, 130 => 34, 127 => 35, 135 => 36, 158 => 37, 77 => 38, 194 => 39, 213 => 40, 2509 => 41, 275 => 42, 151 => 43, 248 => 44, 276 => 45, 235 => 46, 242 => 47, 2567 => 48, 2572 => 49, 2655 => 50, 202 => 51, 5 => 52, 2116 => 53, 2638 => 54, 349 => 55, 252 => 56, 2426 => 57, 87 => 58, 2006 => 59, 2050 => 60, 189 => 61, 2084 => 62, 2117 => 63, 2199 => 64, 2309 => 65, 193 => 66, 2459 => 67, 195 => 68, 218 => 69, 2649 => 70, 2711 => 71, 2005 => 72, 68 => 73, 36 => 74, 167 => 75, 21 => 76, 2628 => 77, 2439 => 78, 2751 => 79, 12 => 80, 9 => 81, 25 => 82, 38 => 83, 2483 => 84, 204 => 85, 24 => 86, 26 => 87, 30 => 88, 254 => 89, 264 => 90, 265 => 91, 333 => 92, 8 => 93, 2 => 94, 57 => 95, 61 => 96, 96 => 97, 99 => 98, 344 => 99, 145 => 100, 2579 => 101, 2633 => 102, 238 => 103, 2032 => 104, 2226 => 105, 2229 => 106, 309 => 107, 2433 => 108, 2393 => 109, 249 => 110, 2653 => 111, 98 => 112, 278 => 113, 62 => 114, 70 => 115, 2348 => 116, 2440 => 117, 166 => 118, 23 => 119, 328 => 120, 331 => 121, 304 => 122, 149 => 123, 147 => 124, 2464 => 125, 2458 => 126, 2502 => 127, 16 => 128, 2692 => 129, 2127 => 130, 324 => 131, 2241 => 132, 2335 => 133, 2506 => 134, 2619 => 135, 2678 => 136, 48 => 137, 256 => 138, 311 => 139, 113 => 140, 160 => 141, 295 => 142, 227 => 143, 257 => 144, 119 => 145, 222 => 146, 2729 => 147, 13 => 148, 155 => 149, 233 => 150, 253 => 151, 302 => 152, 2247 => 153, 6 => 154, 326 => 155, 2636 => 156, 225 => 157, 171 => 158, 172 => 159, 159 => 160, 108 => 161, 219 => 162, 163 => 163, 43 => 164, 2065 => 165, 2169 => 166, 50 => 167, 2261 => 168, 47 => 169, 2415 => 170, 2450 => 171, 2448 => 172, 2428 => 173, 2542 => 174, 2569 => 175, 2287 => 176, 282 => 177, 2623 => 178, 2449 => 179, 2460 => 180, 2571 => 181, 79 => 182, 2710 => 183, 2754 => 184, 399 => 185, 2803 => 186, 2115 => 187, 2184 => 188, 2405 => 189, 2523 => 190, 2529 => 191, 2598 => 192, 2681 => 193, 2046 => 194, 2197 => 195, 2198 => 196, 55 => 197, 93 => 198, 2546 => 199, 2634 => 200, 2635 => 201, 2630 => 202, 2083 => 203, 2142 => 204, 2230 => 205, 46 => 206, 107 => 207, 322 => 208, 2329 => 209, 2086 => 210, 2097 => 211, 2166 => 212, 2168 => 213, 2181 => 214, 294 => 215, 2368 => 216, 2413 => 217, 301 => 218, 2674 => 219, 2026 => 220, 236 => 221, 2643 => 222, 2210 => 223, 231 => 224, 290 => 225, 2535 => 226, 2717 => 227, 2747 => 228, 2110 => 229, 2320 => 230, 2377 => 231, 2447 => 232, 2466 => 233, 2534 => 234, 2545 => 235, 2617 => 236, 2010 => 237, 2011 => 238, 2016 => 239, 2029 => 240, 2755 => 241, 2296 => 242, 2400 => 243, 2504 => 244, 2582 => 245, 2640 => 246, 241 => 247, 2695 => 248, 2815 => 249, 2846 => 250, 2013 => 251, 2278 => 252, 2324 => 253, 2595 => 254, 2233 => 255, 2822 => 256, 3077 => 257, 2865 => 258, 2170 => 259, 196 => 260, 3099 => 261, 2355 => 262, 2271 => 263, 2805 => 264, 2583 => 265, 2790 => 266, 2584 => 267, 2232 => 268, 2913 => 269, 330 => 270, 2637 => 271, 2049 => 272, 2373 => 273, 2119 => 274, 3111 => 275, 2022 => 276, 60 => 277, 223 => 278, 2839 => 279, 2714 => 280, 2107 => 281, 2551 => 282, 2369 => 283, 2237 => 284, 2331 => 285, 2254 => 286, 2148 => 287, 2948 => 288, 2703 => 289, 2841 => 290, 620 => 291, 568 => 292, 2823 => 293, 2627 => 294, 2657 => 295, 2402 => 296, 2441 => 297, 2916 => 298, 2894 => 299, 487 => 300, 2069 => 301, 2304 => 302, 2316 => 303, 284 => 304, 2701 => 305, 2122 => 306, 2842 => 307, 2830 => 308, 2930 => 309, 2860 => 310, 2848 => 311, 2560 => 312, 2927 => 313, 613 => 314, 2601 => 315, 2123 => 316, 2104 => 317, 2220 => 318, 2896 => 319, 2487 => 320, 2804 => 321, 2837 => 322, 2825 => 323, 11 => 324, 2292 => 325, 2424 => 326, 2707 => 327, 2028 => 328, 2339 => 329, 2691 => 330, 510 => 331, 2206 => 332, 2249 => 333, 209 => 334, 419 => 335}


week_date_2012 = {1 => 20120828,
									2 => 20120904,
									3 => 20120911,
									4 => 20120918,
									5 => 20120925,
									6 => 20121002,
									7 => 20121009, 
									8 => 20121016,
									9 => 20121023,
									10 => 20121030,
									11 => 20121106,
									12 => 20121113,
									13 => 20121120,
									14 => 20121127,
									15 => 20121204}
exceptions = ''
Time.zone = 'Eastern Time (US & Canada)'

# define_week
Week.all.each do |current_week|
	wk = current_week
	week = week_date_2012[current_week.id]
	puts week
	url = "http://m.espn.go.com/wireless/"
	doc = Nokogiri::HTML(open(url))
	url = "http://m.espn.go.com/ncf/scoreboard?date=#{week}&groupId=90"
	puts "Parsing Week #{wk.id}: #{url}"
	doc = Nokogiri::HTML(open(url))
	doc.css('div.daysgames').each do |n|
	  date = n.at_css(".day-head").text
	  n.css('div.score-container').each do |nn|
	  	t = nn.at_css('.snap-1').text.gsub(/TBD/,'11:59:59 PM')
	  	espna = nn.attribute('class').to_s.split(' ')[2].split('-')[1]
	  	espnh = nn.attribute('class').to_s.split(' ')[3].split('-')[1]
	  	a = espn_to_fumble[espna.to_i]
	  	h = espn_to_fumble[espnh.to_i]
	  	espn_id = nn.attribute('data-objid')
	  	t = t.to_s
	  	puts "t: '#{t}'"
	  	if !t.include?('AM') & !t.include?('PM') 
	  		t = '12:00 PM'
	  	end
	  	puts "t: #{t}"
	  	puts "espna: #{espna}"
	  	puts "espnh: #{espnh}"
	  	puts "a: #{a}"
	  	puts "h: #{h}"
	  	puts "espn_id: #{espn_id}"
	  	#g = "Game.create! :week_id => #{week[0]}, :away_team_id => #{a}, :home_team_id => #{h}, :start_time => Time.zone.parse('#{date} #{t}'), :espn_game_id => #{espn_id}\n"
	  	if a.nil? || t == 'PPD'
	  		exceptions << "#{espn_id} = #{t}, "
	  	else
	  		g = Game.find_or_initialize_by_espn_game_id(espn_id.to_s.to_i)
	  		gg = Game.where(week_id: wk, away_team_id: a, home_team_id: h)[0]
	  		gh = Game.where(week_id: wk, away_team_id: h, home_team_id: a)[0]
	  		if g.new_record?
	  		  if gg
	  		  	puts "ESPN GAME ID from #{gg.espn_game_id} to #{espn_id}"
	  		  	gg.update_attributes espn_game_id: espn_id.to_s.to_i
	  		  	gg.update_attributes start_time: Time.zone.parse("#{date} #{t}")	
	  		  elsif gh
	  		  	puts "swap home/away and change espn id from #{gh.espn_game_id} to #{espn_id}"
	  		  	gh.update_attributes away_team_id: h, home_team_id: a, espn_game_id: espn_id.to_s.to_i
	  		  	gh.update_attributes start_time: Time.zone.parse("#{date} #{t}")	
	  		  else
	  		  	puts "NEW ESPN: #{espn_id} y to confirm"
	  		  	confirm = gets
	  		  	if confirm.chomp! == 'y'
	  		  		puts "Game.create! week_id: #{wk.id}, away_team_id: #{a}, home_team_id: #{h}, start_time: Time.zone.parse(\"#{date} #{t}\")}, espn_game_id: #{espn_id}"
	  		  		puts 'y to confirm again'
	  		  		confirm2 = gets
	  		  		if confirm2.chomp! == 'y'
	  		  		ggg = Game.create! week_id: wk.id, away_team_id: a, home_team_id: h, start_time: Time.zone.parse("#{date} #{t}"), espn_game_id: espn_id.to_s.to_i
	  		  		puts ggg.matchup
	  		  		end
	  		  	end
	  		  	#puts "new away: #{Team.find(a).name}"
	  		  	#puts "new home: #{Team.find(h).name}"
	  		  	#g.week_id = week[0]
	  		  	#g.away_team_id = a
	  		  	#g.home_team_id = h
	  		  	#g.start_time = Time.zone.parse("#{date} #{t}")	
	  		  	#g.save
	  		  end
	  		else # found espn game id
	  			if !g.final
	  				if gg && gh
	  					gg.delete
	  				end
	  			  	g.update_attributes :away_team_id => a, :home_team_id => h, :start_time => Time.zone.parse("#{date} #{t}")	
	  			end	
	  		end
	  		puts "Game: #{g.id}"
	   	end
	  end
	end
end

puts "Exceptions: #{exceptions}"
