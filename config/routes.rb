FumbleS2::Application.routes.draw do
  
  ActiveAdmin.routes(self)

  resources :picks
  resources :week, :only => [:index, :show]
  resources :standingslive, :only => :index

  #match '/users', :to => 'standings#index'
  match '/user/:id', :to => 'users#show'
  match '/user' => redirect('/users')

  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"
  devise_for :users
  resources :users, :only => [:show, :index]
  resources :results, :only => [:show, :index]
end
