class CreateDivisions < ActiveRecord::Migration
	def change
		create_table :divisions do |t|
		  t.string :name
		  t.integer :season_year
		  t.timestamps
		end

		add_index :divisions, [:name, :season_year], :unique => true
	end
end
