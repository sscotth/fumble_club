class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|

    	t.integer :game_id
    	t.integer :away_score, :default => 0
    	t.integer :home_score, :default => 0
    	t.string :status, :default => 'Pending'
    	t.boolean :final, :default => false

      t.timestamps
    end

    add_index :scores, :game_id, :unique => true
  end
end
