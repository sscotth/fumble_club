class CreateConferences < ActiveRecord::Migration
  def change
    create_table :conferences do |t|

    	t.string :name
    	t.string :league

      t.timestamps
    end

    add_index :conferences, :name, :unique => true
  end
end
