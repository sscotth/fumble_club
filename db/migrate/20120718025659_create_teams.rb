class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|

    	t.string :name
   		t.integer  :conference_id
      t.boolean :pick_required, :default => false
      t.integer :espn_team_id

      t.timestamps
    end

    add_index :teams, :name, :unique => true
  end
end
