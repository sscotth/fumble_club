class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.integer :user_id, :null => false
      t.integer :week_id, :null => false
      t.integer :wins, :null => false
      t.integer :losses, :null => false
      t.integer :points, :null => false
      t.integer :earnings

      t.timestamps
    end

    add_index :results, [:user_id, :week_id], :unique => true
  end
end
