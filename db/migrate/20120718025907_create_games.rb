class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|

    	t.integer :week_id
   		t.integer :away_team_id
   		t.integer :home_team_id
   		t.datetime	:start_time
   		t.boolean :points_req, :default => false
      t.integer :espn_game_id
      t.boolean :force_no_pick, :default => false #use for thursday games
      t.boolean :force_pick, :default => false #use for bowl games & I-AA playoffs

      t.timestamps
    end

    add_index :games, :espn_game_id, :unique => true
    add_index :games, [:home_team_id, :away_team_id, :week_id], :unique => true

  end
end
