class CreateWeeks < ActiveRecord::Migration
  def change
    create_table :weeks do |t|

      t.integer :season_year
    	t.integer :season_week
    	t.datetime :pick_deadline

      t.timestamps
    end

    add_index :weeks, [:season_year, :season_week], :unique => true
  end
end
