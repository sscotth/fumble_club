class AddFullToPenalties < ActiveRecord::Migration
  def change
    add_column :penalties, :full, :boolean, :default => false
  end
end
