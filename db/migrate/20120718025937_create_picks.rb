class CreatePicks < ActiveRecord::Migration
  def change
    create_table :picks do |t|

    	t.integer :user_id
	    t.integer :game_id
	    t.boolean :home_win_pick, :null => false
	    t.integer :away_score_pick
	    t.integer :home_score_pick

      t.timestamps
    end

    add_index :picks, [:user_id, :game_id], :unique => true
  end
end
