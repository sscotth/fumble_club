class CreatePenalties < ActiveRecord::Migration
  def change
    create_table :penalties do |t|

    	t.integer :week_id
    	t.integer :user_id

      t.timestamps
    end
    
    add_index :penalties, [:week_id, :user_id], :unique => true
  end
end
