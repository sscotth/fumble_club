# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121113022459) do

  create_table "active_admin_comments", :force => true do |t|
    t.string    "resource_id",   :null => false
    t.string    "resource_type", :null => false
    t.integer   "author_id"
    t.string    "author_type"
    t.text      "body"
    t.timestamp "created_at",    :null => false
    t.timestamp "updated_at",    :null => false
    t.string    "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string    "email",                  :default => "", :null => false
    t.string    "encrypted_password",     :default => "", :null => false
    t.string    "reset_password_token"
    t.timestamp "reset_password_sent_at"
    t.timestamp "remember_created_at"
    t.integer   "sign_in_count",          :default => 0
    t.timestamp "current_sign_in_at"
    t.timestamp "last_sign_in_at"
    t.string    "current_sign_in_ip"
    t.string    "last_sign_in_ip"
    t.timestamp "created_at",                             :null => false
    t.timestamp "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "conferences", :force => true do |t|
    t.string    "name"
    t.string    "league"
    t.timestamp "created_at", :null => false
    t.timestamp "updated_at", :null => false
  end

  add_index "conferences", ["name"], :name => "index_conferences_on_name", :unique => true

  create_table "divisions", :force => true do |t|
    t.string    "name"
    t.integer   "season_year"
    t.timestamp "created_at",  :null => false
    t.timestamp "updated_at",  :null => false
  end

  add_index "divisions", ["name", "season_year"], :name => "index_divisions_on_name_and_season_year", :unique => true

  create_table "games", :force => true do |t|
    t.integer   "week_id"
    t.integer   "away_team_id"
    t.integer   "home_team_id"
    t.timestamp "start_time"
    t.boolean   "points_req",    :default => false
    t.integer   "espn_game_id"
    t.boolean   "force_no_pick", :default => false
    t.boolean   "force_pick",    :default => false
    t.timestamp "created_at",                       :null => false
    t.timestamp "updated_at",                       :null => false
  end

  add_index "games", ["espn_game_id"], :name => "index_games_on_espn_game_id", :unique => true
  add_index "games", ["home_team_id", "away_team_id", "week_id"], :name => "index_games_on_home_team_id_and_away_team_id_and_week_id", :unique => true

  create_table "penalties", :force => true do |t|
    t.integer   "week_id"
    t.integer   "user_id"
    t.timestamp "created_at",                    :null => false
    t.timestamp "updated_at",                    :null => false
    t.boolean   "full",       :default => false
  end

  add_index "penalties", ["week_id", "user_id"], :name => "index_penalties_on_week_id_and_user_id", :unique => true

  create_table "picks", :force => true do |t|
    t.integer   "user_id"
    t.integer   "game_id"
    t.boolean   "home_win_pick",   :null => false
    t.integer   "away_score_pick"
    t.integer   "home_score_pick"
    t.timestamp "created_at",      :null => false
    t.timestamp "updated_at",      :null => false
  end

  add_index "picks", ["user_id", "game_id"], :name => "index_picks_on_user_id_and_game_id", :unique => true

  create_table "results", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.integer  "week_id",    :null => false
    t.integer  "wins",       :null => false
    t.integer  "losses",     :null => false
    t.integer  "points",     :null => false
    t.integer  "earnings"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "results", ["user_id", "week_id"], :name => "index_results_on_user_id_and_week_id", :unique => true

  create_table "roles", :force => true do |t|
    t.string    "name"
    t.integer   "resource_id"
    t.string    "resource_type"
    t.timestamp "created_at",    :null => false
    t.timestamp "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "scores", :force => true do |t|
    t.integer   "game_id"
    t.integer   "away_score", :default => 0
    t.integer   "home_score", :default => 0
    t.string    "status",     :default => "Pending"
    t.boolean   "final",      :default => false
    t.timestamp "created_at",                        :null => false
    t.timestamp "updated_at",                        :null => false
  end

  add_index "scores", ["game_id"], :name => "index_scores_on_game_id", :unique => true

  create_table "seasons", :force => true do |t|
    t.integer   "year"
    t.timestamp "created_at", :null => false
    t.timestamp "updated_at", :null => false
  end

  create_table "teams", :force => true do |t|
    t.string    "name"
    t.integer   "conference_id"
    t.boolean   "pick_required", :default => false
    t.integer   "espn_team_id"
    t.timestamp "created_at",                       :null => false
    t.timestamp "updated_at",                       :null => false
  end

  add_index "teams", ["name"], :name => "index_teams_on_name", :unique => true

  create_table "users", :force => true do |t|
    t.string    "email",                  :default => "", :null => false
    t.string    "encrypted_password",     :default => "", :null => false
    t.string    "reset_password_token"
    t.timestamp "reset_password_sent_at"
    t.timestamp "remember_created_at"
    t.integer   "sign_in_count",          :default => 0
    t.timestamp "current_sign_in_at"
    t.timestamp "last_sign_in_at"
    t.string    "current_sign_in_ip"
    t.string    "last_sign_in_ip"
    t.timestamp "created_at",                             :null => false
    t.timestamp "updated_at",                             :null => false
    t.string    "name"
    t.string    "confirmation_token"
    t.timestamp "confirmed_at"
    t.timestamp "confirmation_sent_at"
    t.string    "unconfirmed_email"
    t.integer   "team_id"
    t.integer   "division_id"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "weeks", :force => true do |t|
    t.integer   "season_year"
    t.integer   "season_week"
    t.timestamp "pick_deadline"
    t.timestamp "created_at",    :null => false
    t.timestamp "updated_at",    :null => false
  end

  add_index "weeks", ["season_year", "season_week"], :name => "index_weeks_on_season_year_and_season_week", :unique => true

end
