# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
Time.zone = 'Eastern Time (US & Canada)'
puts Time.zone
puts 'SETTING UP DEFAULT USER LOGINS'
# user = User.create! :name => 'First User', :email => 'user@example.com', :password => 'please', :password_confirmation => 'please'
# user.add_role :admin

# user = User.create! :name => 'Michael Johnson', :email => 'mj1101@gmail.com', :password => 'please', :password_confirmation => 'please', :team_id => 95, :division_id => 1
# user = User.create! :name => 'Scott Randles', :email => 'skrandles@bellsouth.net', :password => 'please', :password_confirmation => 'please', :team_id => 226, :division_id => 1
# user = User.create! :name => 'Chris Chamberlain', :email => 'jcchamberlain@gmail.com', :password => 'please', :password_confirmation => 'please', :team_id => 88, :division_id => 1
# user = User.create! :name => 'Charles Baggs', :email => 'charbaggs@comcast.net', :password => 'please', :password_confirmation => 'please', :team_id => 96, :division_id => 1
# user = User.create! :name => 'Bobby Bray', :email => 'bobby.j.bray@gmail.com', :password => 'please', :password_confirmation => 'please', :team_id => 57, :division_id => 1

# user = User.create! :name => 'Scott Humphries', :email => 'sscotth@gmail.com', :password => 'please', :password_confirmation => 'please', :team_id => 109, :division_id => 2
# user = User.create! :name => 'David Chamberlain', :email => 'djchambe@gmail.com', :password => 'please', :password_confirmation => 'please', :team_id => 41, :division_id => 2
# user = User.create! :name => 'Travis Johnson', :email => 'travisjohnsonesq@gmail.com', :password => 'please', :password_confirmation => 'please', :team_id => 95, :division_id => 2
# user = User.create! :name => 'Bill Bade', :email => 'huskerpower1984@yahoo.com', :password => 'please', :password_confirmation => 'please', :team_id => 37, :division_id => 2
# user = User.create! :name => 'Patrick Keough', :email => 'pkeough1553@gmail.com', :password => 'please', :password_confirmation => 'please', :team_id => 32, :division_id => 2

# user = User.create! :name => 'Lee Baggs', :email => 'baggsnlb@comcast.net', :password => 'please', :password_confirmation => 'please', :team_id => 225, :division_id => 3
# user = User.create! :name => 'John Johnson', :email => 'dbjljgator@bellsouth.net', :password => 'please', :password_confirmation => 'please', :team_id => 95, :division_id => 3
# user = User.create! :name => 'Zach Smith', :email => 'zach.smith@optum.com', :password => 'please', :password_confirmation => 'please', :team_id => 4, :division_id => 3
# user = User.create! :name => 'Jeff Chamberlain', :email => 'jeff.chamberlain@aprimo.com', :password => 'please', :password_confirmation => 'please', :team_id => 41, :division_id => 3
# user = User.create! :name => 'Philip Nelson', :email => 'nelsonpg@muohio.edu', :password => 'please', :password_confirmation => 'please', :team_id => 66, :division_id => 3

# user = User.create! :name => 'Joey Nichols', :email => 'jlncbn@att.net', :password => 'please', :password_confirmation => 'please', :team_id => 224, :division_id => 4
# user = User.create! :name => 'Mark Wessel', :email => 'mark.wessel@rbc.com', :password => 'please', :password_confirmation => 'please', :team_id => 222, :division_id => 4
# user = User.create! :name => 'Kenneth Wessel', :email => 'kenny.wessel11@gmail.com', :password => 'please', :password_confirmation => 'please', :team_id => 133, :division_id => 4
# user = User.create! :name => 'Paul Swenson', :email => 'paul.swenson@swensoncpa.com', :password => 'please', :password_confirmation => 'please', :team_id => 224, :division_id => 4
# user = User.create! :name => 'Steve Pynne', :email => 'steve.pynne@pynnecpa.com', :password => 'please', :password_confirmation => 'please', :team_id => 134, :division_id => 4


puts 'SETTING UP SEASONS'

s = Season.create! :year => 2012
s = Season.create! :year => 2011

puts 'SETTING UP CONFERENCE HISTORY'
# need Many to Many table
# c = Team_to_conference.create! :team_id => 17, :conference2012 => 10, :conference2011 => 2
# c = Team_to_conference.create! :team_id => 1, :conference2012 => 1, :conference2011 => 1 

puts 'SETTING UP DEFAULT TEAMS'

t = Team.create! :name => 'Boston College', :espn_team_id => 103 # id = 1
t = Team.create! :name => 'Clemson', :espn_team_id => 228 # id = 2
t = Team.create! :name => 'Duke', :espn_team_id => 150 # id = 3
t = Team.create! :name => 'Florida State', :espn_team_id => 52 # id = 4
t = Team.create! :name => 'Georgia Tech', :espn_team_id => 59 # id = 5
t = Team.create! :name => 'Maryland', :espn_team_id => 120 # id = 6
t = Team.create! :name => 'Miami (FL)', :espn_team_id => 2390 # id = 7
t = Team.create! :name => 'North Carolina', :espn_team_id => 153 # id = 8
t = Team.create! :name => 'North Carolina State', :espn_team_id => 152 # id = 9
t = Team.create! :name => 'Virginia', :espn_team_id => 258 # id = 10
t = Team.create! :name => 'Virginia Tech', :espn_team_id => 259 # id = 11
t = Team.create! :name => 'Wake Forest', :espn_team_id => 154 # id = 12
t = Team.create! :name => 'Baylor', :espn_team_id => 239 # id = 13
t = Team.create! :name => 'Iowa State', :espn_team_id => 66 # id = 14
t = Team.create! :name => 'Kansas', :espn_team_id => 2305 # id = 15
t = Team.create! :name => 'Kansas State', :espn_team_id => 2306 # id = 16
t = Team.create! :name => 'Missouri', :espn_team_id => 142 # id = 17
t = Team.create! :name => 'Oklahoma', :espn_team_id => 201 # id = 18
t = Team.create! :name => 'Oklahoma State', :espn_team_id => 197 # id = 19
t = Team.create! :name => 'Texas', :espn_team_id => 251 # id = 20
t = Team.create! :name => 'Texas A&M', :espn_team_id => 245 # id = 21
t = Team.create! :name => 'Texas Tech', :espn_team_id => 2641 # id = 22
t = Team.create! :name => 'Cincinnati', :espn_team_id => 2132 # id = 23
t = Team.create! :name => 'Connecticut', :espn_team_id => 41 # id = 24
t = Team.create! :name => 'Louisville', :espn_team_id => 97 # id = 25
t = Team.create! :name => 'Pittsburgh', :espn_team_id => 221 # id = 26
t = Team.create! :name => 'Rutgers', :espn_team_id => 164 # id = 27
t = Team.create! :name => 'South Florida', :espn_team_id => 58 # id = 28
t = Team.create! :name => 'Syracuse', :espn_team_id => 183 # id = 29
t = Team.create! :name => 'West Virginia', :espn_team_id => 277 # id = 30
t = Team.create! :name => 'Illinois', :espn_team_id => 356 # id = 31
t = Team.create! :name => 'Indiana', :espn_team_id => 84 # id = 32
t = Team.create! :name => 'Iowa', :espn_team_id => 2294 # id = 33
t = Team.create! :name => 'Michigan', :espn_team_id => 130 # id = 34
t = Team.create! :name => 'Michigan State', :espn_team_id => 127 # id = 35
t = Team.create! :name => 'Minnesota', :espn_team_id => 135 # id = 36
t = Team.create! :name => 'Nebraska', :espn_team_id => 158 # id = 37
t = Team.create! :name => 'Northwestern', :espn_team_id => 77 # id = 38
t = Team.create! :name => 'Ohio State', :espn_team_id => 194 # id = 39
t = Team.create! :name => 'Penn State', :espn_team_id => 213 # id = 40
t = Team.create! :name => 'Purdue', :espn_team_id => 2509 # id = 41
t = Team.create! :name => 'Wisconsin', :espn_team_id => 275 # id = 42
t = Team.create! :name => 'East Carolina', :espn_team_id => 151 # id = 43
t = Team.create! :name => 'Houston', :espn_team_id => 248 # id = 44
t = Team.create! :name => 'Marshall', :espn_team_id => 276 # id = 45
t = Team.create! :name => 'Memphis', :espn_team_id => 235 # id = 46
t = Team.create! :name => 'Rice', :espn_team_id => 242 # id = 47
t = Team.create! :name => 'Southern Methodist', :espn_team_id => 2567 # id = 48
t = Team.create! :name => 'Southern Miss', :espn_team_id => 2572 # id = 49
t = Team.create! :name => 'Tulane', :espn_team_id => 2655 # id = 50
t = Team.create! :name => 'Tulsa', :espn_team_id => 202 # id = 51
t = Team.create! :name => 'UAB', :espn_team_id => 5 # id = 52
t = Team.create! :name => 'UCF', :espn_team_id => 2116 # id = 53
t = Team.create! :name => 'UTEP', :espn_team_id => 2638 # id = 54
t = Team.create! :name => 'Army', :espn_team_id => 349 # id = 55
t = Team.create! :name => 'Brigham Young', :espn_team_id => 252 # id = 56
t = Team.create! :name => 'Navy', :espn_team_id => 2426 # id = 57
t = Team.create! :name => 'Notre Dame', :espn_team_id => 87 # id = 58
t = Team.create! :name => 'Akron', :espn_team_id => 2006 # id = 59
t = Team.create! :name => 'Ball State', :espn_team_id => 2050 # id = 60
t = Team.create! :name => 'Bowling Green', :espn_team_id => 189 # id = 61
t = Team.create! :name => 'Buffalo', :espn_team_id => 2084 # id = 62
t = Team.create! :name => 'Central Michigan', :espn_team_id => 2117 # id = 63
t = Team.create! :name => 'Eastern Michigan', :espn_team_id => 2199 # id = 64
t = Team.create! :name => 'Kent State', :espn_team_id => 2309 # id = 65
t = Team.create! :name => 'Miami (OH)', :espn_team_id => 193 # id = 66
t = Team.create! :name => 'Northern Illinois', :espn_team_id => 2459 # id = 67
t = Team.create! :name => 'Ohio', :espn_team_id => 195 # id = 68
t = Team.create! :name => 'Temple', :espn_team_id => 218 # id = 69
t = Team.create! :name => 'Toledo', :espn_team_id => 2649 # id = 70
t = Team.create! :name => 'Western Michigan', :espn_team_id => 2711 # id = 71
t = Team.create! :name => 'Air Force', :espn_team_id => 2005 # id = 72
t = Team.create! :name => 'Boise State', :espn_team_id => 68 # id = 73
t = Team.create! :name => 'Colorado State', :espn_team_id => 36 # id = 74
t = Team.create! :name => 'New Mexico', :espn_team_id => 167 # id = 75
t = Team.create! :name => 'San Diego State', :espn_team_id => 21 # id = 76
t = Team.create! :name => 'TCU', :espn_team_id => 2628 # id = 77
t = Team.create! :name => 'UNLV', :espn_team_id => 2439 # id = 78
t = Team.create! :name => 'Wyoming', :espn_team_id => 2751 # id = 79
t = Team.create! :name => 'Arizona', :espn_team_id => 12 # id = 80
t = Team.create! :name => 'Arizona State', :espn_team_id => 9 # id = 81
t = Team.create! :name => 'California', :espn_team_id => 25 # id = 82
t = Team.create! :name => 'Colorado', :espn_team_id => 38 # id = 83
t = Team.create! :name => 'Oregon', :espn_team_id => 2483 # id = 84
t = Team.create! :name => 'Oregon State', :espn_team_id => 204 # id = 85
t = Team.create! :name => 'Stanford', :espn_team_id => 24 # id = 86
t = Team.create! :name => 'UCLA', :espn_team_id => 26 # id = 87
t = Team.create! :name => 'USC', :espn_team_id => 30 # id = 88
t = Team.create! :name => 'Utah', :espn_team_id => 254 # id = 89
t = Team.create! :name => 'Washington', :espn_team_id => 264 # id = 90
t = Team.create! :name => 'Washington State', :espn_team_id => 265 # id = 91
t = Team.create! :name => 'Alabama', :espn_team_id => 333 # id = 92
t = Team.create! :name => 'Arkansas', :espn_team_id => 8 # id = 93
t = Team.create! :name => 'Auburn', :espn_team_id => 2 # id = 94
t = Team.create! :name => 'Florida', :espn_team_id => 57 # id = 95
t = Team.create! :name => 'Georgia', :espn_team_id => 61 # id = 96
t = Team.create! :name => 'Kentucky', :espn_team_id => 96 # id = 97
t = Team.create! :name => 'LSU', :espn_team_id => 99 # id = 98
t = Team.create! :name => 'Mississippi State', :espn_team_id => 344 # id = 99
t = Team.create! :name => 'Ole Miss', :espn_team_id => 145 # id = 100
t = Team.create! :name => 'South Carolina', :espn_team_id => 2579 # id = 101
t = Team.create! :name => 'Tennessee', :espn_team_id => 2633 # id = 102
t = Team.create! :name => 'Vanderbilt', :espn_team_id => 238 # id = 103
t = Team.create! :name => 'Arkansas State', :espn_team_id => 2032 # id = 104
t = Team.create! :name => 'Florida Atlantic', :espn_team_id => 2226 # id = 105
t = Team.create! :name => 'Florida International', :espn_team_id => 2229 # id = 106
t = Team.create! :name => 'Louisiana-Lafayette', :espn_team_id => 309 # id = 107
t = Team.create! :name => 'Louisiana-Monroe', :espn_team_id => 2433 # id = 108
t = Team.create! :name => 'Middle Tennessee', :espn_team_id => 2393 # id = 109
t = Team.create! :name => 'North Texas', :espn_team_id => 249 # id = 110
t = Team.create! :name => 'Troy', :espn_team_id => 2653 # id = 111
t = Team.create! :name => 'Western Kentucky', :espn_team_id => 98 # id = 112
t = Team.create! :name => 'Fresno State', :espn_team_id => 278 # id = 113
t = Team.create! :name => 'Hawaii', :espn_team_id => 62 # id = 114
t = Team.create! :name => 'Idaho', :espn_team_id => 70 # id = 115
t = Team.create! :name => 'Louisiana Tech', :espn_team_id => 2348 # id = 116
t = Team.create! :name => 'Nevada', :espn_team_id => 2440 # id = 117
t = Team.create! :name => 'New Mexico State', :espn_team_id => 166 # id = 118
t = Team.create! :name => 'San Jose State', :espn_team_id => 23 # id = 119
t = Team.create! :name => 'Utah State', :espn_team_id => 328 # id = 120
t = Team.create! :name => 'Eastern Washington', :espn_team_id => 331 # id = 121
t = Team.create! :name => 'Idaho State', :espn_team_id => 304 # id = 122
t = Team.create! :name => 'Montana', :espn_team_id => 149 # id = 123
t = Team.create! :name => 'Montana State', :espn_team_id => 147 # id = 124
t = Team.create! :name => 'Northern Arizona', :espn_team_id => 2464 # id = 125
t = Team.create! :name => 'Northern Colorado', :espn_team_id => 2458 # id = 126
t = Team.create! :name => 'Portland State', :espn_team_id => 2502 # id = 127
t = Team.create! :name => 'Sacramento State', :espn_team_id => 16 # id = 128
t = Team.create! :name => 'Weber State', :espn_team_id => 2692 # id = 129
t = Team.create! :name => 'Charleston Southern', :espn_team_id => 2127 # id = 130
t = Team.create! :name => 'Coastal Carolina', :espn_team_id => 324 # id = 131
t = Team.create! :name => 'Gardner-Webb', :espn_team_id => 2241 # id = 132
t = Team.create! :name => 'Liberty', :espn_team_id => 2335 # id = 133
t = Team.create! :name => 'Presbyterian', :espn_team_id => 2506 # id = 134
t = Team.create! :name => 'Stony Brook', :espn_team_id => 2619 # id = 135
t = Team.create! :name => 'Virginia Military Institute', :espn_team_id => 2678 # id = 136
t = Team.create! :name => 'Delaware', :espn_team_id => 48 # id = 137
t = Team.create! :name => 'James Madison', :espn_team_id => 256 # id = 138
t = Team.create! :name => 'Maine', :espn_team_id => 311 # id = 139
t = Team.create! :name => 'Massachusetts', :espn_team_id => 113 # id = 140
t = Team.create! :name => 'New Hampshire', :espn_team_id => 160 # id = 141
t = Team.create! :name => 'Old Dominion', :espn_team_id => 295 # id = 142
t = Team.create! :name => 'Rhode Island', :espn_team_id => 227 # id = 143
t = Team.create! :name => 'Richmond', :espn_team_id => 257 # id = 144
t = Team.create! :name => 'Towson', :espn_team_id => 119 # id = 145
t = Team.create! :name => 'Villanova', :espn_team_id => 222 # id = 146
t = Team.create! :name => 'William & Mary', :espn_team_id => 2729 # id = 147
t = Team.create! :name => 'Cal Poly', :espn_team_id => 13 # id = 148
t = Team.create! :name => 'North Dakota', :espn_team_id => 155 # id = 149
t = Team.create! :name => 'South Dakota', :espn_team_id => 233 # id = 150
t = Team.create! :name => 'Southern Utah', :espn_team_id => 253 # id = 151
t = Team.create! :name => 'UC Davis', :espn_team_id => 302 # id = 152
t = Team.create! :name => 'Georgia State', :espn_team_id => 2247 # id = 153
t = Team.create! :name => 'South Alabama', :espn_team_id => 6 # id = 154
t = Team.create! :name => 'Texas State', :espn_team_id => 326 # id = 155
t = Team.create! :name => 'UTSA', :espn_team_id => 2636 # id = 156
t = Team.create! :name => 'Brown', :espn_team_id => 225 # id = 157
t = Team.create! :name => 'Columbia', :espn_team_id => 171 # id = 158
t = Team.create! :name => 'Cornell', :espn_team_id => 172 # id = 159
t = Team.create! :name => 'Dartmouth', :espn_team_id => 159 # id = 160
t = Team.create! :name => 'Harvard', :espn_team_id => 108 # id = 161
t = Team.create! :name => 'Pennsylvania', :espn_team_id => 219 # id = 162
t = Team.create! :name => 'Princeton', :espn_team_id => 163 # id = 163
t = Team.create! :name => 'Yale', :espn_team_id => 43 # id = 164
t = Team.create! :name => 'Bethune-Cookman', :espn_team_id => 2065 # id = 165
t = Team.create! :name => 'Delaware State', :espn_team_id => 2169 # id = 166
t = Team.create! :name => 'Florida A&M', :espn_team_id => 50 # id = 167
t = Team.create! :name => 'Hampton', :espn_team_id => 2261 # id = 168
t = Team.create! :name => 'Howard', :espn_team_id => 47 # id = 169
t = Team.create! :name => 'Morgan State', :espn_team_id => 2415 # id = 170
t = Team.create! :name => 'Norfolk State', :espn_team_id => 2450 # id = 171
t = Team.create! :name => 'North Carolina A&T', :espn_team_id => 2448 # id = 172
t = Team.create! :name => 'North Carolina Central', :espn_team_id => 2428 # id = 173
t = Team.create! :name => 'Savannah State', :espn_team_id => 2542 # id = 174
t = Team.create! :name => 'South Carolina State', :espn_team_id => 2569 # id = 175
t = Team.create! :name => 'Illinois State', :espn_team_id => 2287 # id = 176
t = Team.create! :name => 'Indiana State', :espn_team_id => 282 # id = 177
t = Team.create! :name => 'Missouri State', :espn_team_id => 2623 # id = 178
t = Team.create! :name => 'North Dakota State', :espn_team_id => 2449 # id = 179
t = Team.create! :name => 'Northern Iowa', :espn_team_id => 2460 # id = 180
t = Team.create! :name => 'South Dakota State', :espn_team_id => 2571 # id = 181
t = Team.create! :name => 'Southern Illinois', :espn_team_id => 79 # id = 182
t = Team.create! :name => 'Western Illinois', :espn_team_id => 2710 # id = 183
t = Team.create! :name => 'Youngstown State', :espn_team_id => 2754 # id = 184
t = Team.create! :name => 'Albany', :espn_team_id => 399 # id = 185
t = Team.create! :name => 'Bryant University', :espn_team_id => 2803 # id = 186
t = Team.create! :name => 'Central Connecticut State', :espn_team_id => 2115 # id = 187
t = Team.create! :name => 'Duquesne', :espn_team_id => 2184 # id = 188
t = Team.create! :name => 'Monmouth', :espn_team_id => 2405 # id = 189
t = Team.create! :name => 'Robert Morris', :espn_team_id => 2523 # id = 190
t = Team.create! :name => 'Sacred Heart', :espn_team_id => 2529 # id = 191
t = Team.create! :name => 'St. Francis (PA)', :espn_team_id => 2598 # id = 192
t = Team.create! :name => 'Wagner', :espn_team_id => 2681 # id = 193
t = Team.create! :name => 'Austin Peay', :espn_team_id => 2046 # id = 194
t = Team.create! :name => 'Eastern Illinois', :espn_team_id => 2197 # id = 195
t = Team.create! :name => 'Eastern Kentucky', :espn_team_id => 2198 # id = 196
t = Team.create! :name => 'Jacksonville State', :espn_team_id => 55 # id = 197
t = Team.create! :name => 'Murray State', :espn_team_id => 93 # id = 198
t = Team.create! :name => 'Southeast Missouri State', :espn_team_id => 2546 # id = 199
t = Team.create! :name => 'Tennessee State', :espn_team_id => 2634 # id = 200
t = Team.create! :name => 'Tennessee Tech', :espn_team_id => 2635 # id = 201
t = Team.create! :name => 'Tennessee-Martin', :espn_team_id => 2630 # id = 202
t = Team.create! :name => 'Bucknell', :espn_team_id => 2083 # id = 203
t = Team.create! :name => 'Colgate', :espn_team_id => 2142 # id = 204
t = Team.create! :name => 'Fordham', :espn_team_id => 2230 # id = 205
t = Team.create! :name => 'Georgetown', :espn_team_id => 46 # id = 206
t = Team.create! :name => 'Holy Cross', :espn_team_id => 107 # id = 207
t = Team.create! :name => 'Lafayette', :espn_team_id => 322 # id = 208
t = Team.create! :name => 'Lehigh', :espn_team_id => 2329 # id = 209
t = Team.create! :name => 'Butler', :espn_team_id => 2086 # id = 210
t = Team.create! :name => 'Campbell', :espn_team_id => 2097 # id = 211
t = Team.create! :name => 'Davidson', :espn_team_id => 2166 # id = 212
t = Team.create! :name => 'Dayton', :espn_team_id => 2168 # id = 213
t = Team.create! :name => 'Drake', :espn_team_id => 2181 # id = 214
t = Team.create! :name => 'Jacksonville', :espn_team_id => 294 # id = 215
t = Team.create! :name => 'Marist', :espn_team_id => 2368 # id = 216
t = Team.create! :name => 'Morehead State', :espn_team_id => 2413 # id = 217
t = Team.create! :name => 'San Diego', :espn_team_id => 301 # id = 218
t = Team.create! :name => 'Valparaiso', :espn_team_id => 2674 # id = 219
t = Team.create! :name => 'Appalachian State', :espn_team_id => 2026 # id = 220
t = Team.create! :name => 'Chattanooga', :espn_team_id => 236 # id = 221
t = Team.create! :name => 'Citadel', :espn_team_id => 2643 # id = 222
t = Team.create! :name => 'Elon', :espn_team_id => 2210 # id = 223
t = Team.create! :name => 'Furman', :espn_team_id => 231 # id = 224
t = Team.create! :name => 'Georgia Southern', :espn_team_id => 290 # id = 225
t = Team.create! :name => 'Samford', :espn_team_id => 2535 # id = 226
t = Team.create! :name => 'Western Carolina', :espn_team_id => 2717 # id = 227
t = Team.create! :name => 'Wofford', :espn_team_id => 2747 # id = 228
t = Team.create! :name => 'Central Arkansas', :espn_team_id => 2110 # id = 229
t = Team.create! :name => 'Lamar', :espn_team_id => 2320 # id = 230
t = Team.create! :name => 'McNeese State', :espn_team_id => 2377 # id = 231
t = Team.create! :name => 'Nicholls State', :espn_team_id => 2447 # id = 232
t = Team.create! :name => 'Northwestern State', :espn_team_id => 2466 # id = 233
t = Team.create! :name => 'Sam Houston State', :espn_team_id => 2534 # id = 234
t = Team.create! :name => 'Southeastern Louisiana', :espn_team_id => 2545 # id = 235
t = Team.create! :name => 'Stephen F. Austin', :espn_team_id => 2617 # id = 236
t = Team.create! :name => 'Alabama A&M', :espn_team_id => 2010 # id = 237
t = Team.create! :name => 'Alabama State', :espn_team_id => 2011 # id = 238
t = Team.create! :name => 'Alcorn State', :espn_team_id => 2016 # id = 239
t = Team.create! :name => 'Arkansas-Pine Bluff', :espn_team_id => 2029 # id = 240
t = Team.create! :name => 'Grambling State', :espn_team_id => 2755 # id = 241
t = Team.create! :name => 'Jackson State', :espn_team_id => 2296 # id = 242
t = Team.create! :name => 'Mississippi Valley State', :espn_team_id => 2400 # id = 243
t = Team.create! :name => 'Prairie View A&M', :espn_team_id => 2504 # id = 244
t = Team.create! :name => 'Southern University', :espn_team_id => 2582 # id = 245
t = Team.create! :name => 'Texas Southern', :espn_team_id => 2640 # id = 246
t = Team.create! :name => 'McMurry', :espn_team_id => 241 # id = 247
t = Team.create! :name => 'West Alabama', :espn_team_id => 2695 # id = 248
t = Team.create! :name => 'Lindenwood', :espn_team_id => 2815 # id = 249
t = Team.create! :name => 'Wesley', :espn_team_id => 2846 # id = 250
t = Team.create! :name => 'Albany State (GA)', :espn_team_id => 2013 # id = 251
t = Team.create! :name => 'Humbolt State', :espn_team_id => 2278 # id = 252
t = Team.create! :name => 'Langston', :espn_team_id => 2324 # id = 253
t = Team.create! :name => 'St. Francis (IL)', :espn_team_id => 2595 # id = 254
t = Team.create! :name => 'Franklin', :espn_team_id => 2233 # id = 255
t = Team.create! :name => 'North Greenville', :espn_team_id => 2822 # id = 256
t = Team.create! :name => 'Kentucky Christian', :espn_team_id => 3077 # id = 257
t = Team.create! :name => 'Evangel', :espn_team_id => 2865 # id = 258
t = Team.create! :name => 'Delta State', :espn_team_id => 2170 # id = 259
t = Team.create! :name => 'Northeastern State', :espn_team_id => 196 # id = 260
t = Team.create! :name => 'Concordia College', :espn_team_id => 3099 # id = 261
t = Team.create! :name => 'Lynchburg', :espn_team_id => 2355 # id = 262
t = Team.create! :name => 'Henderson State', :espn_team_id => 2271 # id = 263
t = Team.create! :name => 'Clark Atlanta', :espn_team_id => 2805 # id = 264
t = Team.create! :name => 'Southern Connecticut State', :espn_team_id => 2583 # id = 265
t = Team.create! :name => 'Albion', :espn_team_id => 2790 # id = 266
t = Team.create! :name => 'Southern Oregon', :espn_team_id => 2584 # id = 267
t = Team.create! :name => 'Fort Valley State', :espn_team_id => 2232 # id = 268
t = Team.create! :name => 'Brevard College', :espn_team_id => 2913 # id = 269
t = Team.create! :name => 'Virginia State', :espn_team_id => 330 # id = 270
t = Team.create! :name => 'Texas College', :espn_team_id => 2637 # id = 271
t = Team.create! :name => 'Azusa Pacific', :espn_team_id => 2049 # id = 272
t = Team.create! :name => 'Maryville TN', :espn_team_id => 2373 # id = 273
t = Team.create! :name => 'Central State', :espn_team_id => 2119 # id = 274
t = Team.create! :name => 'Newport News Apprentice', :espn_team_id => 3111 # id = 275
t = Team.create! :name => 'American International', :espn_team_id => 2022 # id = 276
t = Team.create! :name => 'Morehouse', :espn_team_id => 60 # id = 277
t = Team.create! :name => 'West Chester', :espn_team_id => 223 # id = 278
t = Team.create! :name => 'Tusculum', :espn_team_id => 2839 # id = 279
t = Team.create! :name => 'Western State', :espn_team_id => 2714 # id = 280
t = Team.create! :name => 'Catawba', :espn_team_id => 2107 # id = 281
t = Team.create! :name => 'Shaw', :espn_team_id => 2551 # id = 282
t = Team.create! :name => 'Mars Hill', :espn_team_id => 2369 # id = 283
t = Team.create! :name => 'Fort Lewis', :espn_team_id => 2237 # id = 284
t = Team.create! :name => 'Lenoir-Rhyne', :espn_team_id => 2331 # id = 285
t = Team.create! :name => 'Grand View', :espn_team_id => 2254 # id = 286
t = Team.create! :name => 'Concord', :espn_team_id => 2148 # id = 287
t = Team.create! :name => 'Stillman', :espn_team_id => 2948 # id = 288
t = Team.create! :name => 'Western New Mexico', :espn_team_id => 2703 # id = 289
t = Team.create! :name => 'Union College', :espn_team_id => 2841 # id = 290
t = Team.create! :name => 'Taylor University', :espn_team_id => 620 # id = 291
t = Team.create! :name => 'Minot State University', :espn_team_id => 568 # id = 292
t = Team.create! :name => 'Northwestern Oklahoma State', :espn_team_id => 2823 # id = 293
t = Team.create! :name => 'Tarleton State', :espn_team_id => 2627 # id = 294
t = Team.create! :name => 'Tuskegee', :espn_team_id => 2657 # id = 295
t = Team.create! :name => 'Missouri S&T', :espn_team_id => 2402 # id = 296
t = Team.create! :name => 'New Haven', :espn_team_id => 2441 # id = 297
t = Team.create! :name => 'Incarnate Word', :espn_team_id => 2916 # id = 298
t = Team.create! :name => 'Sioux Falls', :espn_team_id => 2894 # id = 299
t = Team.create! :name => 'Bacone College', :espn_team_id => 487 # id = 300
t = Team.create! :name => 'Black Hills State', :espn_team_id => 2069 # id = 301
t = Team.create! :name => 'Johnson Smith', :espn_team_id => 2304 # id = 302
t = Team.create! :name => 'Kentucky Wesleyan', :espn_team_id => 2316 # id = 303
t = Team.create! :name => 'Wayne State (NE)', :espn_team_id => 284 # id = 304
t = Team.create! :name => 'Montana-Western', :espn_team_id => 2701 # id = 305
t = Team.create! :name => 'Central Oklahoma', :espn_team_id => 2122 # id = 306
t = Team.create! :name => 'Virginia-Wise', :espn_team_id => 2842 # id = 307
t = Team.create! :name => 'St. Anselm', :espn_team_id => 2830 # id = 308
t = Team.create! :name => 'Willamette', :espn_team_id => 2930 # id = 309
t = Team.create! :name => 'Central Methodist', :espn_team_id => 2860 # id = 310
t = Team.create! :name => 'Western Oregon', :espn_team_id => 2848 # id = 311
t = Team.create! :name => 'Shorter', :espn_team_id => 2560 # id = 312
t = Team.create! :name => 'Southwestern Oklahoma', :espn_team_id => 2927 # id = 313
t = Team.create! :name => 'South Dakota Mines', :espn_team_id => 613 # id = 314
t = Team.create! :name => "St. Joseph's (IN)", :espn_team_id => 2601 # id = 315
t = Team.create! :name => 'Chadron State', :espn_team_id => 2123 # id = 316
t = Team.create! :name => 'Carroll (MT)', :espn_team_id => 2104 # id = 317
t = Team.create! :name => 'Fayetteville State', :espn_team_id => 2220 # id = 318
t = Team.create! :name => 'Southern Virginia', :espn_team_id => 2896 # id = 319
t = Team.create! :name => 'Pace', :espn_team_id => 2487 # id = 320
t = Team.create! :name => 'Chowan', :espn_team_id => 2804 # id = 321
t = Team.create! :name => 'Texas A&M-Commerce', :espn_team_id => 2837 # id = 322
t = Team.create! :name => 'Quincy', :espn_team_id => 2825 # id = 323
t = Team.create! :name => 'Colorado Mesa University', :espn_team_id => 11 # id = 324
t = Team.create! :name => 'Indianapolis', :espn_team_id => 2292 # id = 325
t = Team.create! :name => 'New Mexico Highlands', :espn_team_id => 2424 # id = 326
t = Team.create! :name => 'West Virginia State', :espn_team_id => 2707 # id = 327
t = Team.create! :name => 'Arkansas-Monticello', :espn_team_id => 2028 # id = 328
t = Team.create! :name => 'Lincoln (PA)', :espn_team_id => 2339 # id = 329
t = Team.create! :name => 'Webber International', :espn_team_id => 2691 # id = 330
t = Team.create! :name => 'Culver-Stockton', :espn_team_id => 510 # id = 331
t = Team.create! :name => 'Edward Waters', :espn_team_id => 2206 # id = 332
t = Team.create! :name => 'Glenville State', :espn_team_id => 2249 # id = 333
t = Team.create! :name => 'Lock Haven', :espn_team_id => 209 # id = 334
t = Team.create! :name => 'None335', :espn_team_id => 419 # id = 335



puts 'SETTING UP DEFAULT CONFERENCES'

c = Conference.create! :name => 'ACC', :league => 'FBS' # id = 1
c = Conference.create! :name => 'Big 12', :league => 'FBS' # id = 2
c = Conference.create! :name => 'Big East', :league => 'FBS' # id = 3
c = Conference.create! :name => 'Big Ten', :league => 'FBS' # id = 4
c = Conference.create! :name => 'C-USA', :league => 'FBS' # id = 5
c = Conference.create! :name => 'IA Independents', :league => 'FBS' # id = 6
c = Conference.create! :name => 'MAC', :league => 'FBS' # id = 7
c = Conference.create! :name => 'MWC', :league => 'FBS' # id = 8
c = Conference.create! :name => 'Pac-12', :league => 'FBS' # id = 9
c = Conference.create! :name => 'SEC', :league => 'FBS' # id = 10
c = Conference.create! :name => 'Sun Belt', :league => 'FBS' # id = 11
c = Conference.create! :name => 'WAC', :league => 'FCS' # id = 12
c = Conference.create! :name => 'Big Sky', :league => 'FCS' # id = 13
c = Conference.create! :name => 'Big South', :league => 'FCS' # id = 14
c = Conference.create! :name => 'CAA', :league => 'FCS' # id = 15
c = Conference.create! :name => 'Great West', :league => 'FCS' # id = 16
c = Conference.create! :name => 'IAA Independents', :league => 'FCS' # id = 17
c = Conference.create! :name => 'Ivy', :league => 'FCS' # id = 18
c = Conference.create! :name => 'MEAC', :league => 'FCS' # id = 19
c = Conference.create! :name => 'MVC', :league => 'FCS' # id = 20
c = Conference.create! :name => 'Northeast', :league => 'FCS' # id = 21
c = Conference.create! :name => 'OVC', :league => 'FCS' # id = 22
c = Conference.create! :name => 'Patriot', :league => 'FCS' # id = 23
c = Conference.create! :name => 'Pioneer', :league => 'FCS' # id = 24
c = Conference.create! :name => 'Southern', :league => 'FCS' # id = 25
c = Conference.create! :name => 'Southland', :league => 'FCS' # id = 26
c = Conference.create! :name => 'SWAC', :league => 'FCS' # id = 27
c = Conference.create! :name => 'CIAA', :league => 'D2' # id = 28
c = Conference.create! :name => 'NE10', :league => 'D2' # id = 29
c = Conference.create! :name => 'GLIAC', :league => 'D2' # id = 30
c = Conference.create! :name => 'GNAC', :league => 'D2' # id = 31
c = Conference.create! :name => 'Gulf South', :league => 'D2' # id = 32
c = Conference.create! :name => 'D2 Independents', :league => 'D2' # id = 33
c = Conference.create! :name => 'MAIAC', :league => 'D2' # id = 34
c = Conference.create! :name => 'NSC', :league => 'D2' # id = 35
c = Conference.create! :name => 'PSAC', :league => 'D2' # id = 36
c = Conference.create! :name => 'RMAC', :league => 'D2' # id = 37
c = Conference.create! :name => 'SAC', :league => 'D2' # id = 38
c = Conference.create! :name => 'SIAC', :league => 'D2' # id = 39
c = Conference.create! :name => 'WVIAC', :league => 'D2' # id = 40
c = Conference.create! :name => 'ASC', :league => 'D3' # id = 41
c = Conference.create! :name => 'ACFC', :league => 'D3' # id = 42
c = Conference.create! :name => 'Centennial', :league => 'D3' # id = 43
c = Conference.create! :name => 'Empire-8', :league => 'D3' # id = 44
c = Conference.create! :name => 'Heartland', :league => 'D3' # id = 45
c = Conference.create! :name => 'I&WC', :league => 'D3' # id = 46
c = Conference.create! :name => 'D3 Independents', :league => 'D3' # id = 47
c = Conference.create! :name => 'IIAC', :league => 'D3' # id = 48
c = Conference.create! :name => 'Liberty', :league => 'D3' # id = 49
c = Conference.create! :name => 'MIAC', :league => 'D3' # id = 50
c = Conference.create! :name => 'Mid-Atlantic', :league => 'D3' # id = 51
c = Conference.create! :name => 'Midwest', :league => 'D3' # id = 52
c = Conference.create! :name => 'Minnesota Inter.', :league => 'D3' # id = 53
c = Conference.create! :name => 'NESCA', :league => 'D3' # id = 54
c = Conference.create! :name => 'NJAC', :league => 'D3' # id = 55
c = Conference.create! :name => 'NCAC', :league => 'D3' # id = 56
c = Conference.create! :name => 'New England', :league => 'D3' # id = 57
c = Conference.create! :name => 'Northwest', :league => 'D3' # id = 58
c = Conference.create! :name => 'OAC', :league => 'D3' # id = 59
c = Conference.create! :name => 'Old Dominion', :league => 'D3' # id = 60
c = Conference.create! :name => 'Presidents', :league => 'D3' # id = 61
c = Conference.create! :name => 'SCIC', :league => 'D3' # id = 62
c = Conference.create! :name => 'SCAC', :league => 'D3' # id = 63
c = Conference.create! :name => 'St. Louis', :league => 'D3' # id = 64
c = Conference.create! :name => 'UAA', :league => 'D3' # id = 65
c = Conference.create! :name => 'Usa South', :league => 'D3' # id = 66
c = Conference.create! :name => 'Wisconsin', :league => 'D3' # id = 67
c = Conference.create! :name => 'NAIA', :league => 'NAIA' # id = 68

puts 'SETTING UP DEFAULT WEEKS'
w = Week.create! :season_year => 2012, :season_week => 1, :pick_deadline => Time.parse('2012-08-30 3:00 AM') #1 (THURSDAY)
w = Week.create! :season_year => 2012, :season_week => 2, :pick_deadline => Time.parse('2012-09-08 3:00 AM') #2
w = Week.create! :season_year => 2012, :season_week => 3, :pick_deadline => Time.parse('2012-09-15 3:00 AM') #3
w = Week.create! :season_year => 2012, :season_week => 4, :pick_deadline => Time.parse('2012-09-22 3:00 AM') #4
w = Week.create! :season_year => 2012, :season_week => 5, :pick_deadline => Time.parse('2012-09-29 3:00 AM') #5
w = Week.create! :season_year => 2012, :season_week => 6, :pick_deadline => Time.parse('2012-10-06 3:00 AM') #6
w = Week.create! :season_year => 2012, :season_week => 7, :pick_deadline => Time.parse('2012-10-13 3:00 AM') #7
w = Week.create! :season_year => 2012, :season_week => 8, :pick_deadline => Time.parse('2012-10-20 3:00 AM') #8
w = Week.create! :season_year => 2012, :season_week => 9, :pick_deadline => Time.parse('2012-10-27 3:00 AM') #9
w = Week.create! :season_year => 2012, :season_week => 10, :pick_deadline => Time.parse('2012-11-03 3:00 AM') #10
w = Week.create! :season_year => 2012, :season_week => 11, :pick_deadline => Time.parse('2012-11-10 3:00 AM') #11
w = Week.create! :season_year => 2012, :season_week => 12, :pick_deadline => Time.parse('2012-11-17 3:00 AM') #12
w = Week.create! :season_year => 2012, :season_week => 13, :pick_deadline => Time.parse('2012-11-24 3:00 AM') #13 + I-AA Play-In
w = Week.create! :season_year => 2012, :season_week => 14, :pick_deadline => Time.parse('2012-12-01 3:00 AM') #14 + I-AA Sweet 16

#Weeks 15-18 are combined for purposes of weekly prizes
w = Week.create! :season_year => 2012, :season_week => 15, :pick_deadline => Time.parse('2012-12-07 3:00 AM') #15 Army/Navy + I-AA Elite 8 (FRIDAY)
w = Week.create! :season_year => 2012, :season_week => 16, :pick_deadline => Time.parse('2012-12-15 3:00 AM') #16 I-AA Final 4
w = Week.create! :season_year => 2012, :season_week => 17, :pick_deadline => Time.parse('2013-01-01 3:00 AM') #17 BCS (non-champ) + I-AA Championship + prelim ntl champ pick
w = Week.create! :season_year => 2012, :season_week => 18, :pick_deadline => Time.parse('2013-01-07 3:00 AM') #18 BCS National Championship (editable for certain people)

puts 'SETTING UP GAMES'
g = Game.create! :week_id => 1, :away_team_id => 156, :home_team_id => 154, :start_time => Time.parse('Thursday, August 30 10:59:59 PM'), :espn_game_id => 322430006
g = Game.create! :week_id => 1, :away_team_id => 141, :home_team_id => 207, :start_time => Time.parse('Thursday, August 30 10:59:59 PM'), :espn_game_id => 322430107
g = Game.create! :week_id => 1, :away_team_id => 87, :home_team_id => 47, :start_time => Time.parse('Thursday, August 30 10:59:59 PM'), :espn_game_id => 322430242
g = Game.create! :week_id => 1, :away_team_id => 126, :home_team_id => 89, :start_time => Time.parse('Thursday, August 30 10:59:59 PM'), :espn_game_id => 322430254
g = Game.create! :week_id => 1, :away_team_id => 36, :home_team_id => 78, :start_time => Time.parse('Thursday, August 30 10:59:59 PM'), :espn_game_id => 322432439
g = Game.create! :week_id => 1, :away_team_id => 101, :home_team_id => 103, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322430238
g = Game.create! :week_id => 1, :away_team_id => 53, :home_team_id => 59, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322432006
g = Game.create! :week_id => 1, :away_team_id => 64, :home_team_id => 60, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322432050
g = Game.create! :week_id => 1, :away_team_id => 312, :home_team_id => 211, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322432097
g = Game.create! :week_id => 1, :away_team_id => 199, :home_team_id => 63, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322432117
g = Game.create! :week_id => 1, :away_team_id => 334, :home_team_id => 205, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322432230
g = Game.create! :week_id => 1, :away_team_id => 145, :home_team_id => 65, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322432309
g = Game.create! :week_id => 1, :away_team_id => 319, :home_team_id => 217, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322432413
g = Game.create! :week_id => 1, :away_team_id => 313, :home_team_id => 236, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322432617
g = Game.create! :week_id => 1, :away_team_id => 210, :home_team_id => 183, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322432710
g = Game.create! :week_id => 1, :away_team_id => 283, :home_team_id => 227, :start_time => Time.parse('Thursday, August 30 7:00 PM'), :espn_game_id => 322432717
g = Game.create! :week_id => 1, :away_team_id => 140, :home_team_id => 24, :start_time => Time.parse('Thursday, August 30 7:30 PM'), :espn_game_id => 322430041
g = Game.create! :week_id => 1, :away_team_id => 278, :home_team_id => 137, :start_time => Time.parse('Thursday, August 30 7:30 PM'), :espn_game_id => 322430048
g = Game.create! :week_id => 1, :away_team_id => 182, :home_team_id => 195, :start_time => Time.parse('Thursday, August 30 7:30 PM'), :espn_game_id => 322432197
g = Game.create! :week_id => 1, :away_team_id => 175, :home_team_id => 153, :start_time => Time.parse('Thursday, August 30 7:30 PM'), :espn_game_id => 322432247
g = Game.create! :week_id => 1, :away_team_id => 21, :home_team_id => 116, :start_time => Time.parse('Thursday, August 30 7:30 PM'), :espn_game_id => 322432348
g = Game.create! :week_id => 1, :away_team_id => 231, :home_team_id => 109, :start_time => Time.parse('Thursday, August 30 7:30 PM'), :espn_game_id => 322432393
g = Game.create! :week_id => 1, :away_team_id => 314, :home_team_id => 149, :start_time => Time.parse('Thursday, August 30 8:00 PM'), :espn_game_id => 322430155
g = Game.create! :week_id => 1, :away_team_id => 128, :home_team_id => 118, :start_time => Time.parse('Thursday, August 30 8:00 PM'), :espn_game_id => 322430166
g = Game.create! :week_id => 1, :away_team_id => 151, :home_team_id => 120, :start_time => Time.parse('Thursday, August 30 8:00 PM'), :espn_game_id => 322430328
g = Game.create! :week_id => 1, :away_team_id => 286, :home_team_id => 214, :start_time => Time.parse('Thursday, August 30 8:00 PM'), :espn_game_id => 322432181
g = Game.create! :week_id => 1, :away_team_id => 168, :home_team_id => 201, :start_time => Time.parse('Thursday, August 30 8:00 PM'), :espn_game_id => 322432635
g = Game.create! :week_id => 1, :away_team_id => 315, :home_team_id => 219, :start_time => Time.parse('Thursday, August 30 8:00 PM'), :espn_game_id => 322432674
g = Game.create! :week_id => 1, :away_team_id => 121, :home_team_id => 115, :start_time => Time.parse('Thursday, August 30 9:00 PM'), :espn_game_id => 322430070
g = Game.create! :week_id => 1, :away_team_id => 272, :home_team_id => 152, :start_time => Time.parse('Thursday, August 30 9:00 PM'), :espn_game_id => 322430302
g = Game.create! :week_id => 1, :away_team_id => 316, :home_team_id => 124, :start_time => Time.parse('Thursday, August 30 9:05 PM'), :espn_game_id => 322430147
g = Game.create! :week_id => 1, :away_team_id => 91, :home_team_id => 56, :start_time => Time.parse('Thursday, August 30 10:15 PM'), :espn_game_id => 322430252
g = Game.create! :week_id => 1, :away_team_id => 193, :home_team_id => 105, :start_time => Time.parse('Friday, August 31 10:59:59 PM'), :espn_game_id => 322442226
g = Game.create! :week_id => 1, :away_team_id => 146, :home_team_id => 69, :start_time => Time.parse('Friday, August 31 7:00 PM'), :espn_game_id => 322440218
g = Game.create! :week_id => 1, :away_team_id => 102, :home_team_id => 9, :start_time => Time.parse('Friday, August 31 7:30 PM'), :espn_game_id => 322440152
g = Game.create! :week_id => 1, :away_team_id => 73, :home_team_id => 35, :start_time => Time.parse('Friday, August 31 8:00 PM'), :espn_game_id => 322440127
g = Game.create! :week_id => 1, :away_team_id => 58, :home_team_id => 57, :start_time => Time.parse('Saturday, September 1 9:00 AM'), :espn_game_id => 322452426
g = Game.create! :week_id => 1, :away_team_id => 111, :home_team_id => 52, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450005
g = Game.create! :week_id => 1, :away_team_id => 125, :home_team_id => 81, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450009
g = Game.create! :week_id => 1, :away_team_id => 119, :home_team_id => 86, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450024
g = Game.create! :week_id => 1, :away_team_id => 117, :home_team_id => 82, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450025
g = Game.create! :week_id => 1, :away_team_id => 114, :home_team_id => 88, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450030
g = Game.create! :week_id => 1, :away_team_id => 62, :home_team_id => 96, :start_time => Time.parse('Saturday, September 1 12:00 PM'), :espn_game_id => 322450061
g = Game.create! :week_id => 1, :away_team_id => 51, :home_team_id => 14, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450066
g = Game.create! :week_id => 1, :away_team_id => 194, :home_team_id => 112, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450098
g = Game.create! :week_id => 1, :away_team_id => 220, :home_team_id => 43, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450151
g = Game.create! :week_id => 1, :away_team_id => 245, :home_team_id => 75, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450167
g = Game.create! :week_id => 1, :away_team_id => 38, :home_team_id => 29, :start_time => Time.parse('Saturday, September 1 12:00 PM'), :espn_game_id => 322450183
g = Game.create! :week_id => 1, :away_team_id => 66, :home_team_id => 39, :start_time => Time.parse('Saturday, September 1 12:00 PM'), :espn_game_id => 322450194
g = Game.create! :week_id => 1, :away_team_id => 174, :home_team_id => 19, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450197
g = Game.create! :week_id => 1, :away_team_id => 232, :home_team_id => 85, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450204
g = Game.create! :week_id => 1, :away_team_id => 68, :home_team_id => 40, :start_time => Time.parse('Saturday, September 1 12:00 PM'), :espn_game_id => 322450213
g = Game.create! :week_id => 1, :away_team_id => 202, :home_team_id => 46, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450235
g = Game.create! :week_id => 1, :away_team_id => 155, :home_team_id => 44, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450248
g = Game.create! :week_id => 1, :away_team_id => 79, :home_team_id => 20, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450251
g = Game.create! :week_id => 1, :away_team_id => 192, :home_team_id => 138, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450256
g = Game.create! :week_id => 1, :away_team_id => 76, :home_team_id => 90, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450264
g = Game.create! :week_id => 1, :away_team_id => 45, :home_team_id => 30, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450277
g = Game.create! :week_id => 1, :away_team_id => 188, :home_team_id => 142, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450295
g = Game.create! :week_id => 1, :away_team_id => 230, :home_team_id => 107, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450309
g = Game.create! :week_id => 1, :away_team_id => 172, :home_team_id => 131, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450324
g = Game.create! :week_id => 1, :away_team_id => 71, :home_team_id => 31, :start_time => Time.parse('Saturday, September 1 12:00 PM'), :espn_game_id => 322450356
g = Game.create! :week_id => 1, :away_team_id => 204, :home_team_id => 185, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322450399
g = Game.create! :week_id => 1, :away_team_id => 122, :home_team_id => 72, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322452005
g = Game.create! :week_id => 1, :away_team_id => 181, :home_team_id => 15, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322452305
g = Game.create! :week_id => 1, :away_team_id => 178, :home_team_id => 16, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322452306
g = Game.create! :week_id => 1, :away_team_id => 317, :home_team_id => 127, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322452502
g = Game.create! :week_id => 1, :away_team_id => 269, :home_team_id => 134, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322452506
g = Game.create! :week_id => 1, :away_team_id => 187, :home_team_id => 135, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322452619
g = Game.create! :week_id => 1, :away_team_id => 167, :home_team_id => 200, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322452634
g = Game.create! :week_id => 1, :away_team_id => 18, :home_team_id => 54, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322452638
g = Game.create! :week_id => 1, :away_team_id => 233, :home_team_id => 22, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322452641
g = Game.create! :week_id => 1, :away_team_id => 27, :home_team_id => 50, :start_time => Time.parse('Saturday, September 1 10:59:59 PM'), :espn_game_id => 322452655
g = Game.create! :week_id => 1, :away_team_id => 223, :home_team_id => 8, :start_time => Time.parse('Saturday, September 1 12:30 PM'), :espn_game_id => 322450153
g = Game.create! :week_id => 1, :away_team_id => 189, :home_team_id => 209, :start_time => Time.parse('Saturday, September 1 12:30 PM'), :espn_game_id => 322452329
g = Game.create! :week_id => 1, :away_team_id => 191, :home_team_id => 170, :start_time => Time.parse('Saturday, September 1 1:00 PM'), :espn_game_id => 322452415
g = Game.create! :week_id => 1, :away_team_id => 136, :home_team_id => 166, :start_time => Time.parse('Saturday, September 1 2:00 PM'), :espn_game_id => 322452169
g = Game.create! :week_id => 1, :away_team_id => 213, :home_team_id => 176, :start_time => Time.parse('Saturday, September 1 2:00 PM'), :espn_game_id => 322452287
g = Game.create! :week_id => 1, :away_team_id => 147, :home_team_id => 6, :start_time => Time.parse('Saturday, September 1 3:00 PM'), :espn_game_id => 322450120
g = Game.create! :week_id => 1, :away_team_id => 144, :home_team_id => 10, :start_time => Time.parse('Saturday, September 1 3:00 PM'), :espn_game_id => 322450258
g = Game.create! :week_id => 1, :away_team_id => 216, :home_team_id => 186, :start_time => Time.parse('Saturday, September 1 3:00 PM'), :espn_game_id => 322452803
g = Game.create! :week_id => 1, :away_team_id => 277, :home_team_id => 169, :start_time => Time.parse('Saturday, September 1 3:30 PM'), :espn_game_id => 322450047
g = Game.create! :week_id => 1, :away_team_id => 61, :home_team_id => 95, :start_time => Time.parse('Saturday, September 1 3:30 PM'), :espn_game_id => 322450057
g = Game.create! :week_id => 1, :away_team_id => 7, :home_team_id => 1, :start_time => Time.parse('Saturday, September 1 3:30 PM'), :espn_game_id => 322450103
g = Game.create! :week_id => 1, :away_team_id => 150, :home_team_id => 123, :start_time => Time.parse('Saturday, September 1 3:30 PM'), :espn_game_id => 322450149
g = Game.create! :week_id => 1, :away_team_id => 49, :home_team_id => 37, :start_time => Time.parse('Saturday, September 1 3:30 PM'), :espn_game_id => 322450158
g = Game.create! :week_id => 1, :away_team_id => 180, :home_team_id => 42, :start_time => Time.parse('Saturday, September 1 3:30 PM'), :espn_game_id => 322450275
g = Game.create! :week_id => 1, :away_team_id => 33, :home_team_id => 67, :start_time => Time.parse('Saturday, September 1 3:30 PM'), :espn_game_id => 322452459
g = Game.create! :week_id => 1, :away_team_id => 196, :home_team_id => 41, :start_time => Time.parse('Saturday, September 1 3:30 PM'), :espn_game_id => 322452509
g = Game.create! :week_id => 1, :away_team_id => 224, :home_team_id => 226, :start_time => Time.parse('Saturday, September 1 4:00 PM'), :espn_game_id => 322452535
g = Game.create! :week_id => 1, :away_team_id => 198, :home_team_id => 4, :start_time => Time.parse('Saturday, September 1 6:00 PM'), :espn_game_id => 322450052
g = Game.create! :week_id => 1, :away_team_id => 184, :home_team_id => 26, :start_time => Time.parse('Saturday, September 1 6:00 PM'), :espn_game_id => 322450221
g = Game.create! :week_id => 1, :away_team_id => 215, :home_team_id => 225, :start_time => Time.parse('Saturday, September 1 6:00 PM'), :espn_game_id => 322450290
g = Game.create! :week_id => 1, :away_team_id => 253, :home_team_id => 240, :start_time => Time.parse('Saturday, September 1 6:00 PM'), :espn_game_id => 322452029
g = Game.create! :week_id => 1, :away_team_id => 206, :home_team_id => 212, :start_time => Time.parse('Saturday, September 1 6:00 PM'), :espn_game_id => 322452166
g = Game.create! :week_id => 1, :away_team_id => 228, :home_team_id => 132, :start_time => Time.parse('Saturday, September 1 6:00 PM'), :espn_game_id => 322452241
g = Game.create! :week_id => 1, :away_team_id => 261, :home_team_id => 243, :start_time => Time.parse('Saturday, September 1 6:00 PM'), :espn_game_id => 322452400
g = Game.create! :week_id => 1, :away_team_id => 318, :home_team_id => 173, :start_time => Time.parse('Saturday, September 1 6:00 PM'), :espn_game_id => 322452428
g = Game.create! :week_id => 1, :away_team_id => 270, :home_team_id => 171, :start_time => Time.parse('Saturday, September 1 6:00 PM'), :espn_game_id => 322452450
g = Game.create! :week_id => 1, :away_team_id => 130, :home_team_id => 222, :start_time => Time.parse('Saturday, September 1 6:00 PM'), :espn_game_id => 322452643
g = Game.create! :week_id => 1, :away_team_id => 133, :home_team_id => 12, :start_time => Time.parse('Saturday, September 1 6:30 PM'), :espn_game_id => 322450154
g = Game.create! :week_id => 1, :away_team_id => 2, :home_team_id => 94, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322450002
g = Game.create! :week_id => 1, :away_team_id => 197, :home_team_id => 93, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322450008
g = Game.create! :week_id => 1, :away_team_id => 221, :home_team_id => 28, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322450058
g = Game.create! :week_id => 1, :away_team_id => 110, :home_team_id => 98, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322450099
g = Game.create! :week_id => 1, :away_team_id => 235, :home_team_id => 17, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322450142
g = Game.create! :week_id => 1, :away_team_id => 229, :home_team_id => 100, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322450145
g = Game.create! :week_id => 1, :away_team_id => 106, :home_team_id => 3, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322450150
g = Game.create! :week_id => 1, :away_team_id => 242, :home_team_id => 99, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322450344
g = Game.create! :week_id => 1, :away_team_id => 190, :home_team_id => 179, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322452449
g = Game.create! :week_id => 1, :away_team_id => 237, :home_team_id => 295, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322452657
g = Game.create! :week_id => 1, :away_team_id => 239, :home_team_id => 241, :start_time => Time.parse('Saturday, September 1 7:00 PM'), :espn_game_id => 322452755
g = Game.create! :week_id => 1, :away_team_id => 218, :home_team_id => 148, :start_time => Time.parse('Saturday, September 1 7:05 PM'), :espn_game_id => 322450013
g = Game.create! :week_id => 1, :away_team_id => 177, :home_team_id => 32, :start_time => Time.parse('Saturday, September 1 8:00 PM'), :espn_game_id => 322450084
g = Game.create! :week_id => 1, :away_team_id => 34, :home_team_id => 92, :start_time => Time.parse('Saturday, September 1 8:00 PM'), :espn_game_id => 322450333
g = Game.create! :week_id => 1, :away_team_id => 246, :home_team_id => 244, :start_time => Time.parse('Saturday, September 1 8:00 PM'), :espn_game_id => 322452504
g = Game.create! :week_id => 1, :away_team_id => 129, :home_team_id => 113, :start_time => Time.parse('Saturday, September 1 10:00 PM'), :espn_game_id => 322450278
g = Game.create! :week_id => 1, :away_team_id => 70, :home_team_id => 80, :start_time => Time.parse('Saturday, September 1 10:30 PM'), :espn_game_id => 322450012
g = Game.create! :week_id => 1, :away_team_id => 104, :home_team_id => 84, :start_time => Time.parse('Saturday, September 1 10:30 PM'), :espn_game_id => 322452483
g = Game.create! :week_id => 1, :away_team_id => 74, :home_team_id => 83, :start_time => Time.parse('Sunday, September 2 10:59:59 PM'), :espn_game_id => 322460038
g = Game.create! :week_id => 1, :away_team_id => 238, :home_team_id => 165, :start_time => Time.parse('Sunday, September 2 12:00 PM'), :espn_game_id => 322462065
g = Game.create! :week_id => 1, :away_team_id => 97, :home_team_id => 25, :start_time => Time.parse('Sunday, September 2 3:30 PM'), :espn_game_id => 322460097
g = Game.create! :week_id => 1, :away_team_id => 48, :home_team_id => 13, :start_time => Time.parse('Sunday, September 2 6:30 PM'), :espn_game_id => 322460239
g = Game.create! :week_id => 1, :away_team_id => 5, :home_team_id => 11, :start_time => Time.parse('Monday, September 3 8:00 PM'), :espn_game_id => 322470259
g = Game.create! :week_id => 2, :away_team_id => 248, :home_team_id => 226, :start_time => Time.parse('Thursday, September 6 10:59:59 PM'), :espn_game_id => 322502535
g = Game.create! :week_id => 2, :away_team_id => 26, :home_team_id => 23, :start_time => Time.parse('Thursday, September 6 8:00 PM'), :espn_game_id => 322502132
g = Game.create! :week_id => 2, :away_team_id => 256, :home_team_id => 201, :start_time => Time.parse('Thursday, September 6 8:00 PM'), :espn_game_id => 322502635
g = Game.create! :week_id => 2, :away_team_id => 89, :home_team_id => 120, :start_time => Time.parse('Friday, September 7 8:00 PM'), :espn_game_id => 322510328
g = Game.create! :week_id => 2, :away_team_id => 232, :home_team_id => 154, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520006
g = Game.create! :week_id => 2, :away_team_id => 19, :home_team_id => 80, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520012
g = Game.create! :week_id => 2, :away_team_id => 55, :home_team_id => 76, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520021
g = Game.create! :week_id => 2, :away_team_id => 3, :home_team_id => 86, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520024
g = Game.create! :week_id => 2, :away_team_id => 151, :home_team_id => 82, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520025
g = Game.create! :week_id => 2, :away_team_id => 37, :home_team_id => 87, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520026
g = Game.create! :week_id => 2, :away_team_id => 179, :home_team_id => 74, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520036
g = Game.create! :week_id => 2, :away_team_id => 128, :home_team_id => 83, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520038
g = Game.create! :week_id => 2, :away_team_id => 9, :home_team_id => 24, :start_time => Time.parse('Saturday, September 8 12:00 PM'), :espn_game_id => 322520041
g = Game.create! :week_id => 2, :away_team_id => 141, :home_team_id => 36, :start_time => Time.parse('Saturday, September 8 12:00 PM'), :espn_game_id => 322520135
g = Game.create! :week_id => 2, :away_team_id => 53, :home_team_id => 39, :start_time => Time.parse('Saturday, September 8 12:00 PM'), :espn_game_id => 322520194
g = Game.create! :week_id => 2, :away_team_id => 167, :home_team_id => 18, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520201
g = Game.create! :week_id => 2, :away_team_id => 50, :home_team_id => 51, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520202
g = Game.create! :week_id => 2, :away_team_id => 42, :home_team_id => 85, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520204
g = Game.create! :week_id => 2, :away_team_id => 6, :home_team_id => 69, :start_time => Time.parse('Saturday, September 8 12:00 PM'), :espn_game_id => 322520218
g = Game.create! :week_id => 2, :away_team_id => 205, :home_team_id => 146, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520222
g = Game.create! :week_id => 2, :away_team_id => 131, :home_team_id => 224, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520231
g = Game.create! :week_id => 2, :away_team_id => 116, :home_team_id => 44, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520248
g = Game.create! :week_id => 2, :away_team_id => 75, :home_team_id => 20, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520251
g = Game.create! :week_id => 2, :away_team_id => 132, :home_team_id => 144, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520257
g = Game.create! :week_id => 2, :away_team_id => 40, :home_team_id => 10, :start_time => Time.parse('Saturday, September 8 12:00 PM'), :espn_game_id => 322520258
g = Game.create! :week_id => 2, :away_team_id => 121, :home_team_id => 91, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520265
g = Game.create! :week_id => 2, :away_team_id => 227, :home_team_id => 45, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322520276
g = Game.create! :week_id => 2, :away_team_id => 94, :home_team_id => 99, :start_time => Time.parse('Saturday, September 8 12:00 PM'), :espn_game_id => 322520344
g = Game.create! :week_id => 2, :away_team_id => 209, :home_team_id => 187, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522115
g = Game.create! :week_id => 2, :away_team_id => 213, :home_team_id => 188, :start_time => Time.parse('Saturday, September 8 12:00 PM'), :espn_game_id => 322522184
g = Game.create! :week_id => 2, :away_team_id => 176, :home_team_id => 64, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522199
g = Game.create! :week_id => 2, :away_team_id => 59, :home_team_id => 106, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522229
g = Game.create! :week_id => 2, :away_team_id => 47, :home_team_id => 15, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522305
g = Game.create! :week_id => 2, :away_team_id => 7, :home_team_id => 16, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522306
g = Game.create! :week_id => 2, :away_team_id => 244, :home_team_id => 230, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522320
g = Game.create! :week_id => 2, :away_team_id => 203, :home_team_id => 216, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522368
g = Game.create! :week_id => 2, :away_team_id => 105, :home_team_id => 109, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522393
g = Game.create! :week_id => 2, :away_team_id => 125, :home_team_id => 78, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522439
g = Game.create! :week_id => 2, :away_team_id => 28, :home_team_id => 117, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522440
g = Game.create! :week_id => 2, :away_team_id => 274, :home_team_id => 180, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522460
g = Game.create! :week_id => 2, :away_team_id => 113, :home_team_id => 84, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522483
g = Game.create! :week_id => 2, :away_team_id => 185, :home_team_id => 190, :start_time => Time.parse('Saturday, September 8 12:00 PM'), :espn_game_id => 322522523
g = Game.create! :week_id => 2, :away_team_id => 181, :home_team_id => 235, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522545
g = Game.create! :week_id => 2, :away_team_id => 236, :home_team_id => 48, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522567
g = Game.create! :week_id => 2, :away_team_id => 165, :home_team_id => 175, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522569
g = Game.create! :week_id => 2, :away_team_id => 43, :home_team_id => 101, :start_time => Time.parse('Saturday, September 8 12:00 PM'), :espn_game_id => 322522579
g = Game.create! :week_id => 2, :away_team_id => 186, :home_team_id => 192, :start_time => Time.parse('Saturday, September 8 12:00 PM'), :espn_game_id => 322522598
g = Game.create! :week_id => 2, :away_team_id => 320, :home_team_id => 135, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522619
g = Game.create! :week_id => 2, :away_team_id => 241, :home_team_id => 77, :start_time => Time.parse('Saturday, September 8 10:59:59 PM'), :espn_game_id => 322522628
g = Game.create! :week_id => 2, :away_team_id => 60, :home_team_id => 2, :start_time => Time.parse('Saturday, September 8 12:30 PM'), :espn_game_id => 322520228
g = Game.create! :week_id => 2, :away_team_id => 193, :home_team_id => 206, :start_time => Time.parse('Saturday, September 8 1:00 PM'), :espn_game_id => 322520046
g = Game.create! :week_id => 2, :away_team_id => 139, :home_team_id => 1, :start_time => Time.parse('Saturday, September 8 1:00 PM'), :espn_game_id => 322520103
g = Game.create! :week_id => 2, :away_team_id => 182, :home_team_id => 66, :start_time => Time.parse('Saturday, September 8 1:00 PM'), :espn_game_id => 322520193
g = Game.create! :week_id => 2, :away_team_id => 143, :home_team_id => 189, :start_time => Time.parse('Saturday, September 8 1:00 PM'), :espn_game_id => 322522405
g = Game.create! :week_id => 2, :away_team_id => 194, :home_team_id => 11, :start_time => Time.parse('Saturday, September 8 1:30 PM'), :espn_game_id => 322520259
g = Game.create! :week_id => 2, :away_team_id => 215, :home_team_id => 130, :start_time => Time.parse('Saturday, September 8 1:30 PM'), :espn_game_id => 322522127
g = Game.create! :week_id => 2, :away_team_id => 321, :home_team_id => 136, :start_time => Time.parse('Saturday, September 8 1:30 PM'), :espn_game_id => 322522678
g = Game.create! :week_id => 2, :away_team_id => 322, :home_team_id => 156, :start_time => Time.parse('Saturday, September 8 2:00 PM'), :espn_game_id => 322522636
g = Game.create! :week_id => 2, :away_team_id => 323, :home_team_id => 177, :start_time => Time.parse('Saturday, September 8 2:05 PM'), :espn_game_id => 322520282
g = Game.create! :week_id => 2, :away_team_id => 8, :home_team_id => 12, :start_time => Time.parse('Saturday, September 8 3:00 PM'), :espn_game_id => 322520154
g = Game.create! :week_id => 2, :away_team_id => 204, :home_team_id => 150, :start_time => Time.parse('Saturday, September 8 3:00 PM'), :espn_game_id => 322520233
g = Game.create! :week_id => 2, :away_team_id => 129, :home_team_id => 56, :start_time => Time.parse('Saturday, September 8 3:00 PM'), :espn_game_id => 322520252
g = Game.create! :week_id => 2, :away_team_id => 166, :home_team_id => 137, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322520048
g = Game.create! :week_id => 2, :away_team_id => 41, :home_team_id => 58, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322520087
g = Game.create! :week_id => 2, :away_team_id => 178, :home_team_id => 25, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322520097
g = Game.create! :week_id => 2, :away_team_id => 32, :home_team_id => 140, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322520113
g = Game.create! :week_id => 2, :away_team_id => 72, :home_team_id => 34, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322520130
g = Game.create! :week_id => 2, :away_team_id => 169, :home_team_id => 27, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322520164
g = Game.create! :week_id => 2, :away_team_id => 88, :home_team_id => 29, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322520183
g = Game.create! :week_id => 2, :away_team_id => 95, :home_team_id => 21, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322520245
g = Game.create! :week_id => 2, :away_team_id => 112, :home_team_id => 92, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322520333
g = Game.create! :week_id => 2, :away_team_id => 35, :home_team_id => 63, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322522117
g = Game.create! :week_id => 2, :away_team_id => 14, :home_team_id => 33, :start_time => Time.parse('Saturday, September 8 3:30 PM'), :espn_game_id => 322522294
g = Game.create! :week_id => 2, :away_team_id => 324, :home_team_id => 126, :start_time => Time.parse('Saturday, September 8 3:35 PM'), :espn_game_id => 322522458
g = Game.create! :week_id => 2, :away_team_id => 153, :home_team_id => 102, :start_time => Time.parse('Saturday, September 8 4:00 PM'), :espn_game_id => 322522633
g = Game.create! :week_id => 2, :away_team_id => 325, :home_team_id => 183, :start_time => Time.parse('Saturday, September 8 4:00 PM'), :espn_game_id => 322522710
g = Game.create! :week_id => 2, :away_team_id => 70, :home_team_id => 79, :start_time => Time.parse('Saturday, September 8 4:00 PM'), :espn_game_id => 322522751
g = Game.create! :week_id => 2, :away_team_id => 219, :home_team_id => 184, :start_time => Time.parse('Saturday, September 8 4:00 PM'), :espn_game_id => 322522754
g = Game.create! :week_id => 2, :away_team_id => 174, :home_team_id => 4, :start_time => Time.parse('Saturday, September 8 6:00 PM'), :espn_game_id => 322520052
g = Game.create! :week_id => 2, :away_team_id => 301, :home_team_id => 122, :start_time => Time.parse('Saturday, September 8 6:00 PM'), :espn_game_id => 322520304
g = Game.create! :week_id => 2, :away_team_id => 237, :home_team_id => 240, :start_time => Time.parse('Saturday, September 8 6:00 PM'), :espn_game_id => 322522029
g = Game.create! :week_id => 2, :away_team_id => 170, :home_team_id => 62, :start_time => Time.parse('Saturday, September 8 6:00 PM'), :espn_game_id => 322522084
g = Game.create! :week_id => 2, :away_team_id => 255, :home_team_id => 210, :start_time => Time.parse('Saturday, September 8 6:00 PM'), :espn_game_id => 322522086
g = Game.create! :week_id => 2, :away_team_id => 307, :home_team_id => 211, :start_time => Time.parse('Saturday, September 8 6:00 PM'), :espn_game_id => 322522097
g = Game.create! :week_id => 2, :away_team_id => 217, :home_team_id => 196, :start_time => Time.parse('Saturday, September 8 6:00 PM'), :espn_game_id => 322522198
g = Game.create! :week_id => 2, :away_team_id => 142, :home_team_id => 168, :start_time => Time.parse('Saturday, September 8 6:00 PM'), :espn_game_id => 322522261
g = Game.create! :week_id => 2, :away_team_id => 327, :home_team_id => 172, :start_time => Time.parse('Saturday, September 8 6:00 PM'), :espn_game_id => 322522448
g = Game.create! :week_id => 2, :away_team_id => 225, :home_team_id => 222, :start_time => Time.parse('Saturday, September 8 6:00 PM'), :espn_game_id => 322522643
g = Game.create! :week_id => 2, :away_team_id => 123, :home_team_id => 220, :start_time => Time.parse('Saturday, September 8 6:30 PM'), :espn_game_id => 322522026
g = Game.create! :week_id => 2, :away_team_id => 221, :home_team_id => 197, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322520055
g = Game.create! :week_id => 2, :away_team_id => 134, :home_team_id => 5, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322520059
g = Game.create! :week_id => 2, :away_team_id => 229, :home_team_id => 198, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322520093
g = Game.create! :week_id => 2, :away_team_id => 90, :home_team_id => 98, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322520099
g = Game.create! :week_id => 2, :away_team_id => 54, :home_team_id => 100, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322520145
g = Game.create! :week_id => 2, :away_team_id => 127, :home_team_id => 149, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322520155
g = Game.create! :week_id => 2, :away_team_id => 115, :home_team_id => 61, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322520189
g = Game.create! :week_id => 2, :away_team_id => 118, :home_team_id => 68, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322520195
g = Game.create! :week_id => 2, :away_team_id => 246, :home_team_id => 110, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322520249
g = Game.create! :week_id => 2, :away_team_id => 22, :home_team_id => 155, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322520326
g = Game.create! :week_id => 2, :away_team_id => 243, :home_team_id => 238, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522011
g = Game.create! :week_id => 2, :away_team_id => 46, :home_team_id => 104, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522032
g = Game.create! :week_id => 2, :away_team_id => 124, :home_team_id => 214, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522181
g = Game.create! :week_id => 2, :away_team_id => 173, :home_team_id => 223, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522210
g = Game.create! :week_id => 2, :away_team_id => 200, :home_team_id => 242, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522296
g = Game.create! :week_id => 2, :away_team_id => 212, :home_team_id => 285, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522331
g = Game.create! :week_id => 2, :away_team_id => 171, :home_team_id => 133, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522335
g = Game.create! :week_id => 2, :away_team_id => 93, :home_team_id => 108, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522433
g = Game.create! :week_id => 2, :away_team_id => 202, :home_team_id => 67, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522459
g = Game.create! :week_id => 2, :away_team_id => 328, :home_team_id => 233, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522466
g = Game.create! :week_id => 2, :away_team_id => 298, :home_team_id => 234, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522534
g = Game.create! :week_id => 2, :away_team_id => 283, :home_team_id => 199, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522546
g = Game.create! :week_id => 2, :away_team_id => 107, :home_team_id => 111, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522653
g = Game.create! :week_id => 2, :away_team_id => 195, :home_team_id => 71, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522711
g = Game.create! :week_id => 2, :away_team_id => 208, :home_team_id => 147, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522729
g = Game.create! :week_id => 2, :away_team_id => 329, :home_team_id => 228, :start_time => Time.parse('Saturday, September 8 7:00 PM'), :espn_game_id => 322522747
g = Game.create! :week_id => 2, :away_team_id => 65, :home_team_id => 97, :start_time => Time.parse('Saturday, September 8 7:30 PM'), :espn_game_id => 322520096
g = Game.create! :week_id => 2, :away_team_id => 96, :home_team_id => 17, :start_time => Time.parse('Saturday, September 8 7:45 PM'), :espn_game_id => 322520142
g = Game.create! :week_id => 2, :away_team_id => 152, :home_team_id => 119, :start_time => Time.parse('Saturday, September 8 8:00 PM'), :espn_game_id => 322520023
g = Game.create! :week_id => 2, :away_team_id => 103, :home_team_id => 38, :start_time => Time.parse('Saturday, September 8 8:00 PM'), :espn_game_id => 322520077
g = Game.create! :week_id => 2, :away_team_id => 239, :home_team_id => 138, :start_time => Time.parse('Saturday, September 8 8:00 PM'), :espn_game_id => 322520256
g = Game.create! :week_id => 2, :away_team_id => 247, :home_team_id => 231, :start_time => Time.parse('Saturday, September 8 8:00 PM'), :espn_game_id => 322522377
g = Game.create! :week_id => 2, :away_team_id => 289, :home_team_id => 218, :start_time => Time.parse('Saturday, September 8 9:00 PM'), :espn_game_id => 322520301
g = Game.create! :week_id => 2, :away_team_id => 31, :home_team_id => 81, :start_time => Time.parse('Saturday, September 8 10:30 PM'), :espn_game_id => 322520009
g = Game.create! :week_id => 3, :away_team_id => 333, :home_team_id => 221, :start_time => Time.parse('Thursday, September 13 7:00 PM'), :espn_game_id => 322570236
g = Game.create! :week_id => 3, :away_team_id => 235, :home_team_id => 202, :start_time => Time.parse('Thursday, September 13 7:00 PM'), :espn_game_id => 322572630
g = Game.create! :week_id => 3, :away_team_id => 27, :home_team_id => 28, :start_time => Time.parse('Thursday, September 13 7:30 PM'), :espn_game_id => 322570058
g = Game.create! :week_id => 3, :away_team_id => 243, :home_team_id => 245, :start_time => Time.parse('Thursday, September 13 7:30 PM'), :espn_game_id => 322572582
g = Game.create! :week_id => 3, :away_team_id => 91, :home_team_id => 78, :start_time => Time.parse('Friday, September 14 9:00 PM'), :espn_game_id => 322582439
g = Game.create! :week_id => 3, :away_team_id => 175, :home_team_id => 80, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590012
g = Game.create! :week_id => 3, :away_team_id => 149, :home_team_id => 76, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590021
g = Game.create! :week_id => 3, :away_team_id => 88, :home_team_id => 86, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590024
g = Game.create! :week_id => 3, :away_team_id => 44, :home_team_id => 87, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590026
g = Game.create! :week_id => 3, :away_team_id => 12, :home_team_id => 4, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322590052
g = Game.create! :week_id => 3, :away_team_id => 230, :home_team_id => 114, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590062
g = Game.create! :week_id => 3, :away_team_id => 183, :home_team_id => 14, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590066
g = Game.create! :week_id => 3, :away_team_id => 157, :home_team_id => 207, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590107
g = Game.create! :week_id => 3, :away_team_id => 147, :home_team_id => 145, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322590119
g = Game.create! :week_id => 3, :away_team_id => 71, :home_team_id => 36, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322590135
g = Game.create! :week_id => 3, :away_team_id => 236, :home_team_id => 124, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590147
g = Game.create! :week_id => 3, :away_team_id => 104, :home_team_id => 37, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322590158
g = Game.create! :week_id => 3, :away_team_id => 187, :home_team_id => 141, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322590160
g = Game.create! :week_id => 3, :away_team_id => 216, :home_team_id => 158, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590171
g = Game.create! :week_id => 3, :away_team_id => 82, :home_team_id => 39, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322590194
g = Game.create! :week_id => 3, :away_team_id => 107, :home_team_id => 19, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590197
g = Game.create! :week_id => 3, :away_team_id => 232, :home_team_id => 51, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590202
g = Game.create! :week_id => 3, :away_team_id => 11, :home_team_id => 26, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322590221
g = Game.create! :week_id => 3, :away_team_id => 143, :home_team_id => 146, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590222
g = Game.create! :week_id => 3, :away_team_id => 109, :home_team_id => 46, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590235
g = Game.create! :week_id => 3, :away_team_id => 234, :home_team_id => 13, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590239
g = Game.create! :week_id => 3, :away_team_id => 30, :home_team_id => 138, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590256
g = Game.create! :week_id => 3, :away_team_id => 127, :home_team_id => 90, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590264
g = Game.create! :week_id => 3, :away_team_id => 83, :home_team_id => 113, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590278
g = Game.create! :week_id => 3, :away_team_id => 196, :home_team_id => 131, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322590324
g = Game.create! :week_id => 3, :away_team_id => 67, :home_team_id => 55, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322590349
g = Game.create! :week_id => 3, :away_team_id => 130, :home_team_id => 31, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322590356
g = Game.create! :week_id => 3, :away_team_id => 222, :home_team_id => 220, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322592026
g = Game.create! :week_id => 3, :away_team_id => 106, :home_team_id => 53, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322592116
g = Game.create! :week_id => 3, :away_team_id => 190, :home_team_id => 213, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322592168
g = Game.create! :week_id => 3, :away_team_id => 156, :home_team_id => 153, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322592247
g = Game.create! :week_id => 3, :away_team_id => 77, :home_team_id => 15, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322592305
g = Game.create! :week_id => 3, :away_team_id => 110, :home_team_id => 16, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322592306
g = Game.create! :week_id => 3, :away_team_id => 165, :home_team_id => 7, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322592390
g = Game.create! :week_id => 3, :away_team_id => 233, :home_team_id => 117, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322592440
g = Game.create! :week_id => 3, :away_team_id => 201, :home_team_id => 84, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322592483
g = Game.create! :week_id => 3, :away_team_id => 64, :home_team_id => 41, :start_time => Time.parse('Saturday, September 15 12:00 PM'), :espn_game_id => 322592509
g = Game.create! :week_id => 3, :away_team_id => 43, :home_team_id => 49, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322592572
g = Game.create! :week_id => 3, :away_team_id => 194, :home_team_id => 200, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322592634
g = Game.create! :week_id => 3, :away_team_id => 75, :home_team_id => 22, :start_time => Time.parse('Saturday, September 15 10:59:59 PM'), :espn_game_id => 322592641
g = Game.create! :week_id => 3, :away_team_id => 108, :home_team_id => 94, :start_time => Time.parse('Saturday, September 15 12:21 PM'), :espn_game_id => 322590002
g = Game.create! :week_id => 3, :away_team_id => 218, :home_team_id => 161, :start_time => Time.parse('Saturday, September 15 12:30 PM'), :espn_game_id => 322590108
g = Game.create! :week_id => 3, :away_team_id => 24, :home_team_id => 6, :start_time => Time.parse('Saturday, September 15 12:30 PM'), :espn_game_id => 322590120
g = Game.create! :week_id => 3, :away_team_id => 134, :home_team_id => 103, :start_time => Time.parse('Saturday, September 15 12:30 PM'), :espn_game_id => 322590238
g = Game.create! :week_id => 3, :away_team_id => 163, :home_team_id => 209, :start_time => Time.parse('Saturday, September 15 12:30 PM'), :espn_game_id => 322592329
g = Game.create! :week_id => 3, :away_team_id => 164, :home_team_id => 206, :start_time => Time.parse('Saturday, September 15 1:00 PM'), :espn_game_id => 322590046
g = Game.create! :week_id => 3, :away_team_id => 191, :home_team_id => 204, :start_time => Time.parse('Saturday, September 15 1:00 PM'), :espn_game_id => 322592142
g = Game.create! :week_id => 3, :away_team_id => 159, :home_team_id => 205, :start_time => Time.parse('Saturday, September 15 1:00 PM'), :espn_game_id => 322592230
g = Game.create! :week_id => 3, :away_team_id => 192, :home_team_id => 217, :start_time => Time.parse('Saturday, September 15 1:00 PM'), :espn_game_id => 322592413
g = Game.create! :week_id => 3, :away_team_id => 189, :home_team_id => 193, :start_time => Time.parse('Saturday, September 15 1:00 PM'), :espn_game_id => 322592681
g = Game.create! :week_id => 3, :away_team_id => 139, :home_team_id => 186, :start_time => Time.parse('Saturday, September 15 1:00 PM'), :espn_game_id => 322592803
g = Game.create! :week_id => 3, :away_team_id => 144, :home_team_id => 136, :start_time => Time.parse('Saturday, September 15 1:30 PM'), :espn_game_id => 322592678
g = Game.create! :week_id => 3, :away_team_id => 330, :home_team_id => 215, :start_time => Time.parse('Saturday, September 15 2:00 PM'), :espn_game_id => 322590294
g = Game.create! :week_id => 3, :away_team_id => 195, :home_team_id => 176, :start_time => Time.parse('Saturday, September 15 2:00 PM'), :espn_game_id => 322592287
g = Game.create! :week_id => 3, :away_team_id => 188, :home_team_id => 219, :start_time => Time.parse('Saturday, September 15 2:00 PM'), :espn_game_id => 322592674
g = Game.create! :week_id => 3, :away_team_id => 214, :home_team_id => 177, :start_time => Time.parse('Saturday, September 15 2:05 PM'), :espn_game_id => 322590282
g = Game.create! :week_id => 3, :away_team_id => 224, :home_team_id => 2, :start_time => Time.parse('Saturday, September 15 3:00 PM'), :espn_game_id => 322590228
g = Game.create! :week_id => 3, :away_team_id => 327, :home_team_id => 223, :start_time => Time.parse('Saturday, September 15 3:00 PM'), :espn_game_id => 322592210
g = Game.create! :week_id => 3, :away_team_id => 226, :home_team_id => 132, :start_time => Time.parse('Saturday, September 15 3:00 PM'), :espn_game_id => 322592241
g = Game.create! :week_id => 3, :away_team_id => 152, :home_team_id => 181, :start_time => Time.parse('Saturday, September 15 3:00 PM'), :espn_game_id => 322592571
g = Game.create! :week_id => 3, :away_team_id => 92, :home_team_id => 93, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322590008
g = Game.create! :week_id => 3, :away_team_id => 203, :home_team_id => 137, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322590048
g = Game.create! :week_id => 3, :away_team_id => 10, :home_team_id => 5, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322590059
g = Game.create! :week_id => 3, :away_team_id => 1, :home_team_id => 38, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322590077
g = Game.create! :week_id => 3, :away_team_id => 8, :home_team_id => 25, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322590097
g = Game.create! :week_id => 3, :away_team_id => 140, :home_team_id => 34, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322590130
g = Game.create! :week_id => 3, :away_team_id => 133, :home_team_id => 123, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322590149
g = Game.create! :week_id => 3, :away_team_id => 57, :home_team_id => 40, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322590213
g = Game.create! :week_id => 3, :away_team_id => 170, :home_team_id => 59, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322592006
g = Game.create! :week_id => 3, :away_team_id => 180, :home_team_id => 33, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322592294
g = Game.create! :week_id => 3, :away_team_id => 21, :home_team_id => 48, :start_time => Time.parse('Saturday, September 15 3:30 PM'), :espn_game_id => 322592567
g = Game.create! :week_id => 3, :away_team_id => 66, :home_team_id => 73, :start_time => Time.parse('Saturday, September 15 4:00 PM'), :espn_game_id => 322590068
g = Game.create! :week_id => 3, :away_team_id => 135, :home_team_id => 29, :start_time => Time.parse('Saturday, September 15 4:00 PM'), :espn_game_id => 322590183
g = Game.create! :week_id => 3, :away_team_id => 169, :home_team_id => 171, :start_time => Time.parse('Saturday, September 15 4:00 PM'), :espn_game_id => 322592450
g = Game.create! :week_id => 3, :away_team_id => 185, :home_team_id => 184, :start_time => Time.parse('Saturday, September 15 4:00 PM'), :espn_game_id => 322592754
g = Game.create! :week_id => 3, :away_team_id => 240, :home_team_id => 239, :start_time => Time.parse('Saturday, September 15 5:00 PM'), :espn_game_id => 322592016
g = Game.create! :week_id => 3, :away_team_id => 126, :home_team_id => 128, :start_time => Time.parse('Saturday, September 15 5:05 PM'), :espn_game_id => 322590016
g = Game.create! :week_id => 3, :away_team_id => 168, :home_team_id => 167, :start_time => Time.parse('Saturday, September 15 6:00 PM'), :espn_game_id => 322590050
g = Game.create! :week_id => 3, :away_team_id => 154, :home_team_id => 9, :start_time => Time.parse('Saturday, September 15 6:00 PM'), :espn_game_id => 322590152
g = Game.create! :week_id => 3, :away_team_id => 211, :home_team_id => 142, :start_time => Time.parse('Saturday, September 15 6:00 PM'), :espn_game_id => 322590295
g = Game.create! :week_id => 3, :away_team_id => 162, :home_team_id => 208, :start_time => Time.parse('Saturday, September 15 6:00 PM'), :espn_game_id => 322590322
g = Game.create! :week_id => 3, :away_team_id => 262, :home_team_id => 172, :start_time => Time.parse('Saturday, September 15 6:00 PM'), :espn_game_id => 322592448
g = Game.create! :week_id => 3, :away_team_id => 95, :home_team_id => 102, :start_time => Time.parse('Saturday, September 15 6:00 PM'), :espn_game_id => 322592633
g = Game.create! :week_id => 3, :away_team_id => 148, :home_team_id => 79, :start_time => Time.parse('Saturday, September 15 6:00 PM'), :espn_game_id => 322592751
g = Game.create! :week_id => 3, :away_team_id => 68, :home_team_id => 45, :start_time => Time.parse('Saturday, September 15 6:30 PM'), :espn_game_id => 322590276
g = Game.create! :week_id => 3, :away_team_id => 199, :home_team_id => 182, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322590079
g = Game.create! :week_id => 3, :away_team_id => 112, :home_team_id => 97, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322590096
g = Game.create! :week_id => 3, :away_team_id => 81, :home_team_id => 17, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322590142
g = Game.create! :week_id => 3, :away_team_id => 173, :home_team_id => 3, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322590150
g = Game.create! :week_id => 3, :away_team_id => 210, :home_team_id => 160, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322590159
g = Game.create! :week_id => 3, :away_team_id => 244, :home_team_id => 237, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322592010
g = Game.create! :week_id => 3, :away_team_id => 300, :home_team_id => 229, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322592110
g = Game.create! :week_id => 3, :away_team_id => 166, :home_team_id => 23, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322592132
g = Game.create! :week_id => 3, :away_team_id => 47, :home_team_id => 116, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322592348
g = Game.create! :week_id => 3, :away_team_id => 52, :home_team_id => 101, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322592579
g = Game.create! :week_id => 3, :away_team_id => 61, :home_team_id => 70, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322592649
g = Game.create! :week_id => 3, :away_team_id => 99, :home_team_id => 111, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322592653
g = Game.create! :week_id => 3, :away_team_id => 227, :home_team_id => 228, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322592747
g = Game.create! :week_id => 3, :away_team_id => 238, :home_team_id => 241, :start_time => Time.parse('Saturday, September 15 7:00 PM'), :espn_game_id => 322592755
g = Game.create! :week_id => 3, :away_team_id => 284, :home_team_id => 125, :start_time => Time.parse('Saturday, September 15 7:05 PM'), :espn_game_id => 322592464
g = Game.create! :week_id => 3, :away_team_id => 105, :home_team_id => 96, :start_time => Time.parse('Saturday, September 15 7:30 PM'), :espn_game_id => 322590061
g = Game.create! :week_id => 3, :away_team_id => 74, :home_team_id => 119, :start_time => Time.parse('Saturday, September 15 8:00 PM'), :espn_game_id => 322590023
g = Game.create! :week_id => 3, :away_team_id => 60, :home_team_id => 32, :start_time => Time.parse('Saturday, September 15 8:00 PM'), :espn_game_id => 322590084
g = Game.create! :week_id => 3, :away_team_id => 115, :home_team_id => 98, :start_time => Time.parse('Saturday, September 15 8:00 PM'), :espn_game_id => 322590099
g = Game.create! :week_id => 3, :away_team_id => 58, :home_team_id => 35, :start_time => Time.parse('Saturday, September 15 8:00 PM'), :espn_game_id => 322590127
g = Game.create! :week_id => 3, :away_team_id => 326, :home_team_id => 151, :start_time => Time.parse('Saturday, September 15 8:00 PM'), :espn_game_id => 322590253
g = Game.create! :week_id => 3, :away_team_id => 120, :home_team_id => 42, :start_time => Time.parse('Saturday, September 15 8:00 PM'), :espn_game_id => 322590275
g = Game.create! :week_id => 3, :away_team_id => 198, :home_team_id => 178, :start_time => Time.parse('Saturday, September 15 8:00 PM'), :espn_game_id => 322592623
g = Game.create! :week_id => 3, :away_team_id => 118, :home_team_id => 54, :start_time => Time.parse('Saturday, September 15 8:00 PM'), :espn_game_id => 322592638
g = Game.create! :week_id => 3, :away_team_id => 231, :home_team_id => 129, :start_time => Time.parse('Saturday, September 15 8:00 PM'), :espn_game_id => 322592692
g = Game.create! :week_id => 3, :away_team_id => 242, :home_team_id => 246, :start_time => Time.parse('Saturday, September 15 8:30 PM'), :espn_game_id => 322592640
g = Game.create! :week_id => 3, :away_team_id => 20, :home_team_id => 100, :start_time => Time.parse('Saturday, September 15 9:15 PM'), :espn_game_id => 322590145
g = Game.create! :week_id => 3, :away_team_id => 56, :home_team_id => 89, :start_time => Time.parse('Saturday, September 15 10:00 PM'), :espn_game_id => 322590254
g = Game.create! :week_id => 3, :away_team_id => 65, :home_team_id => 62, :start_time => Time.parse('Wednesday, September 19 7:00 PM'), :espn_game_id => 322632084
g = Game.create! :week_id => 4, :away_team_id => 240, :home_team_id => 238, :start_time => Time.parse('Thursday, September 20 7:30 PM'), :espn_game_id => 322642011
g = Game.create! :week_id => 4, :away_team_id => 56, :home_team_id => 73, :start_time => Time.parse('Thursday, September 20 9:00 PM'), :espn_game_id => 322640068
g = Game.create! :week_id => 4, :away_team_id => 206, :home_team_id => 163, :start_time => Time.parse('Friday, September 21 7:00 PM'), :espn_game_id => 322650163
g = Game.create! :week_id => 4, :away_team_id => 13, :home_team_id => 108, :start_time => Time.parse('Friday, September 21 8:00 PM'), :espn_game_id => 322652433
g = Game.create! :week_id => 4, :away_team_id => 98, :home_team_id => 94, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660002
g = Game.create! :week_id => 4, :away_team_id => 27, :home_team_id => 93, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660008
g = Game.create! :week_id => 4, :away_team_id => 89, :home_team_id => 81, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660009
g = Game.create! :week_id => 4, :away_team_id => 119, :home_team_id => 76, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660021
g = Game.create! :week_id => 4, :away_team_id => 85, :home_team_id => 87, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660026
g = Game.create! :week_id => 4, :away_team_id => 82, :home_team_id => 88, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660030
g = Game.create! :week_id => 4, :away_team_id => 120, :home_team_id => 74, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660036
g = Game.create! :week_id => 4, :away_team_id => 2, :home_team_id => 4, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660052
g = Game.create! :week_id => 4, :away_team_id => 97, :home_team_id => 95, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660057
g = Game.create! :week_id => 4, :away_team_id => 7, :home_team_id => 5, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660059
g = Game.create! :week_id => 4, :away_team_id => 103, :home_team_id => 96, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660061
g = Game.create! :week_id => 4, :away_team_id => 117, :home_team_id => 114, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660062
g = Game.create! :week_id => 4, :away_team_id => 150, :home_team_id => 38, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660077
g = Game.create! :week_id => 4, :away_team_id => 160, :home_team_id => 207, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660107
g = Game.create! :week_id => 4, :away_team_id => 64, :home_team_id => 35, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660127
g = Game.create! :week_id => 4, :away_team_id => 46, :home_team_id => 3, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660150
g = Game.create! :week_id => 4, :away_team_id => 43, :home_team_id => 8, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660153
g = Game.create! :week_id => 4, :away_team_id => 55, :home_team_id => 12, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660154
g = Game.create! :week_id => 4, :away_team_id => 122, :home_team_id => 37, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660158
g = Game.create! :week_id => 4, :away_team_id => 205, :home_team_id => 158, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660171
g = Game.create! :week_id => 4, :away_team_id => 140, :home_team_id => 66, :start_time => Time.parse('Saturday, September 22 12:00 PM'), :espn_game_id => 322660193
g = Game.create! :week_id => 4, :away_team_id => 52, :home_team_id => 39, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660194
g = Game.create! :week_id => 4, :away_team_id => 16, :home_team_id => 18, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660201
g = Game.create! :week_id => 4, :away_team_id => 113, :home_team_id => 51, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660202
g = Game.create! :week_id => 4, :away_team_id => 69, :home_team_id => 40, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660213
g = Game.create! :week_id => 4, :away_team_id => 146, :home_team_id => 162, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660219
g = Game.create! :week_id => 4, :away_team_id => 45, :home_team_id => 47, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660242
g = Game.create! :week_id => 4, :away_team_id => 175, :home_team_id => 21, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660245
g = Game.create! :week_id => 4, :away_team_id => 111, :home_team_id => 110, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660249
g = Game.create! :week_id => 4, :away_team_id => 61, :home_team_id => 11, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660259
g = Game.create! :week_id => 4, :away_team_id => 83, :home_team_id => 91, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660265
g = Game.create! :week_id => 4, :away_team_id => 54, :home_team_id => 42, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660275
g = Game.create! :week_id => 4, :away_team_id => 6, :home_team_id => 30, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660277
g = Game.create! :week_id => 4, :away_team_id => 141, :home_team_id => 142, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660295
g = Game.create! :week_id => 4, :away_team_id => 185, :home_team_id => 139, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660311
g = Game.create! :week_id => 4, :away_team_id => 236, :home_team_id => 155, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660326
g = Game.create! :week_id => 4, :away_team_id => 105, :home_team_id => 92, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660333
g = Game.create! :week_id => 4, :away_team_id => 154, :home_team_id => 99, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660344
g = Game.create! :week_id => 4, :away_team_id => 116, :home_team_id => 31, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322660356
g = Game.create! :week_id => 4, :away_team_id => 208, :home_team_id => 203, :start_time => Time.parse('Saturday, September 22 12:00 PM'), :espn_game_id => 322662083
g = Game.create! :week_id => 4, :away_team_id => 211, :home_team_id => 210, :start_time => Time.parse('Saturday, September 22 12:00 PM'), :espn_game_id => 322662086
g = Game.create! :week_id => 4, :away_team_id => 193, :home_team_id => 187, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662115
g = Game.create! :week_id => 4, :away_team_id => 144, :home_team_id => 153, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662247
g = Game.create! :week_id => 4, :away_team_id => 63, :home_team_id => 33, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662294
g = Game.create! :week_id => 4, :away_team_id => 253, :home_team_id => 230, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662320
g = Game.create! :week_id => 4, :away_team_id => 212, :home_team_id => 216, :start_time => Time.parse('Saturday, September 22 12:00 PM'), :espn_game_id => 322662368
g = Game.create! :week_id => 4, :away_team_id => 136, :home_team_id => 57, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662426
g = Game.create! :week_id => 4, :away_team_id => 72, :home_team_id => 78, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662439
g = Game.create! :week_id => 4, :away_team_id => 15, :home_team_id => 67, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662459
g = Game.create! :week_id => 4, :away_team_id => 80, :home_team_id => 84, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662483
g = Game.create! :week_id => 4, :away_team_id => 151, :home_team_id => 127, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662502
g = Game.create! :week_id => 4, :away_team_id => 224, :home_team_id => 134, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662506
g = Game.create! :week_id => 4, :away_team_id => 231, :home_team_id => 235, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662545
g = Game.create! :week_id => 4, :away_team_id => 130, :home_team_id => 312, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662560
g = Game.create! :week_id => 4, :away_team_id => 17, :home_team_id => 101, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662579
g = Game.create! :week_id => 4, :away_team_id => 204, :home_team_id => 135, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662619
g = Game.create! :week_id => 4, :away_team_id => 10, :home_team_id => 77, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662628
g = Game.create! :week_id => 4, :away_team_id => 59, :home_team_id => 102, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662633
g = Game.create! :week_id => 4, :away_team_id => 237, :home_team_id => 246, :start_time => Time.parse('Saturday, September 22 12:00 PM'), :espn_game_id => 322662640
g = Game.create! :week_id => 4, :away_team_id => 100, :home_team_id => 50, :start_time => Time.parse('Saturday, September 22 12:00 PM'), :espn_game_id => 322662655
g = Game.create! :week_id => 4, :away_team_id => 137, :home_team_id => 147, :start_time => Time.parse('Saturday, September 22 10:59:59 PM'), :espn_game_id => 322662729
g = Game.create! :week_id => 4, :away_team_id => 164, :home_team_id => 159, :start_time => Time.parse('Saturday, September 22 1:00 PM'), :espn_game_id => 322660172
g = Game.create! :week_id => 4, :away_team_id => 138, :home_team_id => 143, :start_time => Time.parse('Saturday, September 22 1:00 PM'), :espn_game_id => 322660227
g = Game.create! :week_id => 4, :away_team_id => 215, :home_team_id => 213, :start_time => Time.parse('Saturday, September 22 1:00 PM'), :espn_game_id => 322662168
g = Game.create! :week_id => 4, :away_team_id => 191, :home_team_id => 189, :start_time => Time.parse('Saturday, September 22 1:00 PM'), :espn_game_id => 322662405
g = Game.create! :week_id => 4, :away_team_id => 188, :home_team_id => 186, :start_time => Time.parse('Saturday, September 22 1:00 PM'), :espn_game_id => 322662803
g = Game.create! :week_id => 4, :away_team_id => 171, :home_team_id => 68, :start_time => Time.parse('Saturday, September 22 2:00 PM'), :espn_game_id => 322660195
g = Game.create! :week_id => 4, :away_team_id => 217, :home_team_id => 214, :start_time => Time.parse('Saturday, September 22 2:00 PM'), :espn_game_id => 322662181
g = Game.create! :week_id => 4, :away_team_id => 182, :home_team_id => 178, :start_time => Time.parse('Saturday, September 22 2:00 PM'), :espn_game_id => 322662623
g = Game.create! :week_id => 4, :away_team_id => 293, :home_team_id => 156, :start_time => Time.parse('Saturday, September 22 2:00 PM'), :espn_game_id => 322662636
g = Game.create! :week_id => 4, :away_team_id => 24, :home_team_id => 71, :start_time => Time.parse('Saturday, September 22 2:00 PM'), :espn_game_id => 322662711
g = Game.create! :week_id => 4, :away_team_id => 181, :home_team_id => 177, :start_time => Time.parse('Saturday, September 22 2:05 PM'), :espn_game_id => 322660282
g = Game.create! :week_id => 4, :away_team_id => 125, :home_team_id => 123, :start_time => Time.parse('Saturday, September 22 3:30 PM'), :espn_game_id => 322660149
g = Game.create! :week_id => 4, :away_team_id => 132, :home_team_id => 26, :start_time => Time.parse('Saturday, September 22 3:30 PM'), :espn_game_id => 322660221
g = Game.create! :week_id => 4, :away_team_id => 226, :home_team_id => 227, :start_time => Time.parse('Saturday, September 22 3:30 PM'), :espn_game_id => 322662717
g = Game.create! :week_id => 4, :away_team_id => 126, :home_team_id => 124, :start_time => Time.parse('Saturday, September 22 3:35 PM'), :espn_game_id => 322660147
g = Game.create! :week_id => 4, :away_team_id => 219, :home_team_id => 218, :start_time => Time.parse('Saturday, September 22 4:00 PM'), :espn_game_id => 322660301
g = Game.create! :week_id => 4, :away_team_id => 200, :home_team_id => 165, :start_time => Time.parse('Saturday, September 22 4:00 PM'), :espn_game_id => 322662065
g = Game.create! :week_id => 4, :away_team_id => 234, :home_team_id => 229, :start_time => Time.parse('Saturday, September 22 4:00 PM'), :espn_game_id => 322662110
g = Game.create! :week_id => 4, :away_team_id => 244, :home_team_id => 179, :start_time => Time.parse('Saturday, September 22 4:00 PM'), :espn_game_id => 322662449
g = Game.create! :week_id => 4, :away_team_id => 176, :home_team_id => 183, :start_time => Time.parse('Saturday, September 22 4:00 PM'), :espn_game_id => 322662710
g = Game.create! :week_id => 4, :away_team_id => 161, :home_team_id => 157, :start_time => Time.parse('Saturday, September 22 4:30 PM'), :espn_game_id => 322660225
g = Game.create! :week_id => 4, :away_team_id => 28, :home_team_id => 60, :start_time => Time.parse('Saturday, September 22 4:30 PM'), :espn_game_id => 322662050
g = Game.create! :week_id => 4, :away_team_id => 79, :home_team_id => 115, :start_time => Time.parse('Saturday, September 22 5:00 PM'), :espn_game_id => 322660070
g = Game.create! :week_id => 4, :away_team_id => 197, :home_team_id => 196, :start_time => Time.parse('Saturday, September 22 5:00 PM'), :espn_game_id => 322662198
g = Game.create! :week_id => 4, :away_team_id => 245, :home_team_id => 242, :start_time => Time.parse('Saturday, September 22 5:00 PM'), :espn_game_id => 322662296
g = Game.create! :week_id => 4, :away_team_id => 222, :home_team_id => 9, :start_time => Time.parse('Saturday, September 22 6:00 PM'), :espn_game_id => 322660152
g = Game.create! :week_id => 4, :away_team_id => 220, :home_team_id => 221, :start_time => Time.parse('Saturday, September 22 6:00 PM'), :espn_game_id => 322660236
g = Game.create! :week_id => 4, :away_team_id => 223, :home_team_id => 225, :start_time => Time.parse('Saturday, September 22 6:00 PM'), :espn_game_id => 322660290
g = Game.create! :week_id => 4, :away_team_id => 167, :home_team_id => 166, :start_time => Time.parse('Saturday, September 22 6:00 PM'), :espn_game_id => 322662169
g = Game.create! :week_id => 4, :away_team_id => 49, :home_team_id => 112, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322660098
g = Game.create! :week_id => 4, :away_team_id => 192, :home_team_id => 145, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322660119
g = Game.create! :week_id => 4, :away_team_id => 239, :home_team_id => 104, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322662032
g = Game.create! :week_id => 4, :away_team_id => 202, :home_team_id => 194, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322662046
g = Game.create! :week_id => 4, :away_team_id => 25, :home_team_id => 106, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322662229
g = Game.create! :week_id => 4, :away_team_id => 209, :home_team_id => 133, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322662335
g = Game.create! :week_id => 4, :away_team_id => 258, :home_team_id => 232, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322662447
g = Game.create! :week_id => 4, :away_team_id => 243, :home_team_id => 233, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322662466
g = Game.create! :week_id => 4, :away_team_id => 173, :home_team_id => 174, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322662542
g = Game.create! :week_id => 4, :away_team_id => 201, :home_team_id => 199, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322662546
g = Game.create! :week_id => 4, :away_team_id => 131, :home_team_id => 70, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322662649
g = Game.create! :week_id => 4, :away_team_id => 180, :home_team_id => 184, :start_time => Time.parse('Saturday, September 22 7:00 PM'), :espn_game_id => 322662754
g = Game.create! :week_id => 4, :away_team_id => 34, :home_team_id => 58, :start_time => Time.parse('Saturday, September 22 7:30 PM'), :espn_game_id => 322660087
g = Game.create! :week_id => 4, :away_team_id => 198, :home_team_id => 195, :start_time => Time.parse('Saturday, September 22 7:30 PM'), :espn_game_id => 322662197
g = Game.create! :week_id => 4, :away_team_id => 29, :home_team_id => 36, :start_time => Time.parse('Saturday, September 22 8:00 PM'), :espn_game_id => 322660135
g = Game.create! :week_id => 4, :away_team_id => 75, :home_team_id => 118, :start_time => Time.parse('Saturday, September 22 8:00 PM'), :espn_game_id => 322660166
g = Game.create! :week_id => 4, :away_team_id => 121, :home_team_id => 129, :start_time => Time.parse('Saturday, September 22 8:00 PM'), :espn_game_id => 322662692
g = Game.create! :week_id => 4, :away_team_id => 152, :home_team_id => 148, :start_time => Time.parse('Saturday, September 22 9:05 PM'), :espn_game_id => 322660013
g = Game.create! :week_id => 4, :away_team_id => 149, :home_team_id => 128, :start_time => Time.parse('Saturday, September 22 9:05 PM'), :espn_game_id => 322660016
g = Game.create! :week_id => 5, :away_team_id => 170, :home_team_id => 172, :start_time => Time.parse('Thursday, September 27 7:30 PM'), :espn_game_id => 322712448
g = Game.create! :week_id => 5, :away_team_id => 234, :home_team_id => 246, :start_time => Time.parse('Thursday, September 27 8:00 PM'), :espn_game_id => 322712640
g = Game.create! :week_id => 5, :away_team_id => 86, :home_team_id => 90, :start_time => Time.parse('Thursday, September 27 9:00 PM'), :espn_game_id => 322710264
g = Game.create! :week_id => 5, :away_team_id => 207, :home_team_id => 161, :start_time => Time.parse('Friday, September 28 7:00 PM'), :espn_game_id => 322720108
g = Game.create! :week_id => 5, :away_team_id => 114, :home_team_id => 56, :start_time => Time.parse('Friday, September 28 8:00 PM'), :espn_game_id => 322720252
g = Game.create! :week_id => 5, :away_team_id => 51, :home_team_id => 52, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730005
g = Game.create! :week_id => 5, :away_team_id => 111, :home_team_id => 154, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730006
g = Game.create! :week_id => 5, :away_team_id => 85, :home_team_id => 80, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730012
g = Game.create! :week_id => 5, :away_team_id => 81, :home_team_id => 82, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730025
g = Game.create! :week_id => 5, :away_team_id => 87, :home_team_id => 83, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730038
g = Game.create! :week_id => 5, :away_team_id => 62, :home_team_id => 24, :start_time => Time.parse('Saturday, September 29 12:00 PM'), :espn_game_id => 322730041
g = Game.create! :week_id => 5, :away_team_id => 4, :home_team_id => 28, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730058
g = Game.create! :week_id => 5, :away_team_id => 102, :home_team_id => 96, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730061
g = Game.create! :week_id => 5, :away_team_id => 22, :home_team_id => 14, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730066
g = Game.create! :week_id => 5, :away_team_id => 32, :home_team_id => 38, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730077
g = Game.create! :week_id => 5, :away_team_id => 101, :home_team_id => 97, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730096
g = Game.create! :week_id => 5, :away_team_id => 145, :home_team_id => 98, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730099
g = Game.create! :week_id => 5, :away_team_id => 2, :home_team_id => 1, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730103
g = Game.create! :week_id => 5, :away_team_id => 39, :home_team_id => 35, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730127
g = Game.create! :week_id => 5, :away_team_id => 54, :home_team_id => 43, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730151
g = Game.create! :week_id => 5, :away_team_id => 115, :home_team_id => 8, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730153
g = Game.create! :week_id => 5, :away_team_id => 3, :home_team_id => 12, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730154
g = Game.create! :week_id => 5, :away_team_id => 162, :home_team_id => 160, :start_time => Time.parse('Saturday, September 29 12:00 PM'), :espn_game_id => 322730159
g = Game.create! :week_id => 5, :away_team_id => 137, :home_team_id => 141, :start_time => Time.parse('Saturday, September 29 12:00 PM'), :espn_game_id => 322730160
g = Game.create! :week_id => 5, :away_team_id => 73, :home_team_id => 75, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730167
g = Game.create! :week_id => 5, :away_team_id => 163, :home_team_id => 158, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730171
g = Game.create! :week_id => 5, :away_team_id => 20, :home_team_id => 19, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730197
g = Game.create! :week_id => 5, :away_team_id => 44, :home_team_id => 47, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730242
g = Game.create! :week_id => 5, :away_team_id => 93, :home_team_id => 21, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730245
g = Game.create! :week_id => 5, :away_team_id => 142, :home_team_id => 144, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730257
g = Game.create! :week_id => 5, :away_team_id => 116, :home_team_id => 10, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730258
g = Game.create! :week_id => 5, :away_team_id => 84, :home_team_id => 91, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730265
g = Game.create! :week_id => 5, :away_team_id => 13, :home_team_id => 30, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730277
g = Game.create! :week_id => 5, :away_team_id => 76, :home_team_id => 113, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730278
g = Game.create! :week_id => 5, :away_team_id => 216, :home_team_id => 215, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730294
g = Game.create! :week_id => 5, :away_team_id => 106, :home_team_id => 107, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730309
g = Game.create! :week_id => 5, :away_team_id => 146, :home_team_id => 139, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730311
g = Game.create! :week_id => 5, :away_team_id => 117, :home_team_id => 155, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730326
g = Game.create! :week_id => 5, :away_team_id => 100, :home_team_id => 92, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730333
g = Game.create! :week_id => 5, :away_team_id => 135, :home_team_id => 55, :start_time => Time.parse('Saturday, September 29 12:00 PM'), :espn_game_id => 322730349
g = Game.create! :week_id => 5, :away_team_id => 40, :home_team_id => 31, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730356
g = Game.create! :week_id => 5, :away_team_id => 189, :home_team_id => 185, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322730399
g = Game.create! :week_id => 5, :away_team_id => 74, :home_team_id => 72, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732005
g = Game.create! :week_id => 5, :away_team_id => 131, :home_team_id => 220, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732026
g = Game.create! :week_id => 5, :away_team_id => 112, :home_team_id => 104, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732032
g = Game.create! :week_id => 5, :away_team_id => 17, :home_team_id => 53, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732116
g = Game.create! :week_id => 5, :away_team_id => 11, :home_team_id => 23, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732132
g = Game.create! :week_id => 5, :away_team_id => 110, :home_team_id => 105, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732226
g = Game.create! :week_id => 5, :away_team_id => 36, :home_team_id => 33, :start_time => Time.parse('Saturday, September 29 12:00 PM'), :espn_game_id => 322732294
g = Game.create! :week_id => 5, :away_team_id => 60, :home_team_id => 65, :start_time => Time.parse('Saturday, September 29 12:00 PM'), :espn_game_id => 322732309
g = Game.create! :week_id => 5, :away_team_id => 235, :home_team_id => 230, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732320
g = Game.create! :week_id => 5, :away_team_id => 9, :home_team_id => 7, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732390
g = Game.create! :week_id => 5, :away_team_id => 119, :home_team_id => 57, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732426
g = Game.create! :week_id => 5, :away_team_id => 179, :home_team_id => 180, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732460
g = Game.create! :week_id => 5, :away_team_id => 45, :home_team_id => 41, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732509
g = Game.create! :week_id => 5, :away_team_id => 77, :home_team_id => 48, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732567
g = Game.create! :week_id => 5, :away_team_id => 25, :home_team_id => 49, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732572
g = Game.create! :week_id => 5, :away_team_id => 196, :home_team_id => 202, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732630
g = Game.create! :week_id => 5, :away_team_id => 240, :home_team_id => 200, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732634
g = Game.create! :week_id => 5, :away_team_id => 108, :home_team_id => 50, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732655
g = Game.create! :week_id => 5, :away_team_id => 70, :home_team_id => 71, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732711
g = Game.create! :week_id => 5, :away_team_id => 153, :home_team_id => 147, :start_time => Time.parse('Saturday, September 29 10:59:59 PM'), :espn_game_id => 322732729
g = Game.create! :week_id => 5, :away_team_id => 204, :home_team_id => 164, :start_time => Time.parse('Saturday, September 29 12:30 PM'), :espn_game_id => 322730043
g = Game.create! :week_id => 5, :away_team_id => 205, :home_team_id => 209, :start_time => Time.parse('Saturday, September 29 12:30 PM'), :espn_game_id => 322732329
g = Game.create! :week_id => 5, :away_team_id => 174, :home_team_id => 169, :start_time => Time.parse('Saturday, September 29 1:00 PM'), :espn_game_id => 322730047
g = Game.create! :week_id => 5, :away_team_id => 213, :home_team_id => 210, :start_time => Time.parse('Saturday, September 29 1:00 PM'), :espn_game_id => 322732086
g = Game.create! :week_id => 5, :away_team_id => 192, :home_team_id => 188, :start_time => Time.parse('Saturday, September 29 1:00 PM'), :espn_game_id => 322732184
g = Game.create! :week_id => 5, :away_team_id => 187, :home_team_id => 191, :start_time => Time.parse('Saturday, September 29 1:00 PM'), :espn_game_id => 322732529
g = Game.create! :week_id => 5, :away_team_id => 186, :home_team_id => 193, :start_time => Time.parse('Saturday, September 29 1:00 PM'), :espn_game_id => 322732681
g = Game.create! :week_id => 5, :away_team_id => 227, :home_team_id => 224, :start_time => Time.parse('Saturday, September 29 1:30 PM'), :espn_game_id => 322730231
g = Game.create! :week_id => 5, :away_team_id => 228, :home_team_id => 223, :start_time => Time.parse('Saturday, September 29 1:30 PM'), :espn_game_id => 322732210
g = Game.create! :week_id => 5, :away_team_id => 157, :home_team_id => 206, :start_time => Time.parse('Saturday, September 29 2:00 PM'), :espn_game_id => 322730046
g = Game.create! :week_id => 5, :away_team_id => 66, :home_team_id => 59, :start_time => Time.parse('Saturday, September 29 2:00 PM'), :espn_game_id => 322732006
g = Game.create! :week_id => 5, :away_team_id => 171, :home_team_id => 175, :start_time => Time.parse('Saturday, September 29 2:00 PM'), :espn_game_id => 322732569
g = Game.create! :week_id => 5, :away_team_id => 194, :home_team_id => 195, :start_time => Time.parse('Saturday, September 29 2:30 PM'), :espn_game_id => 322732197
g = Game.create! :week_id => 5, :away_team_id => 176, :home_team_id => 150, :start_time => Time.parse('Saturday, September 29 3:00 PM'), :espn_game_id => 322730233
g = Game.create! :week_id => 5, :away_team_id => 238, :home_team_id => 239, :start_time => Time.parse('Saturday, September 29 3:00 PM'), :espn_game_id => 322732016
g = Game.create! :week_id => 5, :away_team_id => 68, :home_team_id => 140, :start_time => Time.parse('Saturday, September 29 3:30 PM'), :espn_game_id => 322730113
g = Game.create! :week_id => 5, :away_team_id => 143, :home_team_id => 61, :start_time => Time.parse('Saturday, September 29 3:30 PM'), :espn_game_id => 322730189
g = Game.create! :week_id => 5, :away_team_id => 124, :home_team_id => 151, :start_time => Time.parse('Saturday, September 29 3:30 PM'), :espn_game_id => 322730253
g = Game.create! :week_id => 5, :away_team_id => 128, :home_team_id => 122, :start_time => Time.parse('Saturday, September 29 3:30 PM'), :espn_game_id => 322730304
g = Game.create! :week_id => 5, :away_team_id => 63, :home_team_id => 67, :start_time => Time.parse('Saturday, September 29 3:30 PM'), :espn_game_id => 322732459
g = Game.create! :week_id => 5, :away_team_id => 199, :home_team_id => 197, :start_time => Time.parse('Saturday, September 29 4:00 PM'), :espn_game_id => 322730055
g = Game.create! :week_id => 5, :away_team_id => 167, :home_team_id => 245, :start_time => Time.parse('Saturday, September 29 4:30 PM'), :espn_game_id => 322732582
g = Game.create! :week_id => 5, :away_team_id => 244, :home_team_id => 242, :start_time => Time.parse('Saturday, September 29 5:00 PM'), :espn_game_id => 322732296
g = Game.create! :week_id => 5, :away_team_id => 226, :home_team_id => 225, :start_time => Time.parse('Saturday, September 29 6:00 PM'), :espn_game_id => 322730290
g = Game.create! :week_id => 5, :away_team_id => 159, :home_team_id => 203, :start_time => Time.parse('Saturday, September 29 6:00 PM'), :espn_game_id => 322732083
g = Game.create! :week_id => 5, :away_team_id => 214, :home_team_id => 211, :start_time => Time.parse('Saturday, September 29 6:00 PM'), :espn_game_id => 322732097
g = Game.create! :week_id => 5, :away_team_id => 134, :home_team_id => 212, :start_time => Time.parse('Saturday, September 29 6:00 PM'), :espn_game_id => 322732166
g = Game.create! :week_id => 5, :away_team_id => 165, :home_team_id => 168, :start_time => Time.parse('Saturday, September 29 6:00 PM'), :espn_game_id => 322732261
g = Game.create! :week_id => 5, :away_team_id => 208, :home_team_id => 190, :start_time => Time.parse('Saturday, September 29 6:00 PM'), :espn_game_id => 322732523
g = Game.create! :week_id => 5, :away_team_id => 221, :home_team_id => 222, :start_time => Time.parse('Saturday, September 29 6:00 PM'), :espn_game_id => 322732643
g = Game.create! :week_id => 5, :away_team_id => 109, :home_team_id => 5, :start_time => Time.parse('Saturday, September 29 7:00 PM'), :espn_game_id => 322730059
g = Game.create! :week_id => 5, :away_team_id => 177, :home_team_id => 182, :start_time => Time.parse('Saturday, September 29 7:00 PM'), :espn_game_id => 322730079
g = Game.create! :week_id => 5, :away_team_id => 201, :home_team_id => 198, :start_time => Time.parse('Saturday, September 29 7:00 PM'), :espn_game_id => 322730093
g = Game.create! :week_id => 5, :away_team_id => 148, :home_team_id => 149, :start_time => Time.parse('Saturday, September 29 7:00 PM'), :espn_game_id => 322730155
g = Game.create! :week_id => 5, :away_team_id => 241, :home_team_id => 237, :start_time => Time.parse('Saturday, September 29 7:00 PM'), :espn_game_id => 322732010
g = Game.create! :week_id => 5, :away_team_id => 178, :home_team_id => 181, :start_time => Time.parse('Saturday, September 29 7:00 PM'), :espn_game_id => 322732571
g = Game.create! :week_id => 5, :away_team_id => 229, :home_team_id => 236, :start_time => Time.parse('Saturday, September 29 7:00 PM'), :espn_game_id => 322732617
g = Game.create! :week_id => 5, :away_team_id => 123, :home_team_id => 121, :start_time => Time.parse('Saturday, September 29 7:05 PM'), :espn_game_id => 322730331
g = Game.create! :week_id => 5, :away_team_id => 127, :home_team_id => 125, :start_time => Time.parse('Saturday, September 29 7:05 PM'), :espn_game_id => 322732464
g = Game.create! :week_id => 5, :away_team_id => 42, :home_team_id => 37, :start_time => Time.parse('Saturday, September 29 8:00 PM'), :espn_game_id => 322730158
g = Game.create! :week_id => 5, :away_team_id => 156, :home_team_id => 118, :start_time => Time.parse('Saturday, September 29 8:00 PM'), :espn_game_id => 322730166
g = Game.create! :week_id => 5, :away_team_id => 78, :home_team_id => 120, :start_time => Time.parse('Saturday, September 29 8:00 PM'), :espn_game_id => 322730328
g = Game.create! :week_id => 5, :away_team_id => 233, :home_team_id => 231, :start_time => Time.parse('Saturday, September 29 8:00 PM'), :espn_game_id => 322732377
g = Game.create! :week_id => 5, :away_team_id => 129, :home_team_id => 152, :start_time => Time.parse('Saturday, September 29 9:00 PM'), :espn_game_id => 322730302
g = Game.create! :week_id => 6, :away_team_id => 43, :home_team_id => 53, :start_time => Time.parse('Thursday, October 4 10:59:59 PM'), :espn_game_id => 322782116
g = Game.create! :week_id => 6, :away_team_id => 104, :home_team_id => 106, :start_time => Time.parse('Thursday, October 4 7:30 PM'), :espn_game_id => 322782229
g = Game.create! :week_id => 6, :away_team_id => 88, :home_team_id => 89, :start_time => Time.parse('Thursday, October 4 9:00 PM'), :espn_game_id => 322780254
g = Game.create! :week_id => 6, :away_team_id => 26, :home_team_id => 29, :start_time => Time.parse('Friday, October 5 7:00 PM'), :espn_game_id => 322790183
g = Game.create! :week_id => 6, :away_team_id => 148, :home_team_id => 129, :start_time => Time.parse('Friday, October 5 8:00 PM'), :espn_game_id => 322792692
g = Game.create! :week_id => 6, :away_team_id => 120, :home_team_id => 56, :start_time => Time.parse('Friday, October 5 10:15 PM'), :espn_game_id => 322790252
g = Game.create! :week_id => 6, :away_team_id => 57, :home_team_id => 72, :start_time => Time.parse('Saturday, October 6 11:30 AM'), :espn_game_id => 322802005
g = Game.create! :week_id => 6, :away_team_id => 93, :home_team_id => 94, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800002
g = Game.create! :week_id => 6, :away_team_id => 235, :home_team_id => 52, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800005
g = Game.create! :week_id => 6, :away_team_id => 114, :home_team_id => 76, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800021
g = Game.create! :week_id => 6, :away_team_id => 80, :home_team_id => 86, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800024
g = Game.create! :week_id => 6, :away_team_id => 87, :home_team_id => 82, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800025
g = Game.create! :week_id => 6, :away_team_id => 113, :home_team_id => 74, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800036
g = Game.create! :week_id => 6, :away_team_id => 98, :home_team_id => 95, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800057
g = Game.create! :week_id => 6, :away_team_id => 35, :home_team_id => 32, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800084
g = Game.create! :week_id => 6, :away_team_id => 99, :home_team_id => 97, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800096
g = Game.create! :week_id => 6, :away_team_id => 203, :home_team_id => 207, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800107
g = Game.create! :week_id => 6, :away_team_id => 12, :home_team_id => 6, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800120
g = Game.create! :week_id => 6, :away_team_id => 103, :home_team_id => 17, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800142
g = Game.create! :week_id => 6, :away_team_id => 21, :home_team_id => 100, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800145
g = Game.create! :week_id => 6, :away_team_id => 10, :home_team_id => 3, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800150
g = Game.create! :week_id => 6, :away_team_id => 4, :home_team_id => 9, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800152
g = Game.create! :week_id => 6, :away_team_id => 11, :home_team_id => 8, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800153
g = Game.create! :week_id => 6, :away_team_id => 24, :home_team_id => 27, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800164
g = Game.create! :week_id => 6, :away_team_id => 155, :home_team_id => 75, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800167
g = Game.create! :week_id => 6, :away_team_id => 37, :home_team_id => 39, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800194
g = Game.create! :week_id => 6, :away_team_id => 62, :home_team_id => 68, :start_time => Time.parse('Saturday, October 6 12:00 PM'), :espn_game_id => 322800195
g = Game.create! :week_id => 6, :away_team_id => 91, :home_team_id => 85, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800204
g = Game.create! :week_id => 6, :away_team_id => 38, :home_team_id => 40, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800213
g = Game.create! :week_id => 6, :away_team_id => 28, :home_team_id => 69, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800218
g = Game.create! :week_id => 6, :away_team_id => 147, :home_team_id => 162, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800219
g = Game.create! :week_id => 6, :away_team_id => 144, :home_team_id => 146, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800222
g = Game.create! :week_id => 6, :away_team_id => 5, :home_team_id => 2, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800228
g = Game.create! :week_id => 6, :away_team_id => 47, :home_team_id => 46, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800235
g = Game.create! :week_id => 6, :away_team_id => 110, :home_team_id => 44, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800248
g = Game.create! :week_id => 6, :away_team_id => 30, :home_team_id => 20, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800251
g = Game.create! :week_id => 6, :away_team_id => 128, :home_team_id => 151, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800253
g = Game.create! :week_id => 6, :away_team_id => 145, :home_team_id => 138, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800256
g = Game.create! :week_id => 6, :away_team_id => 51, :home_team_id => 45, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800276
g = Game.create! :week_id => 6, :away_team_id => 50, :home_team_id => 107, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322800309
g = Game.create! :week_id => 6, :away_team_id => 1, :home_team_id => 55, :start_time => Time.parse('Saturday, October 6 12:00 PM'), :espn_game_id => 322800349
g = Game.create! :week_id => 6, :away_team_id => 223, :home_team_id => 220, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802026
g = Game.create! :week_id => 6, :away_team_id => 67, :home_team_id => 60, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802050
g = Game.create! :week_id => 6, :away_team_id => 141, :home_team_id => 153, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802247
g = Game.create! :week_id => 6, :away_team_id => 15, :home_team_id => 16, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802306
g = Game.create! :week_id => 6, :away_team_id => 108, :home_team_id => 109, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802393
g = Game.create! :week_id => 6, :away_team_id => 79, :home_team_id => 117, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802440
g = Game.create! :week_id => 6, :away_team_id => 90, :home_team_id => 84, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802483
g = Game.create! :week_id => 6, :away_team_id => 122, :home_team_id => 127, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802502
g = Game.create! :week_id => 6, :away_team_id => 34, :home_team_id => 41, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802509
g = Game.create! :week_id => 6, :away_team_id => 73, :home_team_id => 49, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802572
g = Game.create! :week_id => 6, :away_team_id => 96, :home_team_id => 101, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802579
g = Game.create! :week_id => 6, :away_team_id => 190, :home_team_id => 192, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802598
g = Game.create! :week_id => 6, :away_team_id => 130, :home_team_id => 135, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802619
g = Game.create! :week_id => 6, :away_team_id => 14, :home_team_id => 77, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802628
g = Game.create! :week_id => 6, :away_team_id => 196, :home_team_id => 200, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802634
g = Game.create! :week_id => 6, :away_team_id => 48, :home_team_id => 54, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802638
g = Game.create! :week_id => 6, :away_team_id => 18, :home_team_id => 22, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802641
g = Game.create! :week_id => 6, :away_team_id => 224, :home_team_id => 228, :start_time => Time.parse('Saturday, October 6 10:59:59 PM'), :espn_game_id => 322802747
g = Game.create! :week_id => 6, :away_team_id => 160, :home_team_id => 164, :start_time => Time.parse('Saturday, October 6 12:30 PM'), :espn_game_id => 322800043
g = Game.create! :week_id => 6, :away_team_id => 158, :home_team_id => 209, :start_time => Time.parse('Saturday, October 6 12:30 PM'), :espn_game_id => 322802329
g = Game.create! :week_id => 6, :away_team_id => 167, :home_team_id => 169, :start_time => Time.parse('Saturday, October 6 1:00 PM'), :espn_game_id => 322800047
g = Game.create! :week_id => 6, :away_team_id => 159, :home_team_id => 161, :start_time => Time.parse('Saturday, October 6 1:00 PM'), :espn_game_id => 322800108
g = Game.create! :week_id => 6, :away_team_id => 157, :home_team_id => 143, :start_time => Time.parse('Saturday, October 6 1:00 PM'), :espn_game_id => 322800227
g = Game.create! :week_id => 6, :away_team_id => 213, :home_team_id => 212, :start_time => Time.parse('Saturday, October 6 1:00 PM'), :espn_game_id => 322802166
g = Game.create! :week_id => 6, :away_team_id => 65, :home_team_id => 64, :start_time => Time.parse('Saturday, October 6 1:00 PM'), :espn_game_id => 322802199
g = Game.create! :week_id => 6, :away_team_id => 206, :home_team_id => 205, :start_time => Time.parse('Saturday, October 6 1:00 PM'), :espn_game_id => 322802230
g = Game.create! :week_id => 6, :away_team_id => 215, :home_team_id => 217, :start_time => Time.parse('Saturday, October 6 1:00 PM'), :espn_game_id => 322802413
g = Game.create! :week_id => 6, :away_team_id => 193, :home_team_id => 191, :start_time => Time.parse('Saturday, October 6 1:00 PM'), :espn_game_id => 322802529
g = Game.create! :week_id => 6, :away_team_id => 185, :home_team_id => 186, :start_time => Time.parse('Saturday, October 6 1:00 PM'), :espn_game_id => 322802803
g = Game.create! :week_id => 6, :away_team_id => 134, :home_team_id => 136, :start_time => Time.parse('Saturday, October 6 1:30 PM'), :espn_game_id => 322802678
g = Game.create! :week_id => 6, :away_team_id => 61, :home_team_id => 59, :start_time => Time.parse('Saturday, October 6 2:00 PM'), :espn_game_id => 322802006
g = Game.create! :week_id => 6, :away_team_id => 246, :home_team_id => 238, :start_time => Time.parse('Saturday, October 6 2:00 PM'), :espn_game_id => 322802011
g = Game.create! :week_id => 6, :away_team_id => 218, :home_team_id => 214, :start_time => Time.parse('Saturday, October 6 2:00 PM'), :espn_game_id => 322802181
g = Game.create! :week_id => 6, :away_team_id => 182, :home_team_id => 176, :start_time => Time.parse('Saturday, October 6 2:00 PM'), :espn_game_id => 322802287
g = Game.create! :week_id => 6, :away_team_id => 184, :home_team_id => 179, :start_time => Time.parse('Saturday, October 6 2:00 PM'), :espn_game_id => 322802449
g = Game.create! :week_id => 6, :away_team_id => 210, :home_team_id => 219, :start_time => Time.parse('Saturday, October 6 2:00 PM'), :espn_game_id => 322802674
g = Game.create! :week_id => 6, :away_team_id => 140, :home_team_id => 71, :start_time => Time.parse('Saturday, October 6 2:00 PM'), :espn_game_id => 322802711
g = Game.create! :week_id => 6, :away_team_id => 175, :home_team_id => 173, :start_time => Time.parse('Saturday, October 6 2:30 PM'), :espn_game_id => 322802428
g = Game.create! :week_id => 6, :away_team_id => 183, :home_team_id => 150, :start_time => Time.parse('Saturday, October 6 3:00 PM'), :espn_game_id => 322800233
g = Game.create! :week_id => 6, :away_team_id => 245, :home_team_id => 239, :start_time => Time.parse('Saturday, October 6 3:00 PM'), :espn_game_id => 322802016
g = Game.create! :week_id => 6, :away_team_id => 237, :home_team_id => 243, :start_time => Time.parse('Saturday, October 6 3:00 PM'), :espn_game_id => 322802400
g = Game.create! :week_id => 6, :away_team_id => 222, :home_team_id => 226, :start_time => Time.parse('Saturday, October 6 3:00 PM'), :espn_game_id => 322802535
g = Game.create! :week_id => 6, :away_team_id => 195, :home_team_id => 202, :start_time => Time.parse('Saturday, October 6 3:00 PM'), :espn_game_id => 322802630
g = Game.create! :week_id => 6, :away_team_id => 63, :home_team_id => 70, :start_time => Time.parse('Saturday, October 6 3:00 PM'), :espn_game_id => 322802649
g = Game.create! :week_id => 6, :away_team_id => 178, :home_team_id => 177, :start_time => Time.parse('Saturday, October 6 3:05 PM'), :espn_game_id => 322800282
g = Game.create! :week_id => 6, :away_team_id => 139, :home_team_id => 137, :start_time => Time.parse('Saturday, October 6 3:30 PM'), :espn_game_id => 322800048
g = Game.create! :week_id => 6, :away_team_id => 31, :home_team_id => 42, :start_time => Time.parse('Saturday, October 6 3:30 PM'), :espn_game_id => 322800275
g = Game.create! :week_id => 6, :away_team_id => 132, :home_team_id => 133, :start_time => Time.parse('Saturday, October 6 3:30 PM'), :espn_game_id => 322802335
g = Game.create! :week_id => 6, :away_team_id => 225, :home_team_id => 227, :start_time => Time.parse('Saturday, October 6 3:30 PM'), :espn_game_id => 322802717
g = Game.create! :week_id => 6, :away_team_id => 123, :home_team_id => 126, :start_time => Time.parse('Saturday, October 6 3:35 PM'), :espn_game_id => 322802458
g = Game.create! :week_id => 6, :away_team_id => 172, :home_team_id => 165, :start_time => Time.parse('Saturday, October 6 4:00 PM'), :espn_game_id => 322802065
g = Game.create! :week_id => 6, :away_team_id => 166, :home_team_id => 171, :start_time => Time.parse('Saturday, October 6 4:00 PM'), :espn_game_id => 322802450
g = Game.create! :week_id => 6, :away_team_id => 236, :home_team_id => 234, :start_time => Time.parse('Saturday, October 6 4:00 PM'), :espn_game_id => 322802534
g = Game.create! :week_id => 6, :away_team_id => 118, :home_team_id => 115, :start_time => Time.parse('Saturday, October 6 5:00 PM'), :espn_game_id => 322800070
g = Game.create! :week_id => 6, :away_team_id => 163, :home_team_id => 208, :start_time => Time.parse('Saturday, October 6 6:00 PM'), :espn_game_id => 322800322
g = Game.create! :week_id => 6, :away_team_id => 124, :home_team_id => 152, :start_time => Time.parse('Saturday, October 6 7:00 PM'), :espn_game_id => 322800302
g = Game.create! :week_id => 6, :away_team_id => 242, :home_team_id => 240, :start_time => Time.parse('Saturday, October 6 7:00 PM'), :espn_game_id => 322802029
g = Game.create! :week_id => 6, :away_team_id => 198, :home_team_id => 194, :start_time => Time.parse('Saturday, October 6 7:00 PM'), :espn_game_id => 322802046
g = Game.create! :week_id => 6, :away_team_id => 232, :home_team_id => 229, :start_time => Time.parse('Saturday, October 6 7:00 PM'), :espn_game_id => 322802110
g = Game.create! :week_id => 6, :away_team_id => 66, :home_team_id => 23, :start_time => Time.parse('Saturday, October 6 7:00 PM'), :espn_game_id => 322802132
g = Game.create! :week_id => 6, :away_team_id => 78, :home_team_id => 116, :start_time => Time.parse('Saturday, October 6 7:00 PM'), :espn_game_id => 322802348
g = Game.create! :week_id => 6, :away_team_id => 230, :home_team_id => 233, :start_time => Time.parse('Saturday, October 6 7:00 PM'), :espn_game_id => 322802466
g = Game.create! :week_id => 6, :away_team_id => 241, :home_team_id => 244, :start_time => Time.parse('Saturday, October 6 7:00 PM'), :espn_game_id => 322802504
g = Game.create! :week_id => 6, :away_team_id => 170, :home_team_id => 174, :start_time => Time.parse('Saturday, October 6 7:00 PM'), :espn_game_id => 322802542
g = Game.create! :week_id => 6, :away_team_id => 149, :home_team_id => 121, :start_time => Time.parse('Saturday, October 6 7:05 PM'), :espn_game_id => 322800331
g = Game.create! :week_id => 6, :away_team_id => 7, :home_team_id => 58, :start_time => Time.parse('Saturday, October 6 7:30 PM'), :espn_game_id => 322800087
g = Game.create! :week_id => 6, :away_team_id => 197, :home_team_id => 201, :start_time => Time.parse('Saturday, October 6 8:00 PM'), :espn_game_id => 322802635
g = Game.create! :week_id => 8, :away_team_id => 168, :home_team_id => 173, :start_time => Time.parse('Thursday, October 18 7:30 PM'), :espn_game_id => 322922428
g = Game.create! :week_id => 8, :away_team_id => 84, :home_team_id => 81, :start_time => Time.parse('Thursday, October 18 9:00 PM'), :espn_game_id => 322920009
g = Game.create! :week_id => 8, :away_team_id => 24, :home_team_id => 29, :start_time => Time.parse('Friday, October 19 8:00 PM'), :espn_game_id => 322930183
g = Game.create! :week_id => 8, :away_team_id => 43, :home_team_id => 52, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940005
g = Game.create! :week_id => 8, :away_team_id => 105, :home_team_id => 154, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940006
g = Game.create! :week_id => 8, :away_team_id => 90, :home_team_id => 80, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940012
g = Game.create! :week_id => 8, :away_team_id => 86, :home_team_id => 82, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940025
g = Game.create! :week_id => 8, :away_team_id => 83, :home_team_id => 88, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940030
g = Game.create! :week_id => 8, :away_team_id => 162, :home_team_id => 164, :start_time => Time.parse('Saturday, October 20 12:00 PM'), :espn_game_id => 322940043
g = Game.create! :week_id => 8, :away_team_id => 175, :home_team_id => 167, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940050
g = Game.create! :week_id => 8, :away_team_id => 101, :home_team_id => 95, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940057
g = Game.create! :week_id => 8, :away_team_id => 1, :home_team_id => 5, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940059
g = Game.create! :week_id => 8, :away_team_id => 78, :home_team_id => 73, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940068
g = Game.create! :week_id => 8, :away_team_id => 37, :home_team_id => 38, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940077
g = Game.create! :week_id => 8, :away_team_id => 56, :home_team_id => 58, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940087
g = Game.create! :week_id => 8, :away_team_id => 96, :home_team_id => 97, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940096
g = Game.create! :week_id => 8, :away_team_id => 28, :home_team_id => 25, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940097
g = Game.create! :week_id => 8, :away_team_id => 108, :home_team_id => 112, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940098
g = Game.create! :week_id => 8, :away_team_id => 61, :home_team_id => 140, :start_time => Time.parse('Saturday, October 20 12:00 PM'), :espn_game_id => 322940113
g = Game.create! :week_id => 8, :away_team_id => 9, :home_team_id => 6, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940120
g = Game.create! :week_id => 8, :away_team_id => 35, :home_team_id => 34, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940130
g = Game.create! :week_id => 8, :away_team_id => 8, :home_team_id => 3, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940150
g = Game.create! :week_id => 8, :away_team_id => 161, :home_team_id => 163, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940163
g = Game.create! :week_id => 8, :away_team_id => 160, :home_team_id => 158, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940171
g = Game.create! :week_id => 8, :away_team_id => 41, :home_team_id => 39, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940194
g = Game.create! :week_id => 8, :away_team_id => 14, :home_team_id => 19, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940197
g = Game.create! :week_id => 8, :away_team_id => 15, :home_team_id => 18, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940201
g = Game.create! :week_id => 8, :away_team_id => 47, :home_team_id => 51, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940202
g = Game.create! :week_id => 8, :away_team_id => 89, :home_team_id => 85, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940204
g = Game.create! :week_id => 8, :away_team_id => 27, :home_team_id => 69, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940218
g = Game.create! :week_id => 8, :away_team_id => 159, :home_team_id => 157, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940225
g = Game.create! :week_id => 8, :away_team_id => 11, :home_team_id => 2, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940228
g = Game.create! :week_id => 8, :away_team_id => 53, :home_team_id => 46, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940235
g = Game.create! :week_id => 8, :away_team_id => 94, :home_team_id => 103, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940238
g = Game.create! :week_id => 8, :away_team_id => 98, :home_team_id => 21, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940245
g = Game.create! :week_id => 8, :away_team_id => 13, :home_team_id => 20, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940251
g = Game.create! :week_id => 8, :away_team_id => 138, :home_team_id => 144, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940257
g = Game.create! :week_id => 8, :away_team_id => 12, :home_team_id => 10, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940258
g = Game.create! :week_id => 8, :away_team_id => 36, :home_team_id => 42, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940275
g = Game.create! :week_id => 8, :away_team_id => 16, :home_team_id => 30, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940277
g = Game.create! :week_id => 8, :away_team_id => 79, :home_team_id => 113, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940278
g = Game.create! :week_id => 8, :away_team_id => 141, :home_team_id => 139, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940311
g = Game.create! :week_id => 8, :away_team_id => 128, :home_team_id => 121, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940331
g = Game.create! :week_id => 8, :away_team_id => 109, :home_team_id => 99, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322940344
g = Game.create! :week_id => 8, :away_team_id => 75, :home_team_id => 72, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942005
g = Game.create! :week_id => 8, :away_team_id => 67, :home_team_id => 59, :start_time => Time.parse('Saturday, October 20 12:00 PM'), :espn_game_id => 322942006
g = Game.create! :week_id => 8, :away_team_id => 228, :home_team_id => 220, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942026
g = Game.create! :week_id => 8, :away_team_id => 206, :home_team_id => 204, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942142
g = Game.create! :week_id => 8, :away_team_id => 191, :home_team_id => 188, :start_time => Time.parse('Saturday, October 20 12:00 PM'), :espn_game_id => 322942184
g = Game.create! :week_id => 8, :away_team_id => 146, :home_team_id => 153, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942247
g = Game.create! :week_id => 8, :away_team_id => 4, :home_team_id => 7, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942390
g = Game.create! :week_id => 8, :away_team_id => 32, :home_team_id => 57, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942426
g = Game.create! :week_id => 8, :away_team_id => 76, :home_team_id => 117, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942440
g = Game.create! :week_id => 8, :away_team_id => 187, :home_team_id => 190, :start_time => Time.parse('Saturday, October 20 12:00 PM'), :espn_game_id => 322942523
g = Game.create! :week_id => 8, :away_team_id => 44, :home_team_id => 48, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942567
g = Game.create! :week_id => 8, :away_team_id => 45, :home_team_id => 49, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942572
g = Game.create! :week_id => 8, :away_team_id => 193, :home_team_id => 192, :start_time => Time.parse('Saturday, October 20 12:00 PM'), :espn_game_id => 322942598
g = Game.create! :week_id => 8, :away_team_id => 132, :home_team_id => 135, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942619
g = Game.create! :week_id => 8, :away_team_id => 22, :home_team_id => 77, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942628
g = Game.create! :week_id => 8, :away_team_id => 92, :home_team_id => 102, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942633
g = Game.create! :week_id => 8, :away_team_id => 50, :home_team_id => 54, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942638
g = Game.create! :week_id => 8, :away_team_id => 23, :home_team_id => 70, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942649
g = Game.create! :week_id => 8, :away_team_id => 106, :home_team_id => 111, :start_time => Time.parse('Saturday, October 20 10:59:59 PM'), :espn_game_id => 322942653
g = Game.create! :week_id => 8, :away_team_id => 203, :home_team_id => 209, :start_time => Time.parse('Saturday, October 20 12:30 PM'), :espn_game_id => 322942329
g = Game.create! :week_id => 8, :away_team_id => 170, :home_team_id => 169, :start_time => Time.parse('Saturday, October 20 1:00 PM'), :espn_game_id => 322940047
g = Game.create! :week_id => 8, :away_team_id => 218, :home_team_id => 215, :start_time => Time.parse('Saturday, October 20 1:00 PM'), :espn_game_id => 322940294
g = Game.create! :week_id => 8, :away_team_id => 207, :home_team_id => 208, :start_time => Time.parse('Saturday, October 20 1:00 PM'), :espn_game_id => 322940322
g = Game.create! :week_id => 8, :away_team_id => 219, :home_team_id => 213, :start_time => Time.parse('Saturday, October 20 1:00 PM'), :espn_game_id => 322942168
g = Game.create! :week_id => 8, :away_team_id => 55, :home_team_id => 64, :start_time => Time.parse('Saturday, October 20 1:00 PM'), :espn_game_id => 322942199
g = Game.create! :week_id => 8, :away_team_id => 186, :home_team_id => 189, :start_time => Time.parse('Saturday, October 20 1:00 PM'), :espn_game_id => 322942405
g = Game.create! :week_id => 8, :away_team_id => 225, :home_team_id => 224, :start_time => Time.parse('Saturday, October 20 1:30 PM'), :espn_game_id => 322940231
g = Game.create! :week_id => 8, :away_team_id => 134, :home_team_id => 130, :start_time => Time.parse('Saturday, October 20 1:30 PM'), :espn_game_id => 322942127
g = Game.create! :week_id => 8, :away_team_id => 172, :home_team_id => 166, :start_time => Time.parse('Saturday, October 20 1:30 PM'), :espn_game_id => 322942169
g = Game.create! :week_id => 8, :away_team_id => 131, :home_team_id => 136, :start_time => Time.parse('Saturday, October 20 1:30 PM'), :espn_game_id => 322942678
g = Game.create! :week_id => 8, :away_team_id => 216, :home_team_id => 214, :start_time => Time.parse('Saturday, October 20 2:00 PM'), :espn_game_id => 322942181
g = Game.create! :week_id => 8, :away_team_id => 178, :home_team_id => 176, :start_time => Time.parse('Saturday, October 20 2:00 PM'), :espn_game_id => 322942287
g = Game.create! :week_id => 8, :away_team_id => 332, :home_team_id => 174, :start_time => Time.parse('Saturday, October 20 2:00 PM'), :espn_game_id => 322942542
g = Game.create! :week_id => 8, :away_team_id => 202, :home_team_id => 199, :start_time => Time.parse('Saturday, October 20 2:00 PM'), :espn_game_id => 322942546
g = Game.create! :week_id => 8, :away_team_id => 119, :home_team_id => 156, :start_time => Time.parse('Saturday, October 20 2:00 PM'), :espn_game_id => 322942636
g = Game.create! :week_id => 8, :away_team_id => 129, :home_team_id => 151, :start_time => Time.parse('Saturday, October 20 3:00 PM'), :espn_game_id => 322940253
g = Game.create! :week_id => 8, :away_team_id => 118, :home_team_id => 120, :start_time => Time.parse('Saturday, October 20 3:00 PM'), :espn_game_id => 322940328
g = Game.create! :week_id => 8, :away_team_id => 227, :home_team_id => 223, :start_time => Time.parse('Saturday, October 20 3:00 PM'), :espn_game_id => 322942210
g = Game.create! :week_id => 8, :away_team_id => 239, :home_team_id => 244, :start_time => Time.parse('Saturday, October 20 3:00 PM'), :espn_game_id => 322942504
g = Game.create! :week_id => 8, :away_team_id => 231, :home_team_id => 234, :start_time => Time.parse('Saturday, October 20 3:00 PM'), :espn_game_id => 322942534
g = Game.create! :week_id => 8, :away_team_id => 232, :home_team_id => 236, :start_time => Time.parse('Saturday, October 20 3:00 PM'), :espn_game_id => 322942617
g = Game.create! :week_id => 8, :away_team_id => 262, :home_team_id => 241, :start_time => Time.parse('Saturday, October 20 3:00 PM'), :espn_game_id => 322942755
g = Game.create! :week_id => 8, :away_team_id => 143, :home_team_id => 137, :start_time => Time.parse('Saturday, October 20 3:30 PM'), :espn_game_id => 322940048
g = Game.create! :week_id => 8, :away_team_id => 123, :home_team_id => 149, :start_time => Time.parse('Saturday, October 20 3:30 PM'), :espn_game_id => 322940155
g = Game.create! :week_id => 8, :away_team_id => 26, :home_team_id => 62, :start_time => Time.parse('Saturday, October 20 3:30 PM'), :espn_game_id => 322942084
g = Game.create! :week_id => 8, :away_team_id => 60, :home_team_id => 63, :start_time => Time.parse('Saturday, October 20 3:30 PM'), :espn_game_id => 322942117
g = Game.create! :week_id => 8, :away_team_id => 71, :home_team_id => 65, :start_time => Time.parse('Saturday, October 20 3:30 PM'), :espn_game_id => 322942309
g = Game.create! :week_id => 8, :away_team_id => 287, :home_team_id => 133, :start_time => Time.parse('Saturday, October 20 3:30 PM'), :espn_game_id => 322942335
g = Game.create! :week_id => 8, :away_team_id => 122, :home_team_id => 126, :start_time => Time.parse('Saturday, October 20 3:35 PM'), :espn_game_id => 322942458
g = Game.create! :week_id => 8, :away_team_id => 200, :home_team_id => 197, :start_time => Time.parse('Saturday, October 20 4:00 PM'), :espn_game_id => 322940055
g = Game.create! :week_id => 8, :away_team_id => 171, :home_team_id => 165, :start_time => Time.parse('Saturday, October 20 4:00 PM'), :espn_game_id => 322942065
g = Game.create! :week_id => 8, :away_team_id => 243, :home_team_id => 242, :start_time => Time.parse('Saturday, October 20 4:00 PM'), :espn_game_id => 322942296
g = Game.create! :week_id => 8, :away_team_id => 182, :home_team_id => 184, :start_time => Time.parse('Saturday, October 20 4:00 PM'), :espn_game_id => 322942754
g = Game.create! :week_id => 8, :away_team_id => 181, :home_team_id => 180, :start_time => Time.parse('Saturday, October 20 5:00 PM'), :espn_game_id => 322942460
g = Game.create! :week_id => 8, :away_team_id => 226, :home_team_id => 221, :start_time => Time.parse('Saturday, October 20 6:00 PM'), :espn_game_id => 322940236
g = Game.create! :week_id => 8, :away_team_id => 217, :home_team_id => 210, :start_time => Time.parse('Saturday, October 20 6:00 PM'), :espn_game_id => 322942086
g = Game.create! :week_id => 8, :away_team_id => 212, :home_team_id => 211, :start_time => Time.parse('Saturday, October 20 6:00 PM'), :espn_game_id => 322942097
g = Game.create! :week_id => 8, :away_team_id => 142, :home_team_id => 145, :start_time => Time.parse('Saturday, October 20 7:00 PM'), :espn_game_id => 322940119
g = Game.create! :week_id => 8, :away_team_id => 179, :home_team_id => 150, :start_time => Time.parse('Saturday, October 20 7:00 PM'), :espn_game_id => 322940233
g = Game.create! :week_id => 8, :away_team_id => 230, :home_team_id => 229, :start_time => Time.parse('Saturday, October 20 7:00 PM'), :espn_game_id => 322942110
g = Game.create! :week_id => 8, :away_team_id => 115, :home_team_id => 116, :start_time => Time.parse('Saturday, October 20 7:00 PM'), :espn_game_id => 322942348
g = Game.create! :week_id => 8, :away_team_id => 240, :home_team_id => 245, :start_time => Time.parse('Saturday, October 20 7:00 PM'), :espn_game_id => 322942582
g = Game.create! :week_id => 8, :away_team_id => 177, :home_team_id => 183, :start_time => Time.parse('Saturday, October 20 7:00 PM'), :espn_game_id => 322942710
g = Game.create! :week_id => 8, :away_team_id => 152, :home_team_id => 125, :start_time => Time.parse('Saturday, October 20 7:05 PM'), :espn_game_id => 322942464
g = Game.create! :week_id => 8, :away_team_id => 40, :home_team_id => 33, :start_time => Time.parse('Saturday, October 20 8:00 PM'), :espn_game_id => 322942294
g = Game.create! :week_id => 8, :away_team_id => 196, :home_team_id => 201, :start_time => Time.parse('Saturday, October 20 8:00 PM'), :espn_game_id => 322942635
g = Game.create! :week_id => 8, :away_team_id => 127, :home_team_id => 148, :start_time => Time.parse('Saturday, October 20 9:05 PM'), :espn_game_id => 322940013
g = Game.create! :week_id => 8, :away_team_id => 104, :home_team_id => 107, :start_time => Time.parse('Tuesday, October 23 8:00 PM'), :espn_game_id => 322970309
g = Game.create! :week_id => 9, :away_team_id => 2, :home_team_id => 12, :start_time => Time.parse('Thursday, October 25 7:30 PM'), :espn_game_id => 322990154
g = Game.create! :week_id => 9, :away_team_id => 166, :home_team_id => 170, :start_time => Time.parse('Thursday, October 25 7:30 PM'), :espn_game_id => 322992415
g = Game.create! :week_id => 9, :away_team_id => 23, :home_team_id => 25, :start_time => Time.parse('Friday, October 26 8:00 PM'), :espn_game_id => 323000097
g = Game.create! :week_id => 9, :away_team_id => 117, :home_team_id => 72, :start_time => Time.parse('Friday, October 26 8:00 PM'), :espn_game_id => 323002005
g = Game.create! :week_id => 9, :away_team_id => 21, :home_team_id => 94, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010002
g = Game.create! :week_id => 9, :away_team_id => 87, :home_team_id => 81, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010009
g = Game.create! :week_id => 9, :away_team_id => 88, :home_team_id => 80, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010012
g = Game.create! :week_id => 9, :away_team_id => 78, :home_team_id => 76, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010021
g = Game.create! :week_id => 9, :away_team_id => 91, :home_team_id => 86, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010024
g = Game.create! :week_id => 9, :away_team_id => 114, :home_team_id => 74, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010036
g = Game.create! :week_id => 9, :away_team_id => 3, :home_team_id => 4, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010052
g = Game.create! :week_id => 9, :away_team_id => 29, :home_team_id => 28, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010058
g = Game.create! :week_id => 9, :away_team_id => 56, :home_team_id => 5, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010059
g = Game.create! :week_id => 9, :away_team_id => 13, :home_team_id => 14, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010066
g = Game.create! :week_id => 9, :away_team_id => 33, :home_team_id => 38, :start_time => Time.parse('Saturday, October 27 12:00 PM'), :espn_game_id => 323010077
g = Game.create! :week_id => 9, :away_team_id => 6, :home_team_id => 1, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010103
g = Game.create! :week_id => 9, :away_team_id => 205, :home_team_id => 207, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010107
g = Game.create! :week_id => 9, :away_team_id => 41, :home_team_id => 36, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010135
g = Game.create! :week_id => 9, :away_team_id => 97, :home_team_id => 17, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010142
g = Game.create! :week_id => 9, :away_team_id => 149, :home_team_id => 124, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010147
g = Game.create! :week_id => 9, :away_team_id => 57, :home_team_id => 43, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010151
g = Game.create! :week_id => 9, :away_team_id => 9, :home_team_id => 8, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010153
g = Game.create! :week_id => 9, :away_team_id => 34, :home_team_id => 37, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010158
g = Game.create! :week_id => 9, :away_team_id => 113, :home_team_id => 75, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010167
g = Game.create! :week_id => 9, :away_team_id => 164, :home_team_id => 158, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010171
g = Game.create! :week_id => 9, :away_team_id => 77, :home_team_id => 19, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010197
g = Game.create! :week_id => 9, :away_team_id => 58, :home_team_id => 18, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010201
g = Game.create! :week_id => 9, :away_team_id => 39, :home_team_id => 40, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010213
g = Game.create! :week_id => 9, :away_team_id => 157, :home_team_id => 162, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010219
g = Game.create! :week_id => 9, :away_team_id => 69, :home_team_id => 26, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010221
g = Game.create! :week_id => 9, :away_team_id => 145, :home_team_id => 146, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010222
g = Game.create! :week_id => 9, :away_team_id => 141, :home_team_id => 143, :start_time => Time.parse('Saturday, October 27 12:00 PM'), :espn_game_id => 323010227
g = Game.create! :week_id => 9, :away_team_id => 140, :home_team_id => 103, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010238
g = Game.create! :week_id => 9, :away_team_id => 49, :home_team_id => 47, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010242
g = Game.create! :week_id => 9, :away_team_id => 54, :home_team_id => 44, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010248
g = Game.create! :week_id => 9, :away_team_id => 82, :home_team_id => 89, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010254
g = Game.create! :week_id => 9, :away_team_id => 153, :home_team_id => 138, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010256
g = Game.create! :week_id => 9, :away_team_id => 85, :home_team_id => 90, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010264
g = Game.create! :week_id => 9, :away_team_id => 53, :home_team_id => 45, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010276
g = Game.create! :week_id => 9, :away_team_id => 137, :home_team_id => 142, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010295
g = Game.create! :week_id => 9, :away_team_id => 99, :home_team_id => 92, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010333
g = Game.create! :week_id => 9, :away_team_id => 60, :home_team_id => 55, :start_time => Time.parse('Saturday, October 27 12:00 PM'), :espn_game_id => 323010349
g = Game.create! :week_id => 9, :away_team_id => 32, :home_team_id => 31, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323010356
g = Game.create! :week_id => 9, :away_team_id => 192, :home_team_id => 187, :start_time => Time.parse('Saturday, October 27 12:00 PM'), :espn_game_id => 323012115
g = Game.create! :week_id => 9, :away_team_id => 210, :home_team_id => 212, :start_time => Time.parse('Saturday, October 27 12:00 PM'), :espn_game_id => 323012166
g = Game.create! :week_id => 9, :away_team_id => 189, :home_team_id => 188, :start_time => Time.parse('Saturday, October 27 12:00 PM'), :espn_game_id => 323012184
g = Game.create! :week_id => 9, :away_team_id => 195, :home_team_id => 196, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012198
g = Game.create! :week_id => 9, :away_team_id => 111, :home_team_id => 105, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012226
g = Game.create! :week_id => 9, :away_team_id => 112, :home_team_id => 106, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012229
g = Game.create! :week_id => 9, :away_team_id => 20, :home_team_id => 15, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012305
g = Game.create! :week_id => 9, :away_team_id => 22, :home_team_id => 16, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012306
g = Game.create! :week_id => 9, :away_team_id => 234, :home_team_id => 230, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012320
g = Game.create! :week_id => 9, :away_team_id => 110, :home_team_id => 109, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012393
g = Game.create! :week_id => 9, :away_team_id => 154, :home_team_id => 108, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012433
g = Game.create! :week_id => 9, :away_team_id => 83, :home_team_id => 84, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012483
g = Game.create! :week_id => 9, :away_team_id => 229, :home_team_id => 235, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012545
g = Game.create! :week_id => 9, :away_team_id => 46, :home_team_id => 48, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012567
g = Game.create! :week_id => 9, :away_team_id => 169, :home_team_id => 175, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012569
g = Game.create! :week_id => 9, :away_team_id => 102, :home_team_id => 101, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012579
g = Game.create! :week_id => 9, :away_team_id => 52, :home_team_id => 50, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012655
g = Game.create! :week_id => 9, :away_team_id => 190, :home_team_id => 193, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012681
g = Game.create! :week_id => 9, :away_team_id => 67, :home_team_id => 71, :start_time => Time.parse('Saturday, October 27 12:00 PM'), :espn_game_id => 323012711
g = Game.create! :week_id => 9, :away_team_id => 139, :home_team_id => 147, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012729
g = Game.create! :week_id => 9, :away_team_id => 222, :home_team_id => 228, :start_time => Time.parse('Saturday, October 27 10:59:59 PM'), :espn_game_id => 323012747
g = Game.create! :week_id => 9, :away_team_id => 163, :home_team_id => 159, :start_time => Time.parse('Saturday, October 27 12:30 PM'), :espn_game_id => 323010172
g = Game.create! :week_id => 9, :away_team_id => 204, :home_team_id => 203, :start_time => Time.parse('Saturday, October 27 1:00 PM'), :espn_game_id => 323012083
g = Game.create! :week_id => 9, :away_team_id => 211, :home_team_id => 217, :start_time => Time.parse('Saturday, October 27 1:00 PM'), :espn_game_id => 323012413
g = Game.create! :week_id => 9, :away_team_id => 135, :home_team_id => 134, :start_time => Time.parse('Saturday, October 27 1:00 PM'), :espn_game_id => 323012506
g = Game.create! :week_id => 9, :away_team_id => 185, :home_team_id => 191, :start_time => Time.parse('Saturday, October 27 1:00 PM'), :espn_game_id => 323012529
g = Game.create! :week_id => 9, :away_team_id => 332, :home_team_id => 130, :start_time => Time.parse('Saturday, October 27 1:30 PM'), :espn_game_id => 323012127
g = Game.create! :week_id => 9, :away_team_id => 136, :home_team_id => 132, :start_time => Time.parse('Saturday, October 27 1:30 PM'), :espn_game_id => 323012241
g = Game.create! :week_id => 9, :away_team_id => 171, :home_team_id => 172, :start_time => Time.parse('Saturday, October 27 1:30 PM'), :espn_game_id => 323012448
g = Game.create! :week_id => 9, :away_team_id => 174, :home_team_id => 168, :start_time => Time.parse('Saturday, October 27 2:00 PM'), :espn_game_id => 323012261
g = Game.create! :week_id => 9, :away_team_id => 183, :home_team_id => 178, :start_time => Time.parse('Saturday, October 27 2:00 PM'), :espn_game_id => 323012623
g = Game.create! :week_id => 9, :away_team_id => 201, :home_team_id => 200, :start_time => Time.parse('Saturday, October 27 2:00 PM'), :espn_game_id => 323012634
g = Game.create! :week_id => 9, :away_team_id => 120, :home_team_id => 156, :start_time => Time.parse('Saturday, October 27 2:00 PM'), :espn_game_id => 323012636
g = Game.create! :week_id => 9, :away_team_id => 216, :home_team_id => 219, :start_time => Time.parse('Saturday, October 27 2:00 PM'), :espn_game_id => 323012674
g = Game.create! :week_id => 9, :away_team_id => 150, :home_team_id => 177, :start_time => Time.parse('Saturday, October 27 2:05 PM'), :espn_game_id => 323010282
g = Game.create! :week_id => 9, :away_team_id => 121, :home_team_id => 151, :start_time => Time.parse('Saturday, October 27 3:00 PM'), :espn_game_id => 323010253
g = Game.create! :week_id => 9, :away_team_id => 133, :home_team_id => 131, :start_time => Time.parse('Saturday, October 27 3:00 PM'), :espn_game_id => 323010324
g = Game.create! :week_id => 9, :away_team_id => 224, :home_team_id => 223, :start_time => Time.parse('Saturday, October 27 3:00 PM'), :espn_game_id => 323012210
g = Game.create! :week_id => 9, :away_team_id => 184, :home_team_id => 181, :start_time => Time.parse('Saturday, October 27 3:00 PM'), :espn_game_id => 323012571
g = Game.create! :week_id => 9, :away_team_id => 100, :home_team_id => 93, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323010008
g = Game.create! :week_id => 9, :away_team_id => 95, :home_team_id => 96, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323010061
g = Game.create! :week_id => 9, :away_team_id => 122, :home_team_id => 123, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323010149
g = Game.create! :week_id => 9, :away_team_id => 65, :home_team_id => 27, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323010164
g = Game.create! :week_id => 9, :away_team_id => 64, :home_team_id => 61, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323010189
g = Game.create! :week_id => 9, :away_team_id => 68, :home_team_id => 66, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323010193
g = Game.create! :week_id => 9, :away_team_id => 35, :home_team_id => 42, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323010275
g = Game.create! :week_id => 9, :away_team_id => 238, :home_team_id => 237, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323012010
g = Game.create! :week_id => 9, :away_team_id => 243, :home_team_id => 240, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323012029
g = Game.create! :week_id => 9, :away_team_id => 70, :home_team_id => 62, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323012084
g = Game.create! :week_id => 9, :away_team_id => 59, :home_team_id => 63, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323012117
g = Game.create! :week_id => 9, :away_team_id => 220, :home_team_id => 227, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323012717
g = Game.create! :week_id => 9, :away_team_id => 73, :home_team_id => 79, :start_time => Time.parse('Saturday, October 27 3:30 PM'), :espn_game_id => 323012751
g = Game.create! :week_id => 9, :away_team_id => 125, :home_team_id => 126, :start_time => Time.parse('Saturday, October 27 3:35 PM'), :espn_game_id => 323012458
g = Game.create! :week_id => 9, :away_team_id => 155, :home_team_id => 119, :start_time => Time.parse('Saturday, October 27 4:00 PM'), :espn_game_id => 323010023
g = Game.create! :week_id => 9, :away_team_id => 198, :home_team_id => 197, :start_time => Time.parse('Saturday, October 27 4:00 PM'), :espn_game_id => 323010055
g = Game.create! :week_id => 9, :away_team_id => 173, :home_team_id => 165, :start_time => Time.parse('Saturday, October 27 4:00 PM'), :espn_game_id => 323012065
g = Game.create! :week_id => 9, :away_team_id => 182, :home_team_id => 179, :start_time => Time.parse('Saturday, October 27 4:00 PM'), :espn_game_id => 323012449
g = Game.create! :week_id => 9, :away_team_id => 161, :home_team_id => 160, :start_time => Time.parse('Saturday, October 27 5:00 PM'), :espn_game_id => 323010159
g = Game.create! :week_id => 9, :away_team_id => 127, :home_team_id => 152, :start_time => Time.parse('Saturday, October 27 5:00 PM'), :espn_game_id => 323010302
g = Game.create! :week_id => 9, :away_team_id => 199, :home_team_id => 194, :start_time => Time.parse('Saturday, October 27 5:00 PM'), :espn_game_id => 323012046
g = Game.create! :week_id => 9, :away_team_id => 176, :home_team_id => 180, :start_time => Time.parse('Saturday, October 27 5:00 PM'), :espn_game_id => 323012460
g = Game.create! :week_id => 9, :away_team_id => 245, :home_team_id => 244, :start_time => Time.parse('Saturday, October 27 5:00 PM'), :espn_game_id => 323012504
g = Game.create! :week_id => 9, :away_team_id => 225, :home_team_id => 221, :start_time => Time.parse('Saturday, October 27 6:00 PM'), :espn_game_id => 323010236
g = Game.create! :week_id => 9, :away_team_id => 206, :home_team_id => 208, :start_time => Time.parse('Saturday, October 27 6:00 PM'), :espn_game_id => 323010322
g = Game.create! :week_id => 9, :away_team_id => 232, :home_team_id => 233, :start_time => Time.parse('Saturday, October 27 7:00 PM'), :espn_game_id => 323012466
g = Game.create! :week_id => 9, :away_team_id => 241, :home_team_id => 246, :start_time => Time.parse('Saturday, October 27 7:00 PM'), :espn_game_id => 323012640
g = Game.create! :week_id => 9, :away_team_id => 116, :home_team_id => 118, :start_time => Time.parse('Saturday, October 27 8:00 PM'), :espn_game_id => 323010166
g = Game.create! :week_id => 9, :away_team_id => 236, :home_team_id => 231, :start_time => Time.parse('Saturday, October 27 8:00 PM'), :espn_game_id => 323012377
g = Game.create! :week_id => 9, :away_team_id => 213, :home_team_id => 218, :start_time => Time.parse('Saturday, October 27 9:00 PM'), :espn_game_id => 323010301
g = Game.create! :week_id => 9, :away_team_id => 148, :home_team_id => 128, :start_time => Time.parse('Saturday, October 27 9:05 PM'), :espn_game_id => 323010016
g = Game.create! :week_id => 10, :away_team_id => 64, :home_team_id => 68, :start_time => Time.parse('Thursday, November 1 6:00 PM'), :espn_game_id => 323060195
g = Game.create! :week_id => 10, :away_team_id => 11, :home_team_id => 7, :start_time => Time.parse('Thursday, November 1 7:30 PM'), :espn_game_id => 323062390
g = Game.create! :week_id => 10, :away_team_id => 109, :home_team_id => 112, :start_time => Time.parse('Thursday, November 1 9:15 PM'), :espn_game_id => 323060098
g = Game.create! :week_id => 10, :away_team_id => 90, :home_team_id => 82, :start_time => Time.parse('Friday, November 2 9:00 PM'), :espn_game_id => 323070025
g = Game.create! :week_id => 10, :away_team_id => 118, :home_team_id => 94, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080002
g = Game.create! :week_id => 10, :away_team_id => 106, :home_team_id => 154, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080006
g = Game.create! :week_id => 10, :away_team_id => 51, :home_team_id => 93, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080008
g = Game.create! :week_id => 10, :away_team_id => 80, :home_team_id => 87, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080026
g = Game.create! :week_id => 10, :away_team_id => 84, :home_team_id => 88, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080030
g = Game.create! :week_id => 10, :away_team_id => 86, :home_team_id => 83, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080038
g = Game.create! :week_id => 10, :away_team_id => 17, :home_team_id => 95, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080057
g = Game.create! :week_id => 10, :away_team_id => 24, :home_team_id => 28, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080058
g = Game.create! :week_id => 10, :away_team_id => 100, :home_team_id => 96, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080061
g = Game.create! :week_id => 10, :away_team_id => 18, :home_team_id => 14, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080066
g = Game.create! :week_id => 10, :away_team_id => 76, :home_team_id => 73, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080068
g = Game.create! :week_id => 10, :away_team_id => 33, :home_team_id => 32, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080084
g = Game.create! :week_id => 10, :away_team_id => 103, :home_team_id => 97, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080096
g = Game.create! :week_id => 10, :away_team_id => 69, :home_team_id => 25, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080097
g = Game.create! :week_id => 10, :away_team_id => 209, :home_team_id => 207, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080107
g = Game.create! :week_id => 10, :away_team_id => 5, :home_team_id => 6, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080120
g = Game.create! :week_id => 10, :away_team_id => 37, :home_team_id => 35, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080127
g = Game.create! :week_id => 10, :away_team_id => 34, :home_team_id => 36, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080135
g = Game.create! :week_id => 10, :away_team_id => 2, :home_team_id => 3, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080150
g = Game.create! :week_id => 10, :away_team_id => 44, :home_team_id => 43, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080151
g = Game.create! :week_id => 10, :away_team_id => 10, :home_team_id => 9, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080152
g = Game.create! :week_id => 10, :away_team_id => 1, :home_team_id => 12, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080154
g = Game.create! :week_id => 10, :away_team_id => 147, :home_team_id => 141, :start_time => Time.parse('Saturday, November 3 12:00 PM'), :espn_game_id => 323080160
g = Game.create! :week_id => 10, :away_team_id => 162, :home_team_id => 163, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080163
g = Game.create! :week_id => 10, :away_team_id => 31, :home_team_id => 39, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080194
g = Game.create! :week_id => 10, :away_team_id => 81, :home_team_id => 85, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080204
g = Game.create! :week_id => 10, :away_team_id => 164, :home_team_id => 157, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080225
g = Game.create! :week_id => 10, :away_team_id => 15, :home_team_id => 13, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080239
g = Game.create! :week_id => 10, :away_team_id => 104, :home_team_id => 110, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080249
g = Game.create! :week_id => 10, :away_team_id => 91, :home_team_id => 89, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080254
g = Game.create! :week_id => 10, :away_team_id => 143, :home_team_id => 144, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080257
g = Game.create! :week_id => 10, :away_team_id => 46, :home_team_id => 45, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080276
g = Game.create! :week_id => 10, :away_team_id => 77, :home_team_id => 30, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080277
g = Game.create! :week_id => 10, :away_team_id => 114, :home_team_id => 113, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080278
g = Game.create! :week_id => 10, :away_team_id => 138, :home_team_id => 139, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080311
g = Game.create! :week_id => 10, :away_team_id => 155, :home_team_id => 120, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080328
g = Game.create! :week_id => 10, :away_team_id => 148, :home_team_id => 121, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080331
g = Game.create! :week_id => 10, :away_team_id => 21, :home_team_id => 99, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080344
g = Game.create! :week_id => 10, :away_team_id => 72, :home_team_id => 55, :start_time => Time.parse('Saturday, November 3 12:00 PM'), :espn_game_id => 323080349
g = Game.create! :week_id => 10, :away_team_id => 193, :home_team_id => 185, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323080399
g = Game.create! :week_id => 10, :away_team_id => 66, :home_team_id => 62, :start_time => Time.parse('Saturday, November 3 12:00 PM'), :espn_game_id => 323082084
g = Game.create! :week_id => 10, :away_team_id => 215, :home_team_id => 210, :start_time => Time.parse('Saturday, November 3 12:00 PM'), :espn_game_id => 323082086
g = Game.create! :week_id => 10, :away_team_id => 48, :home_team_id => 53, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082116
g = Game.create! :week_id => 10, :away_team_id => 29, :home_team_id => 23, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082132
g = Game.create! :week_id => 10, :away_team_id => 142, :home_team_id => 153, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082247
g = Game.create! :week_id => 10, :away_team_id => 19, :home_team_id => 16, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082306
g = Game.create! :week_id => 10, :away_team_id => 105, :home_team_id => 57, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082426
g = Game.create! :week_id => 10, :away_team_id => 107, :home_team_id => 108, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082433
g = Game.create! :week_id => 10, :away_team_id => 75, :home_team_id => 78, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082439
g = Game.create! :week_id => 10, :away_team_id => 40, :home_team_id => 41, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082509
g = Game.create! :week_id => 10, :away_team_id => 188, :home_team_id => 190, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082523
g = Game.create! :week_id => 10, :away_team_id => 235, :home_team_id => 234, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082534
g = Game.create! :week_id => 10, :away_team_id => 52, :home_team_id => 49, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082572
g = Game.create! :week_id => 10, :away_team_id => 136, :home_team_id => 135, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082619
g = Game.create! :week_id => 10, :away_team_id => 111, :home_team_id => 102, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082633
g = Game.create! :week_id => 10, :away_team_id => 20, :home_team_id => 22, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082641
g = Game.create! :week_id => 10, :away_team_id => 47, :home_team_id => 50, :start_time => Time.parse('Saturday, November 3 10:59:59 PM'), :espn_game_id => 323082655
g = Game.create! :week_id => 10, :away_team_id => 160, :home_team_id => 159, :start_time => Time.parse('Saturday, November 3 12:30 PM'), :espn_game_id => 323080172
g = Game.create! :week_id => 10, :away_team_id => 168, :home_team_id => 169, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323080047
g = Game.create! :week_id => 10, :away_team_id => 158, :home_team_id => 161, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323080108
g = Game.create! :week_id => 10, :away_team_id => 205, :home_team_id => 203, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323082083
g = Game.create! :week_id => 10, :away_team_id => 219, :home_team_id => 211, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323082097
g = Game.create! :week_id => 10, :away_team_id => 71, :home_team_id => 63, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323082117
g = Game.create! :week_id => 10, :away_team_id => 208, :home_team_id => 204, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323082142
g = Game.create! :week_id => 10, :away_team_id => 214, :home_team_id => 213, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323082168
g = Game.create! :week_id => 10, :away_team_id => 187, :home_team_id => 189, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323082405
g = Game.create! :week_id => 10, :away_team_id => 212, :home_team_id => 217, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323082413
g = Game.create! :week_id => 10, :away_team_id => 165, :home_team_id => 170, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323082415
g = Game.create! :week_id => 10, :away_team_id => 197, :home_team_id => 202, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323082630
g = Game.create! :week_id => 10, :away_team_id => 191, :home_team_id => 186, :start_time => Time.parse('Saturday, November 3 1:00 PM'), :espn_game_id => 323082803
g = Game.create! :week_id => 10, :away_team_id => 131, :home_team_id => 132, :start_time => Time.parse('Saturday, November 3 1:30 PM'), :espn_game_id => 323082241
g = Game.create! :week_id => 10, :away_team_id => 167, :home_team_id => 172, :start_time => Time.parse('Saturday, November 3 1:30 PM'), :espn_game_id => 323082448
g = Game.create! :week_id => 10, :away_team_id => 200, :home_team_id => 198, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323080093
g = Game.create! :week_id => 10, :away_team_id => 151, :home_team_id => 149, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323080155
g = Game.create! :week_id => 10, :away_team_id => 220, :home_team_id => 225, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323080290
g = Game.create! :week_id => 10, :away_team_id => 245, :home_team_id => 237, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082010
g = Game.create! :week_id => 10, :away_team_id => 331, :home_team_id => 194, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082046
g = Game.create! :week_id => 10, :away_team_id => 59, :home_team_id => 65, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082309
g = Game.create! :week_id => 10, :away_team_id => 239, :home_team_id => 243, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082400
g = Game.create! :week_id => 10, :away_team_id => 166, :home_team_id => 173, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082428
g = Game.create! :week_id => 10, :away_team_id => 174, :home_team_id => 171, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082450
g = Game.create! :week_id => 10, :away_team_id => 238, :home_team_id => 244, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082504
g = Game.create! :week_id => 10, :away_team_id => 196, :home_team_id => 199, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082546
g = Game.create! :week_id => 10, :away_team_id => 179, :home_team_id => 178, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082623
g = Game.create! :week_id => 10, :away_team_id => 223, :home_team_id => 222, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082643
g = Game.create! :week_id => 10, :away_team_id => 180, :home_team_id => 183, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082710
g = Game.create! :week_id => 10, :away_team_id => 150, :home_team_id => 184, :start_time => Time.parse('Saturday, November 3 2:00 PM'), :espn_game_id => 323082754
g = Game.create! :week_id => 10, :away_team_id => 176, :home_team_id => 177, :start_time => Time.parse('Saturday, November 3 2:05 PM'), :espn_game_id => 323080282
g = Game.create! :week_id => 10, :away_team_id => 195, :home_team_id => 201, :start_time => Time.parse('Saturday, November 3 2:30 PM'), :espn_game_id => 323082635
g = Game.create! :week_id => 10, :away_team_id => 181, :home_team_id => 182, :start_time => Time.parse('Saturday, November 3 3:00 PM'), :espn_game_id => 323080079
g = Game.create! :week_id => 10, :away_team_id => 228, :home_team_id => 226, :start_time => Time.parse('Saturday, November 3 3:00 PM'), :espn_game_id => 323082535
g = Game.create! :week_id => 10, :away_team_id => 240, :home_team_id => 246, :start_time => Time.parse('Saturday, November 3 3:00 PM'), :espn_game_id => 323082640
g = Game.create! :week_id => 10, :away_team_id => 242, :home_team_id => 241, :start_time => Time.parse('Saturday, November 3 3:00 PM'), :espn_game_id => 323082755
g = Game.create! :week_id => 10, :away_team_id => 145, :home_team_id => 137, :start_time => Time.parse('Saturday, November 3 3:30 PM'), :espn_game_id => 323080048
g = Game.create! :week_id => 10, :away_team_id => 26, :home_team_id => 58, :start_time => Time.parse('Saturday, November 3 3:30 PM'), :espn_game_id => 323080087
g = Game.create! :week_id => 10, :away_team_id => 130, :home_team_id => 133, :start_time => Time.parse('Saturday, November 3 3:30 PM'), :espn_game_id => 323082335
g = Game.create! :week_id => 10, :away_team_id => 140, :home_team_id => 67, :start_time => Time.parse('Saturday, November 3 3:30 PM'), :espn_game_id => 323082459
g = Game.create! :week_id => 10, :away_team_id => 123, :home_team_id => 129, :start_time => Time.parse('Saturday, November 3 3:30 PM'), :espn_game_id => 323082692
g = Game.create! :week_id => 10, :away_team_id => 221, :home_team_id => 227, :start_time => Time.parse('Saturday, November 3 3:30 PM'), :espn_game_id => 323082717
g = Game.create! :week_id => 10, :away_team_id => 156, :home_team_id => 116, :start_time => Time.parse('Saturday, November 3 4:00 PM'), :espn_game_id => 323082348
g = Game.create! :week_id => 10, :away_team_id => 218, :home_team_id => 216, :start_time => Time.parse('Saturday, November 3 4:00 PM'), :espn_game_id => 323082368
g = Game.create! :week_id => 10, :away_team_id => 231, :home_team_id => 232, :start_time => Time.parse('Saturday, November 3 4:00 PM'), :espn_game_id => 323082447
g = Game.create! :week_id => 10, :away_team_id => 126, :home_team_id => 127, :start_time => Time.parse('Saturday, November 3 4:05 PM'), :espn_game_id => 323082502
g = Game.create! :week_id => 10, :away_team_id => 74, :home_team_id => 79, :start_time => Time.parse('Saturday, November 3 4:30 PM'), :espn_game_id => 323082751
g = Game.create! :week_id => 10, :away_team_id => 119, :home_team_id => 115, :start_time => Time.parse('Saturday, November 3 5:00 PM'), :espn_game_id => 323080070
g = Game.create! :week_id => 10, :away_team_id => 125, :home_team_id => 122, :start_time => Time.parse('Saturday, November 3 6:00 PM'), :espn_game_id => 323080304
g = Game.create! :week_id => 10, :away_team_id => 230, :home_team_id => 236, :start_time => Time.parse('Saturday, November 3 7:00 PM'), :espn_game_id => 323082617
g = Game.create! :week_id => 10, :away_team_id => 124, :home_team_id => 128, :start_time => Time.parse('Saturday, November 3 7:05 PM'), :espn_game_id => 323080016
g = Game.create! :week_id => 10, :away_team_id => 92, :home_team_id => 98, :start_time => Time.parse('Saturday, November 3 8:00 PM'), :espn_game_id => 323080099
g = Game.create! :week_id => 10, :away_team_id => 233, :home_team_id => 229, :start_time => Time.parse('Saturday, November 3 8:00 PM'), :espn_game_id => 323082110
g = Game.create! :week_id => 10, :away_team_id => 60, :home_team_id => 70, :start_time => Time.parse('Tuesday, November 6 8:00 PM'), :espn_game_id => 323112649
g = Game.create! :week_id => 10, :away_team_id => 61, :home_team_id => 68, :start_time => Time.parse('Wednesday, November 7 8:00 PM'), :espn_game_id => 323120195
g = Game.create! :week_id => 11, :away_team_id => 108, :home_team_id => 104, :start_time => Time.parse('Thursday, November 8 7:00 PM'), :espn_game_id => 323132032
g = Game.create! :week_id => 11, :away_team_id => 4, :home_team_id => 11, :start_time => Time.parse('Thursday, November 8 7:30 PM'), :espn_game_id => 323130259
g = Game.create! :week_id => 11, :away_team_id => 26, :home_team_id => 24, :start_time => Time.parse('Friday, November 9 8:00 PM'), :espn_game_id => 323140041
g = Game.create! :week_id => 11, :away_team_id => 96, :home_team_id => 94, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150002
g = Game.create! :week_id => 11, :away_team_id => 45, :home_team_id => 52, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150005
g = Game.create! :week_id => 11, :away_team_id => 83, :home_team_id => 80, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150012
g = Game.create! :week_id => 11, :away_team_id => 72, :home_team_id => 76, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150021
g = Game.create! :week_id => 11, :away_team_id => 85, :home_team_id => 86, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150024
g = Game.create! :week_id => 11, :away_team_id => 84, :home_team_id => 82, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150025
g = Game.create! :week_id => 11, :away_team_id => 81, :home_team_id => 88, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150030
g = Game.create! :week_id => 11, :away_team_id => 78, :home_team_id => 74, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150036
g = Game.create! :week_id => 11, :away_team_id => 107, :home_team_id => 95, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150057
g = Game.create! :week_id => 11, :away_team_id => 73, :home_team_id => 114, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150062
g = Game.create! :week_id => 11, :away_team_id => 42, :home_team_id => 32, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150084
g = Game.create! :week_id => 11, :away_team_id => 105, :home_team_id => 112, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150098
g = Game.create! :week_id => 11, :away_team_id => 99, :home_team_id => 98, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150099
g = Game.create! :week_id => 11, :away_team_id => 58, :home_team_id => 1, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150103
g = Game.create! :week_id => 11, :away_team_id => 38, :home_team_id => 34, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150130
g = Game.create! :week_id => 11, :away_team_id => 103, :home_team_id => 100, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150145
g = Game.create! :week_id => 11, :away_team_id => 12, :home_team_id => 9, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150152
g = Game.create! :week_id => 11, :away_team_id => 5, :home_team_id => 8, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150153
g = Game.create! :week_id => 11, :away_team_id => 40, :home_team_id => 37, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150158
g = Game.create! :week_id => 11, :away_team_id => 55, :home_team_id => 27, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150164
g = Game.create! :week_id => 11, :away_team_id => 79, :home_team_id => 75, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150167
g = Game.create! :week_id => 11, :away_team_id => 159, :home_team_id => 158, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150171
g = Game.create! :week_id => 11, :away_team_id => 25, :home_team_id => 29, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150183
g = Game.create! :week_id => 11, :away_team_id => 30, :home_team_id => 19, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150197
g = Game.create! :week_id => 11, :away_team_id => 13, :home_team_id => 18, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150201
g = Game.create! :week_id => 11, :away_team_id => 23, :home_team_id => 69, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150218
g = Game.create! :week_id => 11, :away_team_id => 161, :home_team_id => 162, :start_time => Time.parse('Saturday, November 10 12:00 PM'), :espn_game_id => 323150219
g = Game.create! :week_id => 11, :away_team_id => 138, :home_team_id => 146, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150222
g = Game.create! :week_id => 11, :away_team_id => 6, :home_team_id => 2, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150228
g = Game.create! :week_id => 11, :away_team_id => 50, :home_team_id => 46, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150235
g = Game.create! :week_id => 11, :away_team_id => 51, :home_team_id => 44, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150248
g = Game.create! :week_id => 11, :away_team_id => 154, :home_team_id => 110, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150249
g = Game.create! :week_id => 11, :away_team_id => 14, :home_team_id => 20, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150251
g = Game.create! :week_id => 11, :away_team_id => 115, :home_team_id => 56, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150252
g = Game.create! :week_id => 11, :away_team_id => 137, :home_team_id => 144, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150257
g = Game.create! :week_id => 11, :away_team_id => 7, :home_team_id => 10, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150258
g = Game.create! :week_id => 11, :away_team_id => 89, :home_team_id => 90, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150264
g = Game.create! :week_id => 11, :away_team_id => 87, :home_team_id => 91, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150265
g = Game.create! :week_id => 11, :away_team_id => 147, :home_team_id => 142, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150295
g = Game.create! :week_id => 11, :away_team_id => 153, :home_team_id => 139, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150311
g = Game.create! :week_id => 11, :away_team_id => 116, :home_team_id => 155, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150326
g = Game.create! :week_id => 11, :away_team_id => 152, :home_team_id => 121, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150331
g = Game.create! :week_id => 11, :away_team_id => 21, :home_team_id => 92, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150333
g = Game.create! :week_id => 11, :away_team_id => 36, :home_team_id => 31, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323150356
g = Game.create! :week_id => 11, :away_team_id => 224, :home_team_id => 220, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152026
g = Game.create! :week_id => 11, :away_team_id => 71, :home_team_id => 62, :start_time => Time.parse('Saturday, November 10 12:00 PM'), :espn_game_id => 323152084
g = Game.create! :week_id => 11, :away_team_id => 186, :home_team_id => 187, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152115
g = Game.create! :week_id => 11, :away_team_id => 185, :home_team_id => 188, :start_time => Time.parse('Saturday, November 10 12:00 PM'), :espn_game_id => 323152184
g = Game.create! :week_id => 11, :away_team_id => 198, :home_team_id => 196, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152198
g = Game.create! :week_id => 11, :away_team_id => 63, :home_team_id => 64, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152199
g = Game.create! :week_id => 11, :away_team_id => 41, :home_team_id => 33, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152294
g = Game.create! :week_id => 11, :away_team_id => 232, :home_team_id => 230, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152320
g = Game.create! :week_id => 11, :away_team_id => 213, :home_team_id => 216, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152368
g = Game.create! :week_id => 11, :away_team_id => 192, :home_team_id => 189, :start_time => Time.parse('Saturday, November 10 12:00 PM'), :espn_game_id => 323152405
g = Game.create! :week_id => 11, :away_team_id => 113, :home_team_id => 117, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152440
g = Game.create! :week_id => 11, :away_team_id => 131, :home_team_id => 134, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152506
g = Game.create! :week_id => 11, :away_team_id => 190, :home_team_id => 191, :start_time => Time.parse('Saturday, November 10 12:00 PM'), :espn_game_id => 323152529
g = Game.create! :week_id => 11, :away_team_id => 236, :home_team_id => 235, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152545
g = Game.create! :week_id => 11, :away_team_id => 49, :home_team_id => 48, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152567
g = Game.create! :week_id => 11, :away_team_id => 93, :home_team_id => 101, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152579
g = Game.create! :week_id => 11, :away_team_id => 16, :home_team_id => 77, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152628
g = Game.create! :week_id => 11, :away_team_id => 17, :home_team_id => 102, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152633
g = Game.create! :week_id => 11, :away_team_id => 53, :home_team_id => 54, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152638
g = Game.create! :week_id => 11, :away_team_id => 15, :home_team_id => 22, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152641
g = Game.create! :week_id => 11, :away_team_id => 207, :home_team_id => 193, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152681
g = Game.create! :week_id => 11, :away_team_id => 221, :home_team_id => 228, :start_time => Time.parse('Saturday, November 10 10:59:59 PM'), :espn_game_id => 323152747
g = Game.create! :week_id => 11, :away_team_id => 163, :home_team_id => 164, :start_time => Time.parse('Saturday, November 10 12:30 PM'), :espn_game_id => 323150043
g = Game.create! :week_id => 11, :away_team_id => 204, :home_team_id => 209, :start_time => Time.parse('Saturday, November 10 12:30 PM'), :espn_game_id => 323152329
g = Game.create! :week_id => 11, :away_team_id => 203, :home_team_id => 206, :start_time => Time.parse('Saturday, November 10 1:00 PM'), :espn_game_id => 323150046
g = Game.create! :week_id => 11, :away_team_id => 65, :home_team_id => 66, :start_time => Time.parse('Saturday, November 10 1:00 PM'), :espn_game_id => 323150193
g = Game.create! :week_id => 11, :away_team_id => 180, :home_team_id => 150, :start_time => Time.parse('Saturday, November 10 1:00 PM'), :espn_game_id => 323150233
g = Game.create! :week_id => 11, :away_team_id => 211, :home_team_id => 215, :start_time => Time.parse('Saturday, November 10 1:00 PM'), :espn_game_id => 323150294
g = Game.create! :week_id => 11, :away_team_id => 168, :home_team_id => 166, :start_time => Time.parse('Saturday, November 10 1:00 PM'), :espn_game_id => 323152169
g = Game.create! :week_id => 11, :away_team_id => 208, :home_team_id => 205, :start_time => Time.parse('Saturday, November 10 1:00 PM'), :espn_game_id => 323152230
g = Game.create! :week_id => 11, :away_team_id => 170, :home_team_id => 171, :start_time => Time.parse('Saturday, November 10 1:00 PM'), :espn_game_id => 323152450
g = Game.create! :week_id => 11, :away_team_id => 157, :home_team_id => 160, :start_time => Time.parse('Saturday, November 10 1:30 PM'), :espn_game_id => 323150159
g = Game.create! :week_id => 11, :away_team_id => 132, :home_team_id => 130, :start_time => Time.parse('Saturday, November 10 1:30 PM'), :espn_game_id => 323152127
g = Game.create! :week_id => 11, :away_team_id => 226, :home_team_id => 223, :start_time => Time.parse('Saturday, November 10 1:30 PM'), :espn_game_id => 323152210
g = Game.create! :week_id => 11, :away_team_id => 175, :home_team_id => 172, :start_time => Time.parse('Saturday, November 10 1:30 PM'), :espn_game_id => 323152448
g = Game.create! :week_id => 11, :away_team_id => 222, :home_team_id => 136, :start_time => Time.parse('Saturday, November 10 1:30 PM'), :espn_game_id => 323152678
g = Game.create! :week_id => 11, :away_team_id => 169, :home_team_id => 225, :start_time => Time.parse('Saturday, November 10 2:00 PM'), :espn_game_id => 323150290
g = Game.create! :week_id => 11, :away_team_id => 140, :home_team_id => 59, :start_time => Time.parse('Saturday, November 10 2:00 PM'), :espn_game_id => 323152006
g = Game.create! :week_id => 11, :away_team_id => 210, :home_team_id => 214, :start_time => Time.parse('Saturday, November 10 2:00 PM'), :espn_game_id => 323152181
g = Game.create! :week_id => 11, :away_team_id => 244, :home_team_id => 243, :start_time => Time.parse('Saturday, November 10 2:00 PM'), :espn_game_id => 323152400
g = Game.create! :week_id => 11, :away_team_id => 231, :home_team_id => 156, :start_time => Time.parse('Saturday, November 10 2:00 PM'), :espn_game_id => 323152636
g = Game.create! :week_id => 11, :away_team_id => 212, :home_team_id => 219, :start_time => Time.parse('Saturday, November 10 2:00 PM'), :espn_game_id => 323152674
g = Game.create! :week_id => 11, :away_team_id => 184, :home_team_id => 183, :start_time => Time.parse('Saturday, November 10 2:00 PM'), :espn_game_id => 323152710
g = Game.create! :week_id => 11, :away_team_id => 199, :home_team_id => 195, :start_time => Time.parse('Saturday, November 10 2:30 PM'), :espn_game_id => 323152197
g = Game.create! :week_id => 11, :away_team_id => 202, :home_team_id => 201, :start_time => Time.parse('Saturday, November 10 2:30 PM'), :espn_game_id => 323152635
g = Game.create! :week_id => 11, :away_team_id => 173, :home_team_id => 167, :start_time => Time.parse('Saturday, November 10 3:00 PM'), :espn_game_id => 323150050
g = Game.create! :week_id => 11, :away_team_id => 246, :home_team_id => 239, :start_time => Time.parse('Saturday, November 10 3:00 PM'), :espn_game_id => 323152016
g = Game.create! :week_id => 11, :away_team_id => 237, :home_team_id => 242, :start_time => Time.parse('Saturday, November 10 3:00 PM'), :espn_game_id => 323152296
g = Game.create! :week_id => 11, :away_team_id => 240, :home_team_id => 241, :start_time => Time.parse('Saturday, November 10 3:00 PM'), :espn_game_id => 323152755
g = Game.create! :week_id => 11, :away_team_id => 143, :home_team_id => 145, :start_time => Time.parse('Saturday, November 10 3:30 PM'), :espn_game_id => 323150119
g = Game.create! :week_id => 11, :away_team_id => 127, :home_team_id => 124, :start_time => Time.parse('Saturday, November 10 3:30 PM'), :espn_game_id => 323150147
g = Game.create! :week_id => 11, :away_team_id => 119, :home_team_id => 118, :start_time => Time.parse('Saturday, November 10 3:30 PM'), :espn_game_id => 323150166
g = Game.create! :week_id => 11, :away_team_id => 135, :home_team_id => 133, :start_time => Time.parse('Saturday, November 10 3:30 PM'), :espn_game_id => 323152335
g = Game.create! :week_id => 11, :away_team_id => 57, :home_team_id => 111, :start_time => Time.parse('Saturday, November 10 3:30 PM'), :espn_game_id => 323152653
g = Game.create! :week_id => 11, :away_team_id => 126, :home_team_id => 129, :start_time => Time.parse('Saturday, November 10 3:30 PM'), :espn_game_id => 323152692
g = Game.create! :week_id => 11, :away_team_id => 194, :home_team_id => 197, :start_time => Time.parse('Saturday, November 10 4:00 PM'), :espn_game_id => 323150055
g = Game.create! :week_id => 11, :away_team_id => 181, :home_team_id => 179, :start_time => Time.parse('Saturday, November 10 4:00 PM'), :espn_game_id => 323152449
g = Game.create! :week_id => 11, :away_team_id => 165, :home_team_id => 174, :start_time => Time.parse('Saturday, November 10 5:00 PM'), :espn_game_id => 323152542
g = Game.create! :week_id => 11, :away_team_id => 151, :home_team_id => 125, :start_time => Time.parse('Saturday, November 10 6:05 PM'), :espn_game_id => 323152464
g = Game.create! :week_id => 11, :away_team_id => 234, :home_team_id => 233, :start_time => Time.parse('Saturday, November 10 7:00 PM'), :espn_game_id => 323152466
g = Game.create! :week_id => 11, :away_team_id => 238, :home_team_id => 245, :start_time => Time.parse('Saturday, November 10 7:00 PM'), :espn_game_id => 323152582
g = Game.create! :week_id => 11, :away_team_id => 217, :home_team_id => 218, :start_time => Time.parse('Saturday, November 10 9:00 PM'), :espn_game_id => 323150301
g = Game.create! :week_id => 11, :away_team_id => 122, :home_team_id => 148, :start_time => Time.parse('Saturday, November 10 9:05 PM'), :espn_game_id => 323150013
g = Game.create! :week_id => 11, :away_team_id => 68, :home_team_id => 60, :start_time => Time.parse('Wednesday, November 14 8:00 PM'), :espn_game_id => 323192050
g = Game.create! :week_id => 11, :away_team_id => 70, :home_team_id => 67, :start_time => Time.parse('Wednesday, November 14 9:00 PM'), :espn_game_id => 323192459
g = Game.create! :week_id => 12, :away_team_id => 235, :home_team_id => 232, :start_time => Time.parse('Thursday, November 15 7:00 PM'), :espn_game_id => 323202447
g = Game.create! :week_id => 12, :away_team_id => 8, :home_team_id => 10, :start_time => Time.parse('Thursday, November 15 7:30 PM'), :espn_game_id => 323200258
g = Game.create! :week_id => 12, :away_team_id => 106, :home_team_id => 105, :start_time => Time.parse('Friday, November 16 8:00 PM'), :espn_game_id => 323212226
g = Game.create! :week_id => 12, :away_team_id => 114, :home_team_id => 72, :start_time => Time.parse('Friday, November 16 9:30 PM'), :espn_game_id => 323212005
g = Game.create! :week_id => 12, :away_team_id => 237, :home_team_id => 94, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220002
g = Game.create! :week_id => 12, :away_team_id => 46, :home_team_id => 52, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220005
g = Game.create! :week_id => 12, :away_team_id => 109, :home_team_id => 154, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220006
g = Game.create! :week_id => 12, :away_team_id => 91, :home_team_id => 81, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220009
g = Game.create! :week_id => 12, :away_team_id => 88, :home_team_id => 87, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220026
g = Game.create! :week_id => 12, :away_team_id => 90, :home_team_id => 83, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220038
g = Game.create! :week_id => 12, :away_team_id => 197, :home_team_id => 95, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220057
g = Game.create! :week_id => 12, :away_team_id => 3, :home_team_id => 5, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220059
g = Game.create! :week_id => 12, :away_team_id => 225, :home_team_id => 96, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220061
g = Game.create! :week_id => 12, :away_team_id => 74, :home_team_id => 73, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220068
g = Game.create! :week_id => 12, :away_team_id => 226, :home_team_id => 97, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220096
g = Game.create! :week_id => 12, :away_team_id => 100, :home_team_id => 98, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220099
g = Game.create! :week_id => 12, :away_team_id => 11, :home_team_id => 1, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220103
g = Game.create! :week_id => 12, :away_team_id => 164, :home_team_id => 161, :start_time => Time.parse('Saturday, November 17 12:00 PM'), :espn_game_id => 323220108
g = Game.create! :week_id => 12, :away_team_id => 62, :home_team_id => 140, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220113
g = Game.create! :week_id => 12, :away_team_id => 4, :home_team_id => 6, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220120
g = Game.create! :week_id => 12, :away_team_id => 38, :home_team_id => 35, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220127
g = Game.create! :week_id => 12, :away_team_id => 33, :home_team_id => 34, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220130
g = Game.create! :week_id => 12, :away_team_id => 29, :home_team_id => 17, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220142
g = Game.create! :week_id => 12, :away_team_id => 36, :home_team_id => 37, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220158
g = Game.create! :week_id => 12, :away_team_id => 145, :home_team_id => 141, :start_time => Time.parse('Saturday, November 17 12:00 PM'), :espn_game_id => 323220160
g = Game.create! :week_id => 12, :away_team_id => 160, :home_team_id => 163, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220163
g = Game.create! :week_id => 12, :away_team_id => 117, :home_team_id => 75, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220167
g = Game.create! :week_id => 12, :away_team_id => 65, :home_team_id => 61, :start_time => Time.parse('Saturday, November 17 12:00 PM'), :espn_game_id => 323220189
g = Game.create! :week_id => 12, :away_team_id => 22, :home_team_id => 19, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220197
g = Game.create! :week_id => 12, :away_team_id => 53, :home_team_id => 51, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220202
g = Game.create! :week_id => 12, :away_team_id => 82, :home_team_id => 85, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220204
g = Game.create! :week_id => 12, :away_team_id => 32, :home_team_id => 40, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220213
g = Game.create! :week_id => 12, :away_team_id => 158, :home_team_id => 157, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220225
g = Game.create! :week_id => 12, :away_team_id => 139, :home_team_id => 143, :start_time => Time.parse('Saturday, November 17 12:00 PM'), :espn_game_id => 323220227
g = Game.create! :week_id => 12, :away_team_id => 9, :home_team_id => 2, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220228
g = Game.create! :week_id => 12, :away_team_id => 102, :home_team_id => 103, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220238
g = Game.create! :week_id => 12, :away_team_id => 16, :home_team_id => 13, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220239
g = Game.create! :week_id => 12, :away_team_id => 48, :home_team_id => 47, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220242
g = Game.create! :week_id => 12, :away_team_id => 234, :home_team_id => 21, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220245
g = Game.create! :week_id => 12, :away_team_id => 80, :home_team_id => 89, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220254
g = Game.create! :week_id => 12, :away_team_id => 142, :home_team_id => 138, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220256
g = Game.create! :week_id => 12, :away_team_id => 39, :home_team_id => 42, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220275
g = Game.create! :week_id => 12, :away_team_id => 44, :home_team_id => 45, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220276
g = Game.create! :week_id => 12, :away_team_id => 18, :home_team_id => 30, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220277
g = Game.create! :week_id => 12, :away_team_id => 128, :home_team_id => 152, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220302
g = Game.create! :week_id => 12, :away_team_id => 112, :home_team_id => 107, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220309
g = Game.create! :week_id => 12, :away_team_id => 130, :home_team_id => 131, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220324
g = Game.create! :week_id => 12, :away_team_id => 227, :home_team_id => 92, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220333
g = Game.create! :week_id => 12, :away_team_id => 93, :home_team_id => 99, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220344
g = Game.create! :week_id => 12, :away_team_id => 69, :home_team_id => 55, :start_time => Time.parse('Saturday, November 17 12:00 PM'), :espn_game_id => 323220349
g = Game.create! :week_id => 12, :away_team_id => 41, :home_team_id => 31, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220356
g = Game.create! :week_id => 12, :away_team_id => 187, :home_team_id => 185, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323220399
g = Game.create! :week_id => 12, :away_team_id => 167, :home_team_id => 165, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222065
g = Game.create! :week_id => 12, :away_team_id => 195, :home_team_id => 229, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222110
g = Game.create! :week_id => 12, :away_team_id => 27, :home_team_id => 23, :start_time => Time.parse('Saturday, November 17 12:00 PM'), :espn_game_id => 323222132
g = Game.create! :week_id => 12, :away_team_id => 14, :home_team_id => 15, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222305
g = Game.create! :week_id => 12, :away_team_id => 120, :home_team_id => 116, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222348
g = Game.create! :week_id => 12, :away_team_id => 28, :home_team_id => 7, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222390
g = Game.create! :week_id => 12, :away_team_id => 155, :home_team_id => 57, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222426
g = Game.create! :week_id => 12, :away_team_id => 110, :home_team_id => 108, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222433
g = Game.create! :week_id => 12, :away_team_id => 79, :home_team_id => 78, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222439
g = Game.create! :week_id => 12, :away_team_id => 86, :home_team_id => 84, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222483
g = Game.create! :week_id => 12, :away_team_id => 121, :home_team_id => 127, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222502
g = Game.create! :week_id => 12, :away_team_id => 189, :home_team_id => 190, :start_time => Time.parse('Saturday, November 17 12:00 PM'), :espn_game_id => 323222523
g = Game.create! :week_id => 12, :away_team_id => 174, :home_team_id => 175, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222569
g = Game.create! :week_id => 12, :away_team_id => 54, :home_team_id => 49, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222572
g = Game.create! :week_id => 12, :away_team_id => 228, :home_team_id => 101, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222579
g = Game.create! :week_id => 12, :away_team_id => 191, :home_team_id => 192, :start_time => Time.parse('Saturday, November 17 12:00 PM'), :espn_game_id => 323222598
g = Game.create! :week_id => 12, :away_team_id => 200, :home_team_id => 202, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222630
g = Game.create! :week_id => 12, :away_team_id => 104, :home_team_id => 111, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222653
g = Game.create! :week_id => 12, :away_team_id => 43, :home_team_id => 50, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222655
g = Game.create! :week_id => 12, :away_team_id => 188, :home_team_id => 193, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222681
g = Game.create! :week_id => 12, :away_team_id => 64, :home_team_id => 71, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222711
g = Game.create! :week_id => 12, :away_team_id => 144, :home_team_id => 147, :start_time => Time.parse('Saturday, November 17 10:59:59 PM'), :espn_game_id => 323222729
g = Game.create! :week_id => 12, :away_team_id => 162, :home_team_id => 159, :start_time => Time.parse('Saturday, November 17 12:30 PM'), :espn_game_id => 323220172
g = Game.create! :week_id => 12, :away_team_id => 207, :home_team_id => 206, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323220046
g = Game.create! :week_id => 12, :away_team_id => 166, :home_team_id => 169, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323220047
g = Game.create! :week_id => 12, :away_team_id => 214, :home_team_id => 215, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323220294
g = Game.create! :week_id => 12, :away_team_id => 209, :home_team_id => 208, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323220322
g = Game.create! :week_id => 12, :away_team_id => 186, :home_team_id => 203, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323222083
g = Game.create! :week_id => 12, :away_team_id => 216, :home_team_id => 211, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323222097
g = Game.create! :week_id => 12, :away_team_id => 66, :home_team_id => 63, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323222117
g = Game.create! :week_id => 12, :away_team_id => 218, :home_team_id => 212, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323222166
g = Game.create! :week_id => 12, :away_team_id => 204, :home_team_id => 205, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323222230
g = Game.create! :week_id => 12, :away_team_id => 219, :home_team_id => 217, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323222413
g = Game.create! :week_id => 12, :away_team_id => 168, :home_team_id => 170, :start_time => Time.parse('Saturday, November 17 1:00 PM'), :espn_game_id => 323222415
g = Game.create! :week_id => 12, :away_team_id => 222, :home_team_id => 224, :start_time => Time.parse('Saturday, November 17 1:30 PM'), :espn_game_id => 323220231
g = Game.create! :week_id => 12, :away_team_id => 134, :home_team_id => 132, :start_time => Time.parse('Saturday, November 17 1:30 PM'), :espn_game_id => 323222241
g = Game.create! :week_id => 12, :away_team_id => 133, :home_team_id => 136, :start_time => Time.parse('Saturday, November 17 1:30 PM'), :espn_game_id => 323222678
g = Game.create! :week_id => 12, :away_team_id => 199, :home_team_id => 198, :start_time => Time.parse('Saturday, November 17 2:00 PM'), :espn_game_id => 323220093
g = Game.create! :week_id => 12, :away_team_id => 223, :home_team_id => 221, :start_time => Time.parse('Saturday, November 17 2:00 PM'), :espn_game_id => 323220236
g = Game.create! :week_id => 12, :away_team_id => 201, :home_team_id => 194, :start_time => Time.parse('Saturday, November 17 2:00 PM'), :espn_game_id => 323222046
g = Game.create! :week_id => 12, :away_team_id => 179, :home_team_id => 176, :start_time => Time.parse('Saturday, November 17 2:00 PM'), :espn_game_id => 323222287
g = Game.create! :week_id => 12, :away_team_id => 239, :home_team_id => 242, :start_time => Time.parse('Saturday, November 17 2:00 PM'), :espn_game_id => 323222296
g = Game.create! :week_id => 12, :away_team_id => 172, :home_team_id => 173, :start_time => Time.parse('Saturday, November 17 2:00 PM'), :espn_game_id => 323222428
g = Game.create! :week_id => 12, :away_team_id => 243, :home_team_id => 246, :start_time => Time.parse('Saturday, November 17 2:00 PM'), :espn_game_id => 323222640
g = Game.create! :week_id => 12, :away_team_id => 177, :home_team_id => 184, :start_time => Time.parse('Saturday, November 17 2:00 PM'), :espn_game_id => 323222754
g = Game.create! :week_id => 12, :away_team_id => 149, :home_team_id => 126, :start_time => Time.parse('Saturday, November 17 2:05 PM'), :espn_game_id => 323222458
g = Game.create! :week_id => 12, :away_team_id => 183, :home_team_id => 182, :start_time => Time.parse('Saturday, November 17 3:00 PM'), :espn_game_id => 323220079
g = Game.create! :week_id => 12, :away_team_id => 150, :home_team_id => 181, :start_time => Time.parse('Saturday, November 17 3:00 PM'), :espn_game_id => 323222571
g = Game.create! :week_id => 12, :away_team_id => 146, :home_team_id => 137, :start_time => Time.parse('Saturday, November 17 3:30 PM'), :espn_game_id => 323220048
g = Game.create! :week_id => 12, :away_team_id => 12, :home_team_id => 58, :start_time => Time.parse('Saturday, November 17 3:30 PM'), :espn_game_id => 323220087
g = Game.create! :week_id => 12, :away_team_id => 124, :home_team_id => 123, :start_time => Time.parse('Saturday, November 17 3:30 PM'), :espn_game_id => 323220149
g = Game.create! :week_id => 12, :away_team_id => 244, :home_team_id => 240, :start_time => Time.parse('Saturday, November 17 3:30 PM'), :espn_game_id => 323222029
g = Game.create! :week_id => 12, :away_team_id => 56, :home_team_id => 119, :start_time => Time.parse('Saturday, November 17 4:00 PM'), :espn_game_id => 323220023
g = Game.create! :week_id => 12, :away_team_id => 156, :home_team_id => 115, :start_time => Time.parse('Saturday, November 17 5:00 PM'), :espn_game_id => 323220070
g = Game.create! :week_id => 12, :away_team_id => 129, :home_team_id => 122, :start_time => Time.parse('Saturday, November 17 6:00 PM'), :espn_game_id => 323220304
g = Game.create! :week_id => 12, :away_team_id => 148, :home_team_id => 125, :start_time => Time.parse('Saturday, November 17 6:05 PM'), :espn_game_id => 323222464
g = Game.create! :week_id => 12, :away_team_id => 178, :home_team_id => 180, :start_time => Time.parse('Saturday, November 17 7:00 PM'), :espn_game_id => 323222460
g = Game.create! :week_id => 12, :away_team_id => 233, :home_team_id => 236, :start_time => Time.parse('Saturday, November 17 7:00 PM'), :espn_game_id => 323222617
g = Game.create! :week_id => 12, :away_team_id => 230, :home_team_id => 231, :start_time => Time.parse('Saturday, November 17 8:00 PM'), :espn_game_id => 323222377
g = Game.create! :week_id => 12, :away_team_id => 59, :home_team_id => 70, :start_time => Time.parse('Tuesday, November 20 7:00 PM'), :espn_game_id => 323252649
g = Game.create! :week_id => 13, :away_team_id => 295, :home_team_id => 238, :start_time => Time.parse('Thursday, November 22 4:00 PM'), :espn_game_id => 323272011
g = Game.create! :week_id => 13, :away_team_id => 89, :home_team_id => 83, :start_time => Time.parse('Friday, November 23 10:59:59 PM'), :espn_game_id => 323280038
g = Game.create! :week_id => 13, :away_team_id => 63, :home_team_id => 140, :start_time => Time.parse('Friday, November 23 10:59:59 PM'), :espn_game_id => 323280113
g = Game.create! :week_id => 13, :away_team_id => 45, :home_team_id => 43, :start_time => Time.parse('Friday, November 23 10:59:59 PM'), :espn_game_id => 323280151
g = Game.create! :week_id => 13, :away_team_id => 62, :home_team_id => 61, :start_time => Time.parse('Friday, November 23 10:59:59 PM'), :espn_game_id => 323280189
g = Game.create! :week_id => 13, :away_team_id => 60, :home_team_id => 66, :start_time => Time.parse('Friday, November 23 10:59:59 PM'), :espn_game_id => 323280193
g = Game.create! :week_id => 13, :away_team_id => 29, :home_team_id => 69, :start_time => Time.parse('Friday, November 23 10:59:59 PM'), :espn_game_id => 323280218
g = Game.create! :week_id => 13, :away_team_id => 90, :home_team_id => 91, :start_time => Time.parse('Friday, November 23 10:59:59 PM'), :espn_game_id => 323280265
g = Game.create! :week_id => 13, :away_team_id => 28, :home_team_id => 23, :start_time => Time.parse('Friday, November 23 10:59:59 PM'), :espn_game_id => 323282132
g = Game.create! :week_id => 13, :away_team_id => 67, :home_team_id => 64, :start_time => Time.parse('Friday, November 23 10:59:59 PM'), :espn_game_id => 323282199
g = Game.create! :week_id => 13, :away_team_id => 37, :home_team_id => 33, :start_time => Time.parse('Friday, November 23 12:00 PM'), :espn_game_id => 323282294
g = Game.create! :week_id => 13, :away_team_id => 68, :home_team_id => 65, :start_time => Time.parse('Friday, November 23 10:59:59 PM'), :espn_game_id => 323282309
g = Game.create! :week_id => 13, :away_team_id => 98, :home_team_id => 93, :start_time => Time.parse('Friday, November 23 2:30 PM'), :espn_game_id => 323280008
g = Game.create! :week_id => 13, :away_team_id => 81, :home_team_id => 80, :start_time => Time.parse('Friday, November 23 10:00 PM'), :espn_game_id => 323280012
g = Game.create! :week_id => 13, :away_team_id => 86, :home_team_id => 87, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290026
g = Game.create! :week_id => 13, :away_team_id => 58, :home_team_id => 88, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290030
g = Game.create! :week_id => 13, :away_team_id => 75, :home_team_id => 74, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290036
g = Game.create! :week_id => 13, :away_team_id => 95, :home_team_id => 4, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290052
g = Game.create! :week_id => 13, :away_team_id => 5, :home_team_id => 96, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290061
g = Game.create! :week_id => 13, :away_team_id => 78, :home_team_id => 114, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290062
g = Game.create! :week_id => 13, :away_team_id => 30, :home_team_id => 14, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290066
g = Game.create! :week_id => 13, :away_team_id => 31, :home_team_id => 38, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290077
g = Game.create! :week_id => 13, :away_team_id => 24, :home_team_id => 25, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290097
g = Game.create! :week_id => 13, :away_team_id => 110, :home_team_id => 112, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290098
g = Game.create! :week_id => 13, :away_team_id => 35, :home_team_id => 36, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290135
g = Game.create! :week_id => 13, :away_team_id => 99, :home_team_id => 100, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290145
g = Game.create! :week_id => 13, :away_team_id => 7, :home_team_id => 3, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290150
g = Game.create! :week_id => 13, :away_team_id => 1, :home_team_id => 9, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290152
g = Game.create! :week_id => 13, :away_team_id => 6, :home_team_id => 8, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290153
g = Game.create! :week_id => 13, :away_team_id => 103, :home_team_id => 12, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290154
g = Game.create! :week_id => 13, :away_team_id => 34, :home_team_id => 39, :start_time => Time.parse('Saturday, November 24 12:00 PM'), :espn_game_id => 323290194
g = Game.create! :week_id => 13, :away_team_id => 19, :home_team_id => 18, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290201
g = Game.create! :week_id => 13, :away_team_id => 84, :home_team_id => 85, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290204
g = Game.create! :week_id => 13, :away_team_id => 42, :home_team_id => 40, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290213
g = Game.create! :week_id => 13, :away_team_id => 27, :home_team_id => 26, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290221
g = Game.create! :week_id => 13, :away_team_id => 101, :home_team_id => 2, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290228
g = Game.create! :week_id => 13, :away_team_id => 49, :home_team_id => 46, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290235
g = Game.create! :week_id => 13, :away_team_id => 17, :home_team_id => 21, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290245
g = Game.create! :week_id => 13, :away_team_id => 50, :home_team_id => 44, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290248
g = Game.create! :week_id => 13, :away_team_id => 77, :home_team_id => 20, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290251
g = Game.create! :week_id => 13, :away_team_id => 10, :home_team_id => 11, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290259
g = Game.create! :week_id => 13, :away_team_id => 72, :home_team_id => 113, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290278
g = Game.create! :week_id => 13, :away_team_id => 154, :home_team_id => 107, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290309
g = Game.create! :week_id => 13, :away_team_id => 94, :home_team_id => 92, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323290333
g = Game.create! :week_id => 13, :away_team_id => 52, :home_team_id => 53, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323292116
g = Game.create! :week_id => 13, :away_team_id => 108, :home_team_id => 106, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323292229
g = Game.create! :week_id => 13, :away_team_id => 111, :home_team_id => 109, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323292393
g = Game.create! :week_id => 13, :away_team_id => 73, :home_team_id => 117, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323292440
g = Game.create! :week_id => 13, :away_team_id => 32, :home_team_id => 41, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323292509
g = Game.create! :week_id => 13, :away_team_id => 51, :home_team_id => 48, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323292567
g = Game.create! :week_id => 13, :away_team_id => 97, :home_team_id => 102, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323292633
g = Game.create! :week_id => 13, :away_team_id => 47, :home_team_id => 54, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323292638
g = Game.create! :week_id => 13, :away_team_id => 13, :home_team_id => 22, :start_time => Time.parse('Saturday, November 24 10:59:59 PM'), :espn_game_id => 323292641
g = Game.create! :week_id => 13, :away_team_id => 241, :home_team_id => 245, :start_time => Time.parse('Saturday, November 24 2:00 PM'), :espn_game_id => 323292582
g = Game.create! :week_id => 13, :away_team_id => 155, :home_team_id => 156, :start_time => Time.parse('Saturday, November 24 2:00 PM'), :espn_game_id => 323292636
g = Game.create! :week_id => 13, :away_team_id => 115, :home_team_id => 120, :start_time => Time.parse('Saturday, November 24 3:00 PM'), :espn_game_id => 323290328
g = Game.create! :week_id => 13, :away_team_id => 56, :home_team_id => 118, :start_time => Time.parse('Saturday, November 24 3:30 PM'), :espn_game_id => 323290166
g = Game.create! :week_id => 13, :away_team_id => 76, :home_team_id => 79, :start_time => Time.parse('Saturday, November 24 3:30 PM'), :espn_game_id => 323292751
g = Game.create! :week_id => 13, :away_team_id => 116, :home_team_id => 119, :start_time => Time.parse('Saturday, November 24 4:00 PM'), :espn_game_id => 323290023
g = Game.create! :week_id => 14, :away_team_id => 25, :home_team_id => 27, :start_time => Time.parse('Thursday, November 29 7:30 PM'), :espn_game_id => 323340164
g = Game.create! :week_id => 14, :away_team_id => 23, :home_team_id => 24, :start_time => Time.parse('Saturday, December 1 10:59:59 PM'), :espn_game_id => 323360041
g = Game.create! :week_id => 14, :away_team_id => 26, :home_team_id => 28, :start_time => Time.parse('Saturday, December 1 10:59:59 PM'), :espn_game_id => 323360058
g = Game.create! :week_id => 14, :away_team_id => 154, :home_team_id => 114, :start_time => Time.parse('Saturday, December 1 10:59:59 PM'), :espn_game_id => 323360062
g = Game.create! :week_id => 14, :away_team_id => 19, :home_team_id => 13, :start_time => Time.parse('Saturday, December 1 10:59:59 PM'), :espn_game_id => 323360239
g = Game.create! :week_id => 14, :away_team_id => 15, :home_team_id => 30, :start_time => Time.parse('Saturday, December 1 10:59:59 PM'), :espn_game_id => 323360277
g = Game.create! :week_id => 14, :away_team_id => 118, :home_team_id => 155, :start_time => Time.parse('Saturday, December 1 10:59:59 PM'), :espn_game_id => 323360326
g = Game.create! :week_id => 14, :away_team_id => 109, :home_team_id => 104, :start_time => Time.parse('Saturday, December 1 10:59:59 PM'), :espn_game_id => 323362032
g = Game.create! :week_id => 14, :away_team_id => 107, :home_team_id => 105, :start_time => Time.parse('Saturday, December 1 10:59:59 PM'), :espn_game_id => 323362226
g = Game.create! :week_id => 14, :away_team_id => 20, :home_team_id => 16, :start_time => Time.parse('Saturday, December 1 10:59:59 PM'), :espn_game_id => 323362306
g = Game.create! :week_id => 14, :away_team_id => 18, :home_team_id => 77, :start_time => Time.parse('Saturday, December 1 10:59:59 PM'), :espn_game_id => 323362628
g = Game.create! :week_id => 15, :away_team_id => 57, :home_team_id => 55, :start_time => Time.parse('Saturday, December 8 3:00 PM'), :espn_game_id => 323430349

#week 7 games below for now

g = Game.create! :week_id => 7, :home_team_id => 112, :away_team_id => 111, :start_time => Time.parse('Thursday, October 11 7:30 PM'), :espn_game_id => 322852653
g = Game.create! :week_id => 7, :home_team_id => 81, :away_team_id => 83, :start_time => Time.parse('Thursday, October 11 9:00 PM'), :espn_game_id => 322850038
g = Game.create! :week_id => 7, :home_team_id => 57, :away_team_id => 63, :start_time => Time.parse('Friday, October 12 8:00 PM'), :espn_game_id => 322862117
g = Game.create! :week_id => 7, :home_team_id => 97, :away_team_id => 93, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870008
g = Game.create! :week_id => 7, :home_team_id => 74, :away_team_id => 76, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870021
g = Game.create! :week_id => 7, :home_team_id => 89, :away_team_id => 87, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870026
g = Game.create! :week_id => 7, :home_team_id => 69, :away_team_id => 24, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870041
g = Game.create! :week_id => 7, :home_team_id => 174, :away_team_id => 167, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870050
g = Game.create! :week_id => 7, :home_team_id => 1, :away_team_id => 4, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870052
g = Game.create! :week_id => 7, :home_team_id => 75, :away_team_id => 114, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870062
g = Game.create! :week_id => 7, :home_team_id => 16, :away_team_id => 14, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870066
g = Game.create! :week_id => 7, :home_team_id => 113, :away_team_id => 73, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870068
g = Game.create! :week_id => 7, :home_team_id => 39, :away_team_id => 32, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870084
g = Game.create! :week_id => 7, :home_team_id => 86, :away_team_id => 58, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870087
g = Game.create! :week_id => 7, :home_team_id => 101, :away_team_id => 98, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870099
g = Game.create! :week_id => 7, :home_team_id => 33, :away_team_id => 35, :start_time => Time.parse('Saturday, October 13 12:00 PM'), :espn_game_id => 322870127
g = Game.create! :week_id => 7, :home_team_id => 38, :away_team_id => 36, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870135
g = Game.create! :week_id => 7, :home_team_id => 92, :away_team_id => 17, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870142
g = Game.create! :week_id => 7, :home_team_id => 94, :away_team_id => 100, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870145
g = Game.create! :week_id => 7, :home_team_id => 151, :away_team_id => 123, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870149
g = Game.create! :week_id => 7, :home_team_id => 46, :away_team_id => 43, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870151
g = Game.create! :week_id => 7, :home_team_id => 144, :away_team_id => 141, :start_time => Time.parse('Saturday, October 13 12:00 PM'), :espn_game_id => 322870160
g = Game.create! :week_id => 7, :home_team_id => 157, :away_team_id => 163, :start_time => Time.parse('Saturday, October 13 12:00 PM'), :espn_game_id => 322870163
g = Game.create! :week_id => 7, :home_team_id => 29, :away_team_id => 27, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870164
g = Game.create! :week_id => 7, :home_team_id => 20, :away_team_id => 18, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870201
g = Game.create! :week_id => 7, :home_team_id => 54, :away_team_id => 51, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870202
g = Game.create! :week_id => 7, :home_team_id => 158, :away_team_id => 162, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870219
g = Game.create! :week_id => 7, :home_team_id => 25, :away_team_id => 26, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870221
g = Game.create! :week_id => 7, :home_team_id => 95, :away_team_id => 103, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870238
g = Game.create! :week_id => 7, :home_team_id => 77, :away_team_id => 13, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870239
g = Game.create! :week_id => 7, :home_team_id => 156, :away_team_id => 47, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870242
g = Game.create! :week_id => 7, :home_team_id => 52, :away_team_id => 44, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870248
g = Game.create! :week_id => 7, :home_team_id => 85, :away_team_id => 56, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870252
g = Game.create! :week_id => 7, :home_team_id => 147, :away_team_id => 138, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870256
g = Game.create! :week_id => 7, :home_team_id => 6, :away_team_id => 10, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870258
g = Game.create! :week_id => 7, :home_team_id => 3, :away_team_id => 11, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870259
g = Game.create! :week_id => 7, :home_team_id => 88, :away_team_id => 90, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870264
g = Game.create! :week_id => 7, :home_team_id => 82, :away_team_id => 91, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870265
g = Game.create! :week_id => 7, :home_team_id => 146, :away_team_id => 142, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870295
g = Game.create! :week_id => 7, :home_team_id => 135, :away_team_id => 131, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870324
g = Game.create! :week_id => 7, :home_team_id => 115, :away_team_id => 155, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870326
g = Game.create! :week_id => 7, :home_team_id => 102, :away_team_id => 99, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322870344
g = Game.create! :week_id => 7, :home_team_id => 65, :away_team_id => 55, :start_time => Time.parse('Saturday, October 13 12:00 PM'), :espn_game_id => 322870349
g = Game.create! :week_id => 7, :home_team_id => 154, :away_team_id => 104, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872032
g = Game.create! :week_id => 7, :home_team_id => 71, :away_team_id => 60, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872050
g = Game.create! :week_id => 7, :home_team_id => 188, :away_team_id => 187, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872115
g = Game.create! :week_id => 7, :home_team_id => 49, :away_team_id => 53, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872116
g = Game.create! :week_id => 7, :home_team_id => 205, :away_team_id => 23, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872132
g = Game.create! :week_id => 7, :home_team_id => 207, :away_team_id => 204, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872142
g = Game.create! :week_id => 7, :home_team_id => 194, :away_team_id => 196, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872198
g = Game.create! :week_id => 7, :home_team_id => 70, :away_team_id => 64, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872199
g = Game.create! :week_id => 7, :home_team_id => 109, :away_team_id => 106, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872229
g = Game.create! :week_id => 7, :home_team_id => 19, :away_team_id => 15, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872305
g = Game.create! :week_id => 7, :home_team_id => 247, :away_team_id => 230, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872320
g = Game.create! :week_id => 7, :home_team_id => 8, :away_team_id => 7, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872390
g = Game.create! :week_id => 7, :home_team_id => 105, :away_team_id => 108, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872433
g = Game.create! :week_id => 7, :home_team_id => 117, :away_team_id => 78, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872439
g = Game.create! :week_id => 7, :home_team_id => 42, :away_team_id => 41, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872509
g = Game.create! :week_id => 7, :home_team_id => 233, :away_team_id => 235, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872545
g = Game.create! :week_id => 7, :home_team_id => 30, :away_team_id => 22, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872641
g = Game.create! :week_id => 7, :home_team_id => 48, :away_team_id => 50, :start_time => Time.parse('Saturday, October 13 10:59:59 PM'), :espn_game_id => 322872655
g = Game.create! :week_id => 7, :home_team_id => 208, :away_team_id => 164, :start_time => Time.parse('Saturday, October 13 12:30 PM'), :espn_game_id => 322870043
g = Game.create! :week_id => 7, :home_team_id => 189, :away_team_id => 159, :start_time => Time.parse('Saturday, October 13 12:30 PM'), :espn_game_id => 322870172
g = Game.create! :week_id => 7, :home_team_id => 209, :away_team_id => 206, :start_time => Time.parse('Saturday, October 13 1:00 PM'), :espn_game_id => 322870046
g = Game.create! :week_id => 7, :home_team_id => 153, :away_team_id => 143, :start_time => Time.parse('Saturday, October 13 1:00 PM'), :espn_game_id => 322870227
g = Game.create! :week_id => 7, :home_team_id => 192, :away_team_id => 185, :start_time => Time.parse('Saturday, October 13 1:00 PM'), :espn_game_id => 322870399
g = Game.create! :week_id => 7, :home_team_id => 215, :away_team_id => 212, :start_time => Time.parse('Saturday, October 13 1:00 PM'), :espn_game_id => 322872166
g = Game.create! :week_id => 7, :home_team_id => 217, :away_team_id => 213, :start_time => Time.parse('Saturday, October 13 1:00 PM'), :espn_game_id => 322872168
g = Game.create! :week_id => 7, :home_team_id => 171, :away_team_id => 168, :start_time => Time.parse('Saturday, October 13 1:00 PM'), :espn_game_id => 322872261
g = Game.create! :week_id => 7, :home_team_id => 210, :away_team_id => 216, :start_time => Time.parse('Saturday, October 13 1:00 PM'), :espn_game_id => 322872368
g = Game.create! :week_id => 7, :home_team_id => 173, :away_team_id => 170, :start_time => Time.parse('Saturday, October 13 1:00 PM'), :espn_game_id => 322872415
g = Game.create! :week_id => 7, :home_team_id => 190, :away_team_id => 186, :start_time => Time.parse('Saturday, October 13 1:00 PM'), :espn_game_id => 322872803
g = Game.create! :week_id => 7, :home_team_id => 191, :away_team_id => 160, :start_time => Time.parse('Saturday, October 13 1:30 PM'), :espn_game_id => 322870159
g = Game.create! :week_id => 7, :home_team_id => 221, :away_team_id => 224, :start_time => Time.parse('Saturday, October 13 1:30 PM'), :espn_game_id => 322870231
g = Game.create! :week_id => 7, :home_team_id => 136, :away_team_id => 130, :start_time => Time.parse('Saturday, October 13 1:30 PM'), :espn_game_id => 322872127
g = Game.create! :week_id => 7, :home_team_id => 175, :away_team_id => 166, :start_time => Time.parse('Saturday, October 13 1:30 PM'), :espn_game_id => 322872169
g = Game.create! :week_id => 7, :home_team_id => 169, :away_team_id => 172, :start_time => Time.parse('Saturday, October 13 1:30 PM'), :espn_game_id => 322872448
g = Game.create! :week_id => 7, :home_team_id => 133, :away_team_id => 134, :start_time => Time.parse('Saturday, October 13 1:30 PM'), :espn_game_id => 322872506
g = Game.create! :week_id => 7, :home_team_id => 59, :away_team_id => 68, :start_time => Time.parse('Saturday, October 13 2:00 PM'), :espn_game_id => 322870195
g = Game.create! :week_id => 7, :home_team_id => 239, :away_team_id => 237, :start_time => Time.parse('Saturday, October 13 2:00 PM'), :espn_game_id => 322872010
g = Game.create! :week_id => 7, :home_team_id => 242, :away_team_id => 238, :start_time => Time.parse('Saturday, October 13 2:00 PM'), :espn_game_id => 322872011
g = Game.create! :week_id => 7, :home_team_id => 184, :away_team_id => 176, :start_time => Time.parse('Saturday, October 13 2:00 PM'), :espn_game_id => 322872287
g = Game.create! :week_id => 7, :home_team_id => 150, :away_team_id => 178, :start_time => Time.parse('Saturday, October 13 2:00 PM'), :espn_game_id => 322872623
g = Game.create! :week_id => 7, :home_team_id => 227, :away_team_id => 222, :start_time => Time.parse('Saturday, October 13 2:00 PM'), :espn_game_id => 322872643
g = Game.create! :week_id => 7, :home_team_id => 214, :away_team_id => 219, :start_time => Time.parse('Saturday, October 13 2:00 PM'), :espn_game_id => 322872674
g = Game.create! :week_id => 7, :home_team_id => 197, :away_team_id => 195, :start_time => Time.parse('Saturday, October 13 2:30 PM'), :espn_game_id => 322872197
g = Game.create! :week_id => 7, :home_team_id => 180, :away_team_id => 182, :start_time => Time.parse('Saturday, October 13 3:00 PM'), :espn_game_id => 322870079
g = Game.create! :week_id => 7, :home_team_id => 241, :away_team_id => 243, :start_time => Time.parse('Saturday, October 13 3:00 PM'), :espn_game_id => 322872400
g = Game.create! :week_id => 7, :home_team_id => 220, :away_team_id => 226, :start_time => Time.parse('Saturday, October 13 3:00 PM'), :espn_game_id => 322872535
g = Game.create! :week_id => 7, :home_team_id => 203, :away_team_id => 161, :start_time => Time.parse('Saturday, October 13 3:30 PM'), :espn_game_id => 322870108
g = Game.create! :week_id => 7, :home_team_id => 31, :away_team_id => 34, :start_time => Time.parse('Saturday, October 13 3:30 PM'), :espn_game_id => 322870130
g = Game.create! :week_id => 7, :home_team_id => 66, :away_team_id => 61, :start_time => Time.parse('Saturday, October 13 3:30 PM'), :espn_game_id => 322870189
g = Game.create! :week_id => 7, :home_team_id => 62, :away_team_id => 67, :start_time => Time.parse('Saturday, October 13 3:30 PM'), :espn_game_id => 322872459
g = Game.create! :week_id => 7, :home_team_id => 121, :away_team_id => 124, :start_time => Time.parse('Saturday, October 13 3:35 PM'), :espn_game_id => 322870147
g = Game.create! :week_id => 7, :home_team_id => 120, :away_team_id => 119, :start_time => Time.parse('Saturday, October 13 4:00 PM'), :espn_game_id => 322870023
g = Game.create! :week_id => 7, :home_team_id => 202, :away_team_id => 198, :start_time => Time.parse('Saturday, October 13 4:00 PM'), :espn_game_id => 322870093
g = Game.create! :week_id => 7, :home_team_id => 125, :away_team_id => 149, :start_time => Time.parse('Saturday, October 13 4:00 PM'), :espn_game_id => 322870155
g = Game.create! :week_id => 7, :home_team_id => 177, :away_team_id => 179, :start_time => Time.parse('Saturday, October 13 4:00 PM'), :espn_game_id => 322872449
g = Game.create! :week_id => 7, :home_team_id => 211, :away_team_id => 218, :start_time => Time.parse('Saturday, October 13 5:00 PM'), :espn_game_id => 322870301
g = Game.create! :week_id => 7, :home_team_id => 228, :away_team_id => 225, :start_time => Time.parse('Saturday, October 13 6:00 PM'), :espn_game_id => 322870290
g = Game.create! :week_id => 7, :home_team_id => 152, :away_team_id => 122, :start_time => Time.parse('Saturday, October 13 6:00 PM'), :espn_game_id => 322870304
g = Game.create! :week_id => 7, :home_team_id => 335, :away_team_id => 132, :start_time => Time.parse('Saturday, October 13 6:00 PM'), :espn_game_id => 322872241
g = Game.create! :week_id => 7, :home_team_id => 246, :away_team_id => 245, :start_time => Time.parse('Saturday, October 13 6:30 PM'), :espn_game_id => 322872582
g = Game.create! :week_id => 7, :home_team_id => 139, :away_team_id => 145, :start_time => Time.parse('Saturday, October 13 7:00 PM'), :espn_game_id => 322870119
g = Game.create! :week_id => 7, :home_team_id => 234, :away_team_id => 232, :start_time => Time.parse('Saturday, October 13 4:00 PM'), :espn_game_id => 322872447
g = Game.create! :week_id => 7, :home_team_id => 200, :away_team_id => 199, :start_time => Time.parse('Saturday, October 13 4:00 PM'), :espn_game_id => 322872546
g = Game.create! :week_id => 7, :home_team_id => 183, :away_team_id => 181, :start_time => Time.parse('Saturday, October 13 4:00 PM'), :espn_game_id => 322872571
g = Game.create! :week_id => 7, :home_team_id => 72, :away_team_id => 79, :start_time => Time.parse('Saturday, October 13 4:00 PM'), :espn_game_id => 322872751
g = Game.create! :week_id => 7, :home_team_id => 229, :away_team_id => 231, :start_time => Time.parse('Saturday, October 13 8:00 PM'), :espn_game_id => 322872377
g = Game.create! :week_id => 7, :home_team_id => 126, :away_team_id => 148, :start_time => Time.parse('Saturday, October 13 9:05 PM'), :espn_game_id => 322870013
g = Game.create! :week_id => 7, :home_team_id => 129, :away_team_id => 128, :start_time => Time.parse('Saturday, October 13 4:00 PM'), :espn_game_id => 322870016
g = Game.create! :week_id => 7, :home_team_id => 107, :away_team_id => 110, :start_time => Time.parse('Tuesday, October 16 9:00 PM'), :espn_game_id => 322900249

puts 'ADDING PICKS REQUIRED'
# Pre-defined points games (must add BCS game at the end of season)

# picks_req = [66,229,268,383,492,638,730,908,1018,1114,1180,1292,1412,1450,1482]
# picks_req.each do |pr|
# 	Game.find(pr).update_attributes(:points_req => true)
# end

puts 'SETTING UP FINISHED SCORES'

# s = Score.create! :game_id => 1480, :away_score => 23, :home_score => 20, :status => 'Final/OT', :final => true #SUGAR
# s = Score.create! :game_id => 1482, :away_score => ALA, :home_score => LSU, :status => 'Final', :final => true #FBS Champ

puts 'SETTING UP DEFAULT PICKS'

# Week 1
# p = Pick.create! :user_id => 18, :game_id => 43, :home_win_pick => 1
# p = Pick.create! :user_id => 1, :game_id => 66, :home_win_pick => 1, :away_score_pick => 24, :home_score_pick => 28
# Add Season information

puts 'CHANGING GAMES TO FORCE_NO_PICK'
# commonly for games on Thursdays with teams from picked teams

# ig_games = [6,8,9,13,15] #1
# ig_games << 247 #3
# ig_games << 592 #6
# ig_games << 1038 #10
# ig_games << 1151 #11
# ig_games << 1267 #12
# ig_games << 1379 #13

# ig_games.each do |g_id|
# 	Game.find(g_id).update_attributes(:force_no_pick => true)
# end


puts 'CHANGING GAMES TO FORCE_PICK'
# commonly for FCS playoffs

# fce_games = [1434,1435,1436,1438] #13
# fce_games += [1462,1464,1465,1466,1467] #14

# fce_games.each do |g_id|
# 	Game.find(g_id).update_attributes(:force_pick => true)
# end

puts 'SETTING UP THE USER PENALTIES'

# x = Penalty.create! :season_week => 8, :user_id => 11